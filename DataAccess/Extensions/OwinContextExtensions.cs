using Autofac;
using Autofac.Integration.Owin;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using StudioKit.Data.Entity.Identity;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.DataAccess.Interfaces;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Models.Interfaces;
using StudioKit.Sharding.Entity.Identity.Models;
using StudioKit.Sharding.Entity.Interfaces;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.DataAccess.Extensions
{
	public static class OwinContextExtensions
	{
		private const string CurrentUserKey = "CurrentUser";
		private const string CurrentUserRolesKey = "CurrentUserRoles";
		private const string ConfigurationKey = "Configuration";
		private const string LicenseKey = "License";

		#region User

		public static T GetCurrentUser<T>(this IOwinContext context)
			where T : IBaseUser, new()
		{
			var user = context.Get<T>(CurrentUserKey);
			if (user != null)
				return user;

			var userId = context.Request.User.Identity.GetUserId();
			var scope = context.GetAutofacLifetimeScope();
			var userManager = scope.Resolve<IUserManager<T>>();
			user = userManager.FindById(userId);

			context.Set(CurrentUserKey, user);
			return user;
		}

		public static async Task<T> GetCurrentUserAsync<T>(this IOwinContext context)
			where T : IBaseUser, new()
		{
			var user = context.Get<T>(CurrentUserKey);
			if (user != null)
				return user;

			var userId = context.Request.User.Identity.GetUserId();
			var scope = context.GetAutofacLifetimeScope();
			var userManager = scope.Resolve<IUserManager<T>>();
			user = await userManager.FindByIdAsync(userId);

			context.Set(CurrentUserKey, user);
			return user;
		}

		#endregion User

		#region UserRoles

		public static IList<string> GetCurrentUserRoles<T>(this IOwinContext context)
			where T : IBaseUser, new()
		{
			var roles = context.Get<IList<string>>(CurrentUserRolesKey);
			if (roles != null)
				return roles;

			var userId = context.Request.User.Identity.GetUserId();
			var scope = context.GetAutofacLifetimeScope();
			var userManager = scope.Resolve<IUserManager<T>>();
			roles = userManager.GetRoles(userId);

			context.Set(CurrentUserRolesKey, roles);
			return roles;
		}

		public static async Task<IList<string>> GetCurrentUserRolesAsync<T>(this IOwinContext context)
			where T : IBaseUser, new()
		{
			var roles = context.Get<IList<string>>(CurrentUserRolesKey);
			if (roles != null)
				return roles;

			var userId = context.Request.User.Identity.GetUserId();
			var scope = context.GetAutofacLifetimeScope();
			var userManager = scope.Resolve<IUserManager<T>>();
			roles = await userManager.GetRolesAsync(userId);

			context.Set(CurrentUserRolesKey, roles);
			return roles;
		}

		public static bool IsCurrentUserAdmin<T>(this IOwinContext context)
			where T : IBaseUser, new()
		{
			var roles = context.GetCurrentUserRoles<T>();
			if (roles == null)
				return false;
			return roles.Contains(BaseRole.SuperAdmin) || roles.Contains(BaseRole.Admin);
		}

		public static async Task<bool> IsCurrentUserAdminAsync<T>(this IOwinContext context)
			where T : IBaseUser, new()
		{
			var roles = await context.GetCurrentUserRolesAsync<T>();
			if (roles == null)
				return false;
			return roles.Contains(BaseRole.SuperAdmin) || roles.Contains(BaseRole.Admin);
		}

		public static bool IsCurrentUserSuperAdmin<T>(this IOwinContext context)
			where T : IBaseUser, new()
		{
			var roles = context.GetCurrentUserRoles<T>();
			return roles != null && roles.Contains(BaseRole.SuperAdmin);
		}

		public static async Task<bool> IsCurrentUserSuperAdminAsync<T>(this IOwinContext context)
			where T : IBaseUser, new()
		{
			var roles = await context.GetCurrentUserRolesAsync<T>();
			return roles != null && roles.Contains(BaseRole.SuperAdmin);
		}

		#endregion UserRoles

		#region Configuration

		public static T GetConfiguration<T>(this IOwinContext context)
			where T : BaseConfiguration, new()
		{
			var configuration = context.Get<T>(ConfigurationKey);
			if (configuration != null)
				return configuration;

			var scope = context.GetAutofacLifetimeScope();
			var db = scope.Resolve<IShardDbContext<T, License>>();
			configuration = db.GetConfiguration();

			context.Set(ConfigurationKey, configuration);
			//Configuration.Instance = configuration;

			return configuration;
		}

		public static async Task<T> GetConfigurationAsync<T>(this IOwinContext context, CancellationToken cancellationToken = default(CancellationToken))
			where T : BaseConfiguration, new()
		{
			var configuration = context.Get<T>(ConfigurationKey);
			if (configuration != null)
				return configuration;

			var scope = context.GetAutofacLifetimeScope();
			var db = scope.Resolve<IShardDbContext<T, License>>();
			configuration = await db.GetConfigurationAsync(cancellationToken);

			context.Set(ConfigurationKey, configuration);
			//Configuration.Instance = configuration;

			return configuration;
		}

		#endregion Configuration

		#region License

		public static License GetLicense(this IOwinContext context)
		{
			var license = context.Get<License>(LicenseKey);
			if (license != null)
				return license;

			var scope = context.GetAutofacLifetimeScope();
			var db = scope.Resolve<IBaseDbContext>();
			license = db.GetLicense();

			context.Set(LicenseKey, license);

			return license;
		}

		public static async Task<License> GetLicenseAsync(this IOwinContext context, CancellationToken cancellationToken = default(CancellationToken))
		{
			var license = context.Get<License>(LicenseKey);
			if (license != null)
				return license;

			var scope = context.GetAutofacLifetimeScope();
			var db = scope.Resolve<IBaseDbContext>();
			license = await db.GetLicenseAsync(cancellationToken);

			context.Set(LicenseKey, license);

			return license;
		}

		#endregion License
	}
}