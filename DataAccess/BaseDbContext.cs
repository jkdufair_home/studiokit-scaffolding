using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using StudioKit.Data;
using StudioKit.Data.Attributes;
using StudioKit.Data.Entity.Attributes;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.Data.Interfaces;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.ExternalProvider.DataAccess.Interfaces;
using StudioKit.ExternalProvider.Lti.DataAccess;
using StudioKit.ExternalProvider.Lti.Models;
using StudioKit.ExternalProvider.UniTime.BusinessLogic.Utilities;
using StudioKit.ExternalProvider.UniTime.Models;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.Common;
using StudioKit.Scaffolding.Common.Interfaces;
using StudioKit.Scaffolding.DataAccess.Interfaces;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Models.Interfaces;
using StudioKit.Scaffolding.Properties;
using StudioKit.Security;
using StudioKit.Security.Entity.Identity.Interfaces;
using StudioKit.Security.Entity.Identity.Models;
using StudioKit.Security.Interfaces;
using StudioKit.Sharding;
using StudioKit.Sharding.Entity.Identity;
using StudioKit.Sharding.Entity.Identity.Models;
using StudioKit.UserInfoService;
using StudioKit.Utilities.Extensions;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Activity = StudioKit.Scaffolding.Models.Activity;
using Login = StudioKit.Data.Entity.Identity.Models.Login;

namespace StudioKit.Scaffolding.DataAccess
{
	public class BaseDbContext<TUser, TGroup, TConfiguration> :
		UserIdentityShardDbContext<TUser, IdentityProvider, TConfiguration, License>,
		IIdentityClientDbContext<AppClient, IdentityServiceClient, ServiceClientRole, RefreshToken>,
		IRosterSyncDbContext<TUser, TGroup, GroupUserRole, GroupUserRoleLog, ExternalGroup, ExternalGroupUser>,
		ITermSyncDbContext<ExternalTerm>,
		ILtiLaunchDbContext<GroupUserRole, GroupUserRoleLog, ExternalGroup, ExternalGroupUser>,
		IBaseDbContext,
		IPrincipalService
		where TUser : IdentityUser<string, UserLogin, UserRole, UserClaim>, IBaseUser, new()
		where TGroup : BaseGroup, new()
		where TConfiguration : BaseConfiguration, new()
	{
		protected readonly IDateTimeProvider DateTimeProvider;
		protected readonly IPrincipalProvider PrincipalProvider;

		public BaseDbContext() : this("DefaultConnection", new DateTimeProvider(), new NullPrincipalProvider())
		{
		}

		public BaseDbContext(DbConnection existingConnection, IDateTimeProvider dateTimeProvider, IPrincipalProvider principalProvider) : base(existingConnection)
		{
			DateTimeProvider = dateTimeProvider;
			PrincipalProvider = principalProvider;
			SetSavingChanges();
		}

		public BaseDbContext(string nameOrConnectionString, IDateTimeProvider dateTimeProvider, IPrincipalProvider principalProvider) : base(nameOrConnectionString)
		{
			DateTimeProvider = dateTimeProvider;
			PrincipalProvider = principalProvider;
			SetSavingChanges();
		}

		private void SetSavingChanges()
		{
			var objectContext = ((IObjectContextAdapter)this).ObjectContext;
			objectContext.SavingChanges += (sender, args) =>
			{
				var utcNow = DateTimeProvider.UtcNow;
				var principalId = PrincipalProvider.GetPrincipal()?.Identity.GetUserId();
				foreach (var entry in ChangeTracker.Entries<IAuditable>())
				{
					var entity = entry.Entity;
					switch (entry.State)
					{
						case EntityState.Added:
							entity.DateStored = utcNow;
							entity.DateLastUpdated = utcNow;
							entity.LastUpdatedById = principalId;
							break;

						case EntityState.Modified:
							entity.DateLastUpdated = utcNow;
							entity.LastUpdatedById = principalId;
							break;
					}
				}
				ChangeTracker.DetectChanges();
			};
		}

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);

			modelBuilder.Conventions.Add(
				new AttributeToColumnAnnotationConvention<CaseSensitiveAttribute, bool>("CaseSensitive",
					(property, attributes) => attributes.Single().IsEnabled));
			modelBuilder.Conventions.Add(new DecimalPrecisionAttributeConvention());
		}

		#region StudioKit.Data.Entity.Identity

		public virtual DbSet<Login> Logins { get; set; }

		#endregion StudioKit.Data.Entity.Identity

		#region StudioKit.Security.Entity + StudioKit.Security.Entity.Identity

		public virtual DbSet<RefreshToken> RefreshTokens { get; set; }

		public virtual DbSet<Client> Clients { get; set; }

		public virtual DbSet<AppClient> AppClients { get; set; }

		public virtual DbSet<IdentityServiceClient> ServiceClients { get; set; }

		public virtual DbSet<ServiceClientRole> ServiceClientRoles { get; set; }

		#endregion StudioKit.Security.Entity + StudioKit.Security.Entity.Identity

		#region StudioKit.ExternalProvider

		public virtual DbSet<ExternalProvider.Models.ExternalProvider> ExternalProviders { get; set; }

		public virtual DbSet<UniTimeExternalProvider> UniTimeExternalProviders { get; set; }

		public virtual DbSet<LtiExternalProvider> LtiExternalProviders { get; set; }

		public virtual DbSet<ExternalGroup> ExternalGroups { get; set; }

		public virtual DbSet<ExternalGroupUser> ExternalGroupUsers { get; set; }

		public virtual DbSet<LtiLaunch> LtiLaunches { get; set; }

		public virtual DbSet<UniTimeGroup> UniTimeGroups { get; set; }

		#endregion StudioKit.ExternalProvider

		#region Scaffolding

		public virtual DbSet<ExternalTerm> ExternalTerms { get; set; }

		public virtual DbSet<TGroup> Groups { get; set; }

		public virtual DbSet<EntityUserRole> EntityUserRoles { get; set; }

		public virtual DbSet<GroupUserRole> GroupUserRoles { get; set; }

		public virtual DbSet<GroupUserRoleLog> GroupUserRoleLogs { get; set; }

		public virtual DbSet<Activity> Activities { get; set; }

		public virtual DbSet<RoleActivity> RoleActivities { get; set; }

		public virtual DbSet<Artifact> Artifacts { get; set; }

		public virtual Func<int, IQueryable<BaseGroup>> GroupsQueryableFunc<T>() where T : ModelBase
		{
			throw new NotImplementedException();
		}

		public IQueryable<int> GroupsQueryableGroupIds<T>(string userId, T entity, params string[] activities)
			where T : ModelBase
		{
			return GroupsQueryableFunc<T>()(entity.Id)
				.SelectMany(g => g.GroupUserRoles)
				.Where(r => r.UserId.Equals(userId))
				.Join(RoleActivities.Where(ra => activities.Contains(ra.Activity.Name)),
					gur => gur.RoleId,
					ra => ra.RoleId,
					(eur, ra) => eur.GroupId)
				.Distinct();
		}

		public async Task<(List<string>, List<string>)> UserEntityRolesAndActivitiesAsync<TEntity, TEntityUserRole>(string userId,
			TEntity entity, CancellationToken cancellationToken = default(CancellationToken))
			where TEntity : ModelBase
			where TEntityUserRole : EntityUserRole
		{
			var entityUserRoleIds = (await EntityUserRoles.Where(eur => eur.UserId == userId && eur is TEntityUserRole)
					.ToListAsync(cancellationToken))
				.Where(eur => eur.EntityId == entity.Id)
				.Select(eur => eur.RoleId)
				.ToList();
			var roleNamesAndActivities = await RoleActivities.Where(ra => entityUserRoleIds.Contains(ra.RoleId)).Select(ra => new
			{
				RoleName = ra.Role.Name,
				ActivityName = ra.Activity.Name
			}).ToListAsync(cancellationToken);
			return (roleNamesAndActivities.Select(ra => ra.RoleName).Distinct().ToList(),
				roleNamesAndActivities.Select(o => o.ActivityName).Distinct().ToList());
		}

		public async Task<Dictionary<int, (List<string>, List<string>)>> UserEntityRolesAndActivitiesAsync<TEntity, TEntityUserRole>(string userId,
			IQueryable<TEntity> entities, CancellationToken cancellationToken = default(CancellationToken))
			where TEntity : ModelBase
			where TEntityUserRole : EntityUserRole
		{
			// load the entity ids
			var entityIds = await entities.Select(e => e.Id).ToListAsync(cancellationToken);

			// load the entity roles for the user
			var entityUserRoles = (await EntityUserRoles
					.Where(eur => eur.UserId == userId && eur is TEntityUserRole)
					.ToListAsync(cancellationToken))
				.Where(eur => entityIds.Contains(eur.EntityId))
				.ToList();

			// for the roles the user has, load the corresponding activities
			var roleIds = entityUserRoles
				.Select(o => o.RoleId)
				.Distinct()
				.ToList();
			var roleNamesAndActivities = (await RoleActivities
					.Where(ra => roleIds.Contains(ra.RoleId))
					.Select(ra => new
					{
						ra.RoleId,
						RoleName = ra.Role.Name,
						ActivityName = ra.Activity.Name,
					})
					.ToListAsync(cancellationToken))
				.GroupBy(ra => new
				{
					Id = ra.RoleId,
					ra.RoleName
				})
				.Select(g => new
				{
					Role = g.Key,
					ActivityNames = g.Select(o => o.ActivityName)
				});

			// join user's entity roles and activities, group distinctly by entity
			var rolesAndActivitiesByEntity = entityUserRoles
				.Join(roleNamesAndActivities,
					eur => eur.RoleId,
					rna => rna.Role.Id,
					(eur, rna) => new
					{
						eur.EntityId,
						rna.Role.RoleName,
						rna.ActivityNames
					})
				.GroupBy(eur => eur.EntityId)
				.ToDictionary(
					g => g.Key,
					g => (g.Select(ar => ar.RoleName).Distinct().ToList(), g.SelectMany(ar => ar.ActivityNames).Distinct().ToList())
				);

			// return entityIds in a dictionary paired with a tuple of roles and activities (if any)
			return entityIds
				.ToDictionary(
					entityId => entityId,
					entityId => !rolesAndActivitiesByEntity.ContainsKey(entityId)
						? (new List<string>(), new List<string>())
						: rolesAndActivitiesByEntity[entityId]);
		}

		#endregion Scaffolding

		#region IPrincipalService

		public bool PrincipalExists(string id)
		{
			return Users.Any(u => u.Id.Equals(id)) || ServiceClients.Any(c => c.ClientId.Equals(id));
		}

		#endregion IPrincipalService

		#region Repository Business Logic

		public async Task<TEntity> FindEntityAsync<TEntity>(int entityKey,
			bool throwIfSoftDeleted = true,
			string notFoundMessage = null,
			CancellationToken cancellationToken = default(CancellationToken))
			where TEntity : ModelBase
		{
			var entity = await Set<TEntity>().FindAsync(cancellationToken, entityKey);
			if (entity == null || entity is IDelible delibleEntity && delibleEntity.IsDeleted && throwIfSoftDeleted)
				throw new EntityNotFoundException(notFoundMessage ?? string.Format(Strings.EntityNotFoundById, typeof(TEntity).Name, entityKey));
			return entity;
		}

		public IQueryable<TGroup> GetUserGroupsQueryable(bool queryAll, string userId, bool includeIfSoftDeleted = false)
		{
			if (queryAll)
				return Groups.Where(g => includeIfSoftDeleted || !g.IsDeleted);

			return GroupUserRoles
				.Where(r => r.UserId.Equals(userId))
				.Join(RoleActivities.Where(ra => ra.Activity.Name == BaseActivity.GroupRead),
					gur => gur.RoleId,
					ra => ra.RoleId,
					(eur, ra) => eur.GroupId)
				.Join(Groups.Where(g => includeIfSoftDeleted || !g.IsDeleted),
					groupId => groupId,
					g => g.Id, (groupId, g) => g)
				.Distinct();
		}

		public virtual async Task DeleteGroupAsync(TGroup group, CancellationToken cancellationToken = default(CancellationToken))
		{
			group.IsDeleted = true;
			await SaveChangesAsync(cancellationToken);
		}

		public virtual async Task SaveGroupUserRoleAsync(GroupUserRole groupUserRole, CancellationToken cancellationToken = default(CancellationToken))
		{
			GroupUserRoles.Add(groupUserRole);
			await SaveChangesAsync(cancellationToken);
		}

		public virtual async Task SaveGroupUserRolesAsync(int groupId, List<GroupUserRole> groupUserRoles, CancellationToken cancellationToken = default(CancellationToken))
		{
			GroupUserRoles.AddRange(groupUserRoles);
			await SaveChangesAsync(cancellationToken);
		}

		public virtual Task OnSyncRosterCompleteAsync(int? groupId, CancellationToken cancellationToken = default(CancellationToken))
		{
			return Task.FromResult(0);
		}

		/// <summary>
		/// Get or create a list of <see cref="TUser"/>s. Uses the given <see cref="identifiers"/> as each User's UserName and Email on creation.
		/// </summary>
		/// <param name="identifiers">A list of scoped identifiers, e.g. email address or ePPN.</param>
		/// <param name="configuration"><see cref="TConfiguration"/> used to validate domains in scoped identifiers.</param>
		/// <param name="shardKey">The shard key of the shard database.</param>
		/// <param name="userInfoService">The service needed to call the User Information Service from Purdue.</param>
		/// <param name="cancellationToken">A <see cref="CancellationToken"/> from the caller</param>
		/// <returns>A <see cref="UserSearchResult{TUser}"/> containing which user identifiers were found or created, and which were invalid.</returns>
		public async Task<UserSearchResult<TUser>> GetOrCreateUsersByIdentifiersAsync(List<string> identifiers, TConfiguration configuration, string shardKey, IUserInfoService userInfoService = null, CancellationToken cancellationToken = default(CancellationToken))
		{
			if (shardKey.Equals(ShardDomainConstants.PurdueShardKey) && userInfoService != null)
			{
				var (userInfoUsers, invalidIdentifiers) = userInfoService.GetUserInfoUsersByIdentifiers<TUser>(identifiers);
				var result = await GetOrCreateUsersAsync(userInfoUsers, configuration, cancellationToken);
				result.InvalidIdentifiers = result.InvalidIdentifiers.Union(invalidIdentifiers).ToList();
				return result;
			}

			var users = identifiers
				.Select(i => new TUser
				{
					UserName = i,
					Email = i
				})
				.ToList();

			return await GetOrCreateUsersAsync(users, configuration, cancellationToken);
		}

		/// <summary>
		/// Get or create a list of <see cref="TUser"/>s. Users are on UserName or Email and both are required in order to be created.
		/// </summary>
		/// <param name="users">A list of <see cref="TUser"/>s to get or create. Are matched on UserName or Email.</param>
		/// <param name="configuration"><see cref="TConfiguration"/> used to validate domains in scoped identifiers.</param>
		/// <param name="cancellationToken">A <see cref="CancellationToken"/> from the caller</param>
		/// <returns>A <see cref="UserSearchResult{TUser}"/> containing which user identifiers were found or created, and which were invalid.</returns>
		public async Task<UserSearchResult<TUser>> GetOrCreateUsersAsync(List<TUser> users, TConfiguration configuration, CancellationToken cancellationToken = default(CancellationToken))
		{
			var result = new UserSearchResult<TUser>
			{
				AllowedDomains = configuration.AllowedDomains
			};

			// valid UserName/Email = scoped variable (email or ePPN), e.g. alias@domain.edu
			var validUsers = users
				.Where(u =>
					u.UserName.HasValidDomainScope()
					&& configuration.ValidateAllowedDomains(u.UserName)
					&& u.Email.HasValidDomainScope()
					&& configuration.ValidateAllowedDomains(u.Email))
				.ToList();
			var invalidUsers = users
				.Where(u =>
					!u.UserName.HasValidDomainScope()
					|| !configuration.ValidateAllowedDomains(u.UserName)
					|| !u.Email.HasValidDomainScope()
					|| !configuration.ValidateAllowedDomains(u.Email))
				.ToList();
			result.InvalidIdentifiers = invalidUsers.Select(u => u.UserName).ToList();

			// Load existing users
			var existingUsers = await BulkLoadUsersAsync(validUsers, cancellationToken);

			// Find users with no match
			var newUsers = validUsers
				.Where(u =>
					existingUsers.All(e => e.UserName != u.UserName)
					&& existingUsers.All(e => e.Email != u.Email))
				.ToList();

			// Update any user if firstname, lastname or puid is empty
			await UpdateUsersWithUsersAsync(existingUsers, validUsers, cancellationToken);

			// No new users need to be created
			if (!newUsers.Any())
			{
				result.Users = existingUsers;
				return result;
			}

			// Create users that were not found
			foreach (var newUser in newUsers)
			{
				newUser.SecurityStamp = Guid.NewGuid().ToString();
				Users.Add(newUser);
			}
			await SaveChangesAsync(cancellationToken);

			// Add to result
			existingUsers.AddRange(newUsers);
			result.Users = existingUsers;

			return result;
		}

		public virtual async Task<List<TUser>> BulkLoadUsersAsync(List<TUser> validUsers, CancellationToken cancellationToken = default(CancellationToken))
		{
			var validUserNames = validUsers.Select(u => u.UserName).ToList();
			var validEmails = validUsers.Select(u => u.Email).ToList();
			const int parametersMaximum = 2000;
			var existingUsers = new List<TUser>();

			// load by username
			while (validUserNames.Any())
			{
				var chunkUserNames = validUserNames.Take(parametersMaximum).ToList();
				var paramsString = string.Join(",", chunkUserNames.Select((u, i) => $"@p{i}"));
				var paramsArray = chunkUserNames.Select((u, i) => new SqlParameter($"@p{i}", u)).ToArray();
				var chunkResults = await Database
					.SqlQuery<TUser>($"SELECT * FROM [dbo].[AspNetUsers] WHERE [UserName] IN ({paramsString})", paramsArray)
					.ToListAsync(cancellationToken);
				existingUsers.AddRange(chunkResults);
				validUserNames = validUserNames.Skip(parametersMaximum).ToList();
			}
			existingUsers = existingUsers.Distinct().ToList();

			// filter out by users already loaded
			validEmails = validEmails.Where(e => existingUsers.All(u => !u.Email.Equals(e))).ToList();
			// load by email
			while (validEmails.Any())
			{
				var chunkEmails = validEmails.Take(parametersMaximum).ToList();
				var paramsString = string.Join(",", chunkEmails.Select((u, i) => $"@p{i}"));
				var paramsArray = chunkEmails.Select((u, i) => new SqlParameter($"@p{i}", u)).ToArray();
				var chunkResults = await Database
					.SqlQuery<TUser>($"SELECT * FROM [dbo].[AspNetUsers] WHERE [Email] IN ({paramsString})", paramsArray)
					.ToListAsync(cancellationToken);
				existingUsers.AddRange(chunkResults);
				validEmails = validEmails.Skip(parametersMaximum).ToList();
			}
			existingUsers = existingUsers.Distinct().ToList();

			return existingUsers;
		}

		public async Task UpdateUsersWithUsersAsync(List<TUser> existingUsers, List<TUser> newUsers, CancellationToken cancellationToken = default(CancellationToken))
		{
			// update any user if firstname, lastname, puid, alias is empty
			var emptyUsers = existingUsers
				.Where(e =>
					(e.FirstName == null || e.LastName == null || e.EmployeeNumber == null || e.Uid == null) &&
					newUsers.Any(n => n.Email.Equals(e.Email)))
				.ToList();

			if (emptyUsers.Any())
			{
				foreach (var emptyUser in emptyUsers)
				{
					var user = newUsers.SingleOrDefault(v => v.Email.Equals(emptyUser.Email));
					if (user == null) continue;
					emptyUser.FirstName = user.FirstName;
					emptyUser.LastName = user.LastName;
					emptyUser.EmployeeNumber = user.EmployeeNumber;
					emptyUser.Uid = user.Uid;
					Entry(emptyUser).State = EntityState.Modified;
				}
				await SaveChangesAsync(cancellationToken);
			}
		}

		public async Task<IEnumerable<TUser>> GetOrCreateUsersAsync(IEnumerable<TUser> users, CancellationToken cancellationToken = default(CancellationToken))
		{
			var newUsers = users.ToList();

			// load (or create) new users from the DB. also updates missing user data
			var configuration = await GetConfigurationAsync(cancellationToken);
			var getOrCreateResult = newUsers.Any()
				? await GetOrCreateUsersAsync(newUsers, configuration, cancellationToken)
				: new UserSearchResult<TUser>();

			return getOrCreateResult.Users
				.Distinct()
				.ToList();
		}

		#endregion Repository Business Logic
	}
}