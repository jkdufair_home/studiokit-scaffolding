using StudioKit.Data.Entity.Identity;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.Scaffolding.Common;
using StudioKit.Scaffolding.DataAccess.Interfaces;
using StudioKit.Scaffolding.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;

namespace StudioKit.Scaffolding.DataAccess
{
	public static class BaseSeed
	{
		public static void Seed(IBaseDbContext dbContext)
		{
			// Note: Not using IDateTimeProvider here for UtcNow. Not necessary
			SeedDatabase(dbContext);
		}

		public static void SeedDatabase(IBaseDbContext dbContext)
		{
			SeedRoles(dbContext);
			SeedBaseActivities(dbContext);
			SeedBaseRoleActivities(dbContext);
		}

		public static void SeedRoles(IBaseDbContext dbContext)
		{
			CreateRoles(dbContext, typeof(BaseRole));
			dbContext.SaveChanges();
		}

		public static void SeedBaseActivities(IBaseDbContext dbContext)
		{
			CreateActivities(dbContext, typeof(BaseActivity));
			dbContext.SaveChanges();
		}

		public static void SeedBaseRoleActivities(IBaseDbContext dbContext)
		{
			var roleActivities = new Dictionary<string, List<string>>
			{
				{
					BaseRole.SuperAdmin,
					new List<string>
					{
						BaseActivity.UserRoleReadAny,
						BaseActivity.UserRoleModifyAny,

						BaseActivity.UserReadAny,
						BaseActivity.UserUpdateAny,

						BaseActivity.GroupRead,
						BaseActivity.GroupCreate,
						BaseActivity.GroupUpdate,
						BaseActivity.GroupDelete,

						BaseActivity.GroupUserRoleRead,
						BaseActivity.GroupUserRoleModify,

						BaseActivity.ExternalGroupRead,
						BaseActivity.ExternalGroupConnectAny,
						BaseActivity.ExternalGroupConnectOwn,

						BaseActivity.GroupRosterSyncAll,
						BaseActivity.GroupRosterSync,

						BaseActivity.ExternalProviderModify,

						BaseActivity.LtiLaunchReadAny,

						BaseActivity.ConfigurationModify,

						BaseActivity.IdentityProviderModify,

						BaseActivity.CacheStatisticsRead,
						BaseActivity.CachePurge
					}
				},
				{
					BaseRole.Admin,
					new List<string>
					{
						BaseActivity.UserRoleReadAny,
						BaseActivity.UserRoleModifyAny,

						BaseActivity.UserReadAny,
						BaseActivity.UserUpdateAny,

						BaseActivity.GroupRead,
						BaseActivity.GroupCreate,
						BaseActivity.GroupUpdate,
						BaseActivity.GroupDelete,

						BaseActivity.GroupUserRoleRead,
						BaseActivity.GroupUserRoleModify,

						BaseActivity.ExternalGroupRead,
						BaseActivity.ExternalGroupConnectAny,
						BaseActivity.ExternalGroupConnectOwn,

						BaseActivity.GroupRosterSyncAll,
						BaseActivity.GroupRosterSync,

						BaseActivity.ExternalProviderModify,

						BaseActivity.LtiLaunchReadAny,

						BaseActivity.ConfigurationModify,

						BaseActivity.IdentityProviderModify,

						BaseActivity.CacheStatisticsRead,
						BaseActivity.CachePurge
					}
				},
				{
					BaseRole.Creator,
					new List<string>
					{
						BaseActivity.GroupCreate,

						BaseActivity.ExternalGroupConnectOwn,

						BaseActivity.LtiLaunchReadOwn
					}
				},
				{
					BaseRole.GroupOwner,
					new List<string>
					{
						BaseActivity.GroupRead,
						BaseActivity.GroupUpdate,
						BaseActivity.GroupDelete,

						BaseActivity.GroupUserRoleRead,
						BaseActivity.GroupUserRoleModify,

						BaseActivity.ExternalGroupRead,
						BaseActivity.ExternalGroupConnectOwn,

						BaseActivity.GroupRosterSync
					}
				},
				{
					BaseRole.GroupGrader,
					new List<string>
					{
						BaseActivity.GroupRead,
						BaseActivity.GroupUserRoleRead
					}
				},
				{
					BaseRole.GroupLearner,
					new List<string>
					{
						BaseActivity.GroupRead
					}
				}
			};

			var roles = dbContext.Roles.ToList();
			CreateRoleActivities(dbContext, roleActivities, roles);
			dbContext.SaveChanges();
		}

		public static void CreateRoles(IBaseDbContext dbContext, Type roleType)
		{
			var existingRoleNames = dbContext.Roles.Select(r => r.Name).ToList();
			var rolesToAdd = GetNewNamesForType(roleType, existingRoleNames)
				.Select(roleName => new Role
				{
					Name = roleName
				})
				.ToList();
			if (!rolesToAdd.Any()) return;
			((DbSet<Role>)dbContext.Roles).AddRange(rolesToAdd);
		}

		public static void CreateActivities(IBaseDbContext dbContext, Type activityType)
		{
			var existingActivityNames = dbContext.Activities.Select(a => a.Name).ToList();
			var activitiesToAdd = GetNewNamesForType(activityType, existingActivityNames)
				.Select(activityName => new Activity
				{
					Name = activityName
				})
				.ToList();
			if (!activitiesToAdd.Any()) return;
			dbContext.Activities.AddRange(activitiesToAdd);
		}

		public static void DeleteOldActivities(IBaseDbContext dbContext, params Type[] activityTypes)
		{
			var existingActivityNames = dbContext.Activities.ToList();
			var activitiesToDelete = GetOldNamesForType(activityTypes, existingActivityNames).ToList();
			if (!activitiesToDelete.Any()) return;
			dbContext.Activities.RemoveRange(activitiesToDelete);
		}

		public static void CreateRoleActivities(IBaseDbContext dbContext, Dictionary<string, List<string>> roleActivities,
			List<Role> existingRoles)
		{
			var existingActivities = dbContext.Activities.ToList();
			var existingRoleActivities = dbContext.RoleActivities.ToList();
			var roleActivitiesToAdd = roleActivities
				.SelectMany(roleActivity =>
					roleActivity.Value.Select(activityName => new RoleActivity
					{
						RoleId = existingRoles.Single(r => r.Name.Equals(roleActivity.Key)).Id,
						ActivityId = existingActivities.Single(a => a.Name.Equals(activityName)).Id
					})
				)
				.Where(ra => !existingRoleActivities.Any(cra =>
					cra.ActivityId == ra.ActivityId && cra.RoleId == ra.RoleId))
				.ToList();
			if (!roleActivitiesToAdd.Any()) return;
			dbContext.RoleActivities.AddRange(roleActivitiesToAdd);
		}

		private static IEnumerable<string> GetNewNamesForType(IReflect type, ICollection<string> existingNames)
		{
			var typeNames = type
				.GetFields(BindingFlags.Public | BindingFlags.Static)
				.Where(f => f.IsLiteral && !f.IsInitOnly && f.FieldType == typeof(string))
				.Select(x => (string)x.GetRawConstantValue());
			return typeNames
				.Where(name => !existingNames.Contains(name))
				.ToList();
		}

		private static IEnumerable<Activity> GetOldNamesForType(IEnumerable<IReflect> types, IEnumerable<Activity> existingActivities)
		{
			var typeNames = new List<string>();
			foreach (var type in types)
			{
				var names = type
					.GetFields(BindingFlags.Public | BindingFlags.Static)
					.Where(f => f.IsLiteral && !f.IsInitOnly && f.FieldType == typeof(string))
					.Select(x => (string)x.GetRawConstantValue());
				typeNames.AddRange(names);
			}
			
			return existingActivities
				.Where(activity => !typeNames.Contains(activity.Name))
				.ToList();
		}
	}
}