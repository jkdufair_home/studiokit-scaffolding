﻿using StudioKit.Data.Entity;
using StudioKit.Sharding.Entity;
using System.Data.Entity.SqlServer;

namespace StudioKit.Scaffolding.DataAccess
{
	public class BaseDbConfiguration : ShardDbConfiguration
	{
		public BaseDbConfiguration()
		{
			// TODO: Using multiple Setters won't work. The last occurrence overrides the previous ones.
			// Find the proper way to apply multiple MigrationSqlGenerators

			//SetMigrationSqlGenerator(SqlProviderServices.ProviderInvariantName,
			//	() => new CaseSensitiveSqlServerMigrationSqlGenerator());

			SetMigrationSqlGenerator(SqlProviderServices.ProviderInvariantName,
				() => new DefaultDateSqlServerMigrationSqlGenerator());
		}
	}
}