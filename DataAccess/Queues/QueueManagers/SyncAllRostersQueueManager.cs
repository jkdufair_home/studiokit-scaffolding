﻿using StudioKit.Cloud.Storage.Queue;
using StudioKit.Diagnostics;
using StudioKit.ErrorHandling.Interfaces;
using StudioKit.Scaffolding.DataAccess.Queues.QueueMessages;

namespace StudioKit.Scaffolding.DataAccess.Queues.QueueManagers
{
	public class SyncAllRostersQueueManager : AzureQueueManager<SyncAllRostersMessage>
	{
		public SyncAllRostersQueueManager(ILogger logger, IErrorHandler errorHandler) : base(logger, errorHandler)
		{
		}
	}
}