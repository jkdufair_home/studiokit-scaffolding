﻿using StudioKit.Cloud.Storage.Queue;
using System.Runtime.Serialization;

namespace StudioKit.Scaffolding.DataAccess.Queues.QueueMessages
{
	[DataContract]
	public class SyncGroupRosterMessage : AzureQueueMessage
	{
		[DataMember]
		public string ShardKey { get; set; }

		[DataMember]
		public int GroupId { get; set; }
	}
}