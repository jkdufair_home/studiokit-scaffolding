﻿using StudioKit.Caliper.Interfaces;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Interfaces;
using StudioKit.Sharding.Interfaces;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.DataAccess.Interfaces
{
	public interface IConfigurationProvider<TConfiguration> : ICaliperConfigurationProvider, ILtiConfigurationProvider
		where TConfiguration : IBaseShardConfiguration, ICaliperConfiguration
	{
		Task<TConfiguration> GetConfigurationAsync(CancellationToken cancellationToken = default(CancellationToken));
	}
}