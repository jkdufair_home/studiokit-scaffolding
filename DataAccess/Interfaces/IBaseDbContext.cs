using StudioKit.Data;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.ExternalProvider.UniTime.Models;
using StudioKit.Scaffolding.Models;
using StudioKit.Sharding.Entity.Identity.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.DataAccess.Interfaces
{
	public interface IBaseDbContext
	{
		Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken));

		int SaveChanges();

		IDbSet<Role> Roles { get; set; }

		DbSet<UserRole> IdentityUserRoles { get; set; }

		DbSet<IdentityProvider> IdentityProviders { get; set; }

		DbSet<Login> Logins { get; set; }

		DbSet<ExternalTerm> ExternalTerms { get; set; }

		DbSet<Activity> Activities { get; set; }

		DbSet<RoleActivity> RoleActivities { get; set; }

		DbSet<UniTimeGroup> UniTimeGroups { get; set; }

		#region Licenses

		DbSet<License> Licenses { get; set; }

		License GetLicense();

		Task<License> GetLicenseAsync(CancellationToken cancellationToken = default(CancellationToken));

		#endregion Licenses

		#region EntityUserRoles

		DbSet<EntityUserRole> EntityUserRoles { get; set; }

		DbSet<GroupUserRole> GroupUserRoles { get; set; }

		DbSet<GroupUserRoleLog> GroupUserRoleLogs { get; set; }

		Task SaveGroupUserRoleAsync(GroupUserRole groupUserRole, CancellationToken cancellationToken = default(CancellationToken));

		Task SaveGroupUserRolesAsync(int groupId, List<GroupUserRole> groupUserRoles, CancellationToken cancellationToken = default(CancellationToken));

		/// <summary>
		///	Returns a function used to generate a queryable of <see cref="BaseGroup"/>, representing all groups connected to the entity of type <see cref="T"/> with the Id passed to said function.
		/// </summary>
		/// <typeparam name="T">Type of entity, implementing <see cref="ModelBase"/></typeparam>
		/// <returns></returns>
		Func<int, IQueryable<BaseGroup>> GroupsQueryableFunc<T>() where T : ModelBase;

		IQueryable<int> GroupsQueryableGroupIds<T>(string userId, T entity, params string[] activities)
			where T : ModelBase;

		#endregion EntityUserRoles

		#region Artifacts

		DbSet<Artifact> Artifacts { get; set; }

		#endregion Artifacts

		Task<TEntity> FindEntityAsync<TEntity>(int entityKey,
			bool throwIfSoftDeleted = true,
			string notFoundMessage = null,
			CancellationToken cancellationToken = default(CancellationToken))
			where TEntity : ModelBase;
	}
}