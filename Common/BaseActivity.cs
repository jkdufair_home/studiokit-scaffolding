﻿namespace StudioKit.Scaffolding.Common
{
	public static class BaseActivity
	{
		public const string UserRoleReadAny = "UserRoleReadAny";
		public const string UserRoleModifyAny = "UserRoleModifyAny";
		public const string UserReadAny = "UserReadAny";
		public const string UserUpdateAny = "UserUpdateAny";
		public const string GroupRead = "GroupRead";
		public const string GroupCreate = "GroupCreate";
		public const string GroupUpdate = "GroupUpdate";
		public const string GroupDelete = "GroupDelete";
		public const string GroupUserRoleRead = "GroupUserRoleRead";
		public const string GroupUserRoleModify = "GroupUserRoleModify";
		public const string ExternalProviderModify = "ExternalProviderModify";
		public const string ExternalGroupRead = "ExternalGroupRead";
		public const string ExternalGroupConnectAny = "ExternalGroupConnectAny";
		public const string ExternalGroupConnectOwn = "ExternalGroupConnectOwn";
		public const string LtiLaunchReadOwn = "LtiLaunchReadOwn";
		public const string LtiLaunchReadAny = "LtiLaunchReadAny";
		public const string GroupRosterSyncAll = "GroupRosterSyncAll";
		public const string GroupRosterSync = "GroupRosterSync";
		public const string ConfigurationModify = "ConfigurationModify";
		public const string IdentityProviderModify = "IdentityProviderModify";
		public const string CacheStatisticsRead = "CacheStatisticsRead";
		public const string CachePurge = "CachePurge";
	}
}