﻿namespace StudioKit.Scaffolding.Common
{
	public static class ScaffoldingClaimTypes
	{
		public const string AppId = "StudioKit.Scaffolding.AppId";
		public const string AccessToken = "StudioKit.Scaffolding.AccessToken";
		public const string ExpiresIn = "StudioKit.Scaffolding.ExpiresIn";
		public const string RefreshToken = "StudioKit.Scaffolding.RefreshToken";

		public const string CasLogin = "StudioKit.Scaffolding.Cas.Login";
		public const string CasPuid = "StudioKit.Scaffolding.Cas.Puid";
		public const string CasI2A2Characteristics = "StudioKit.Cas.I2A2Characteristics";

		public const string FacebookAccessToken = "StudioKit.Scaffolding.FacebookAccessToken";
		public const string FacebookExpiresIn = "StudioKit.Scaffolding.FacebookExpiresIn";

		public const string GoogleAccessToken = "StudioKit.Scaffolding.GoogleAccessToken";
		public const string GoogleRefreshToken = "StudioKit.Scaffolding.GoogleRefreshToken";
		public const string GoogleExpiresIn = "StudioKit.Scaffolding.GoogleExpiresIn";
	}
}