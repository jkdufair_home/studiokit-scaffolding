﻿using StudioKit.Scaffolding.Common.Interfaces;

namespace StudioKit.Scaffolding.Common
{
	public class SimpleShardKeyProvider : IShardKeyProvider
	{
		private readonly string _shardKey;

		public SimpleShardKeyProvider(string shardKey)
		{
			_shardKey = shardKey;
		}

		public string GetShardKey()
		{
			return _shardKey;
		}
	}
}