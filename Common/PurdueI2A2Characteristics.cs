﻿using System.Linq;

namespace StudioKit.Scaffolding.Common
{
	public static class PurdueI2A2Characteristics
	{
		// https://www.purdue.edu/apps/account/html/chars.txt

		public const string AdminProfessionalStaff = "13101";
		public const string Faculty = "13105";
		public const string LimitedTermLecturer = "13111";
		public const string VisitingOrEmeritusFaculty = "13124";
		public const string ManagementProfessional = "13112";
		public const string ClinicalResearch = "13103";
		public const string ContinuingLecturer = "13104";
		public const string GraduateTeachingAssistant = "10362";

		public static bool IsPurdueInstructor(string[] i2A2Characteristics)
		{
			return i2A2Characteristics.Contains(AdminProfessionalStaff)
					|| i2A2Characteristics.Contains(ManagementProfessional)
					|| i2A2Characteristics.Contains(Faculty)
					|| i2A2Characteristics.Contains(LimitedTermLecturer)
					|| i2A2Characteristics.Contains(VisitingOrEmeritusFaculty)
					|| i2A2Characteristics.Contains(ClinicalResearch)
					|| i2A2Characteristics.Contains(ContinuingLecturer)
					|| i2A2Characteristics.Contains(GraduateTeachingAssistant);
		}
	}
}