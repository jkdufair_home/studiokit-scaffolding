﻿namespace StudioKit.Scaffolding.Common
{
	public static class LoginProviderConstants
	{
		public const string Shibboleth = "KentorAuthServices";
		public const string Lti = "LTI";
		public const string Facebook = "Facebook";
		public const string Google = "Google";
	}
}