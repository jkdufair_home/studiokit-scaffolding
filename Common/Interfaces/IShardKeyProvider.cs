﻿namespace StudioKit.Scaffolding.Common.Interfaces
{
	public interface IShardKeyProvider
	{
		string GetShardKey();
	}
}