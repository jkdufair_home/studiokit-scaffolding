﻿using System;

namespace StudioKit.Scaffolding.Common.Interfaces
{
	public interface IDateTimeProvider
	{
		DateTime UtcNow { get; }
	}
}