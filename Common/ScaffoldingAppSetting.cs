﻿namespace StudioKit.Scaffolding.Common
{
	public static class ScaffoldingAppSetting
	{
		public const string FacebookAppId = "FacebookAppId";

		public const string FacebookAppSecret = "FacebookAppSecret";

		public const string GoogleClientId = "GoogleClientId";

		public const string GoogleClientSecret = "GoogleClientSecret";

		// ReSharper disable once InconsistentNaming
		public const string GoogleClientIdiOS = "GoogleClientId-iOS";

		public const string GoogleClientIdAndroidDebug = "GoogleClientId-Android-Debug";

		public const string GoogleClientIdAndroid = "GoogleClientId-Android";

		public const string ArtifactTotpSecretKey = "ArtifactTotpSecretKey";

		public const string ArtifactTotpDurationSeconds = "ArtifactTotpDurationSeconds";
	}
}