using StudioKit.Scaffolding.Common.Interfaces;
using System;

namespace StudioKit.Scaffolding.Common
{
	/// <summary>
	/// This utility exists as a means of wrapping DateTime.UtcNow in order to be able
	/// to mock it for unit testing by providing an instance that can be set up to return
	/// a predictable value
	/// </summary>
	public class DateTimeProvider : IDateTimeProvider
	{
		public DateTime UtcNow => DateTime.UtcNow;
	}
}