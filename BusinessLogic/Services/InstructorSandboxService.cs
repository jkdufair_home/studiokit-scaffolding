﻿using Microsoft.AspNet.Identity.EntityFramework;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.DataAccess;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Models.Interfaces;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Services
{
	public abstract class InstructorSandboxService<TContext, TUser, TGroup, TConfiguration> : IInstructorSandboxService<TUser>
		where TContext : BaseDbContext<TUser, TGroup, TConfiguration>
		where TUser : IdentityUser<string, UserLogin, UserRole, UserClaim>, IBaseUser, new()
		where TGroup : BaseGroup, new()
		where TConfiguration : BaseConfiguration, new()
	{
		private readonly TContext _dbContext;

		protected InstructorSandboxService(TContext dbContext)
		{
			_dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
		}

		public void SetUpInstructorSandbox(TUser user)
		{
			var configuration = _dbContext.GetConfiguration();
			if (!configuration.IsInstructorSandboxEnabled)
				return;
			if (!IsUserInstructor(user))
				throw new ForbiddenException();
			CreateInstructorSandbox(user);
		}

		public async Task SetUpInstructorSandboxAsync(TUser user, CancellationToken cancellationToken = default(CancellationToken))
		{
			var configuration = await _dbContext.GetConfigurationAsync(cancellationToken);
			if (!configuration.IsInstructorSandboxEnabled)
				return;
			if (!await IsUserInstructorAsync(user, cancellationToken))
				throw new ForbiddenException();
			await CreateInstructorSandboxAsync(user, cancellationToken);
		}

		protected abstract Task<bool> IsUserInstructorAsync(TUser user, CancellationToken cancellationToken = default(CancellationToken));

		protected abstract bool IsUserInstructor(TUser user);

		protected abstract Task CreateInstructorSandboxAsync(TUser user, CancellationToken cancellationToken = default(CancellationToken));

		protected abstract void CreateInstructorSandbox(TUser user);
	}
}