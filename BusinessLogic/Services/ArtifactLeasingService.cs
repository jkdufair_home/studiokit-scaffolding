using OtpNet;
using StudioKit.Cloud.Storage.Blob;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.Scaffolding.BusinessLogic.Utils;
using StudioKit.Scaffolding.Common.Interfaces;
using StudioKit.Scaffolding.DataAccess.Interfaces;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Properties;
using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Services
{
	public class ArtifactLeasingService
	{
		private readonly IDateTimeProvider _dateTimeProvider;
		private readonly string _totpSecretKey;
		private readonly IBlobStorage _artifactStorage;
		private readonly IBaseDbContext _dbContext;
		private readonly Totp _totp;
		private static readonly VerificationWindow VerificationWindow = new VerificationWindow(previous: 1, future: 1);

		public ArtifactLeasingService(IBaseDbContext dbContext, IBlobStorage artifactStorage, IDateTimeProvider dateTimeProvider,
			string totpSecretKey, int totpDurationSeconds)
		{
			_dateTimeProvider = dateTimeProvider;
			_totpSecretKey = totpSecretKey;
			_artifactStorage = artifactStorage;
			_dbContext = dbContext;
			_totp = new Totp(Encoding.UTF8.GetBytes(totpSecretKey), totpDurationSeconds);
		}

		/// <summary>
		/// Associate a file artifact Id with a totp code, encrypting the payload
		/// for retrieval by a third party rendering service
		/// This returns token to formatted as "{salt}|{encryptedPayload}
		/// It formats encryptedPayload to as "{artifactId}|{totpCode}
		/// This method depends on having validated access to the artifact
		/// referenced in the artifact Id
		/// Base64 encoded strings have {/,+} replaced with {-,_} to accommodate inclusion in URLs
		/// </summary>
		/// <param name="artifactId">The id of the artifact to provide temporary access to</param>
		/// <returns>A token providing temporary access</returns>
		public string AssociateTotp(int artifactId)
		{
			var totpCode = _totp.ComputeTotp(_dateTimeProvider.UtcNow);
			var (salt, encryptedPayload) = Utilities.Encryption.EncryptStringAES($"{artifactId}|{totpCode}", _totpSecretKey);
			return $"{salt}|{encryptedPayload}".Replace('/', '-').Replace('+', '_');
		}

		/// <summary>
		/// Locate and return file artifact specified in <param name="token"></param>
		/// This expects token to be formatted as "{salt}|{encryptedPayload}
		/// It expects encryptedPayload to be formatted as "{artifactId}|{totpCode}
		/// Base64 encoded strings will need to have had {/,+} replaced with {-,_}
		/// </summary>
		/// <param name="token">Data indicating temporary access to artifact Id</param>
		/// <param name="cancellationToken">A <see cref="CancellationToken"/> from the caller</param>
		/// <returns>The specified <see cref="FileArtifact"/></returns>
		public async Task<FileArtifact> ArtifactFromTokenAsync(string token, CancellationToken cancellationToken = default(CancellationToken))
		{
			// These are all called from external services. EntityNotFound is all they need to know
			if (string.IsNullOrEmpty(token))
				throw new EntityNotFoundException();

			// separate the salt and encrypted payload
			if (token.IndexOf("|", StringComparison.Ordinal) == -1)
				throw new EntityNotFoundException();
			var substrings = token.Replace('-', '/').Replace('_', '+').Split('|');
			if (substrings.Length != 2)
				throw new EntityNotFoundException();
			var salt = substrings[0];
			var encryptedPayload = substrings[1];

			// decrypt and separate the artifact Id and totpCode
			var plaintext = Utilities.Encryption.DecryptStringAES(encryptedPayload, _totpSecretKey, Convert.FromBase64String(salt));
			if (plaintext.IndexOf("|", StringComparison.Ordinal) == -1)
				throw new EntityNotFoundException();
			substrings = plaintext.Split('|');
			if (substrings.Length != 2)
				throw new EntityNotFoundException();
			var artifactId = Convert.ToInt16(substrings[0]);
			var totpCode = substrings[1];

			// validate the totpCode
			if (!_totp.VerifyTotp(_dateTimeProvider.UtcNow, totpCode, out var _, VerificationWindow))
				throw new EntityNotFoundException();
			return await _dbContext.FindEntityAsync<FileArtifact>(artifactId, cancellationToken: cancellationToken);
		}

		///  <summary>
		///  Given an artifact, stream the contents to the provided writable stream
		/// 	(i.e. the HTTP output stream)
		///  </summary>
		///  <param name="artifact">The artifact from which to stream out of blob storage</param>
		///  <param name="stream">The stream to copy to</param>
		///  <param name="cancellationToken">A <see cref="CancellationToken"/> from the caller</param>
		public async Task StreamArtifactAsync(FileArtifact artifact, Stream stream, CancellationToken cancellationToken = default(CancellationToken))
		{
			if (stream == null)
				throw new ArgumentNullException(nameof(stream));
			if (artifact == null)
				throw new ArgumentNullException(nameof(artifact));
			if (!stream.CanWrite)
				throw new ApplicationException(Strings.StreamNotWritable);
			await ArtifactHelpers.StreamFileArtifactAsync(artifact, stream, _artifactStorage, cancellationToken);
		}
	}
}