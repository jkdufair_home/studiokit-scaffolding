﻿using Microsoft.AspNet.Identity.EntityFramework;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Interfaces.Authorization;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.DataAccess;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Services
{
	public class BaseUserService<TContext, TUser, TGroup, TConfiguration, TUserBusinessModel, TUserEditBusinessModel> : IBaseUserService<TUser, TUserBusinessModel, TUserEditBusinessModel>
		where TContext : BaseDbContext<TUser, TGroup, TConfiguration>
		where TUser : IdentityUser<string, UserLogin, UserRole, UserClaim>, IBaseUser, new()
		where TGroup : BaseGroup, new()
		where TConfiguration : BaseConfiguration, new()
		where TUserBusinessModel : BaseUserBusinessModel<TUser>, new()
		where TUserEditBusinessModel : BaseUserEditBusinessModel<TUser>, new()
	{
		private readonly TContext _dbContext;
		private readonly IUserManager<TUser> _userManager;
		private readonly IUserAuthorizationService _userAuthorizationService;

		public BaseUserService(TContext dbContext, IUserManager<TUser> userManager, IUserAuthorizationService userAuthorizationService)
		{
			_dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
			_userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
			_userAuthorizationService = userAuthorizationService ?? throw new ArgumentNullException(nameof(userAuthorizationService));
		}

		public Task<IEnumerable<TUserBusinessModel>> GetAllAsync(IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken), IDictionary<string, int> relationIds = null)
		{
			throw new NotSupportedException();
		}

		public async Task<TUserBusinessModel> GetAsync(string entityId, IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken), IDictionary<string, int> relationIds = null)
		{
			if (principal == null) throw new ArgumentNullException(nameof(principal));

			await _userAuthorizationService.AssertCanReadAsync(entityId, principal, cancellationToken);

			var user = await _userManager.FindByIdAsync(entityId);

			return new TUserBusinessModel
			{
				User = user,
				Roles = user.Roles.Join(_dbContext.RoleActivities, r => r.RoleId, ra => ra.RoleId, (n, ra) => ra.Role.Name)
					.Distinct(),
				Activities = user.Roles.Join(_dbContext.RoleActivities, r => r.RoleId, ra => ra.RoleId, (n, ra) => ra.Activity.Name)
					.Distinct()
			};
		}

		public Task<TUserBusinessModel> CreateAsync(TUserEditBusinessModel businessModel, IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken), IDictionary<string, int> relationIds = null)
		{
			throw new NotImplementedException();
		}

		public Task<TUserBusinessModel> UpdateAsync(string entityId, TUserEditBusinessModel businessModel, IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken), IDictionary<string, int> relationIds = null)
		{
			throw new NotImplementedException();
		}

		public Task DeleteAsync(string entityId, IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken), IDictionary<string, int> relationIds = null)
		{
			throw new NotSupportedException();
		}

		public async Task<IEnumerable<TUserBusinessModel>> GetAsync(IPrincipal principal, string keywords, CancellationToken cancellationToken = default(CancellationToken))
		{
			if (principal == null) throw new ArgumentNullException(nameof(principal));
			if (string.IsNullOrWhiteSpace(keywords)) throw new ArgumentNullException(nameof(keywords));

			await _userAuthorizationService.AssertCanReadAnyAsync(principal, cancellationToken);

			var keywordList = keywords.Split(' ').Select(k => k.ToLower()).ToList();

			var usersQueryable = _dbContext.Users.Where(u => keywordList.All(k =>
				u.Id.ToLower().Contains(k) ||
				u.UserName.ToLower().Contains(k) ||
				u.Email.ToLower().Contains(k) ||
				u.FirstName.ToLower().Contains(k) ||
				u.LastName.ToLower().Contains(k) ||
				u.EmployeeNumber.ToLower().Contains(k) ||
				u.Uid.ToLower().Contains(k)));

			var users = await usersQueryable.ToListAsync(cancellationToken);
			var userRoles = await usersQueryable
				.Join(_dbContext.IdentityUserRoles, u => u.Id, ur => ur.UserId, (u, ur) => new { UserId = u.Id, ur.RoleId })
				.Join(_dbContext.Roles, n => n.RoleId, r => r.Id, (n, r) => new { n.UserId, r.Name })
				.ToListAsync(cancellationToken);
			var userRoleActivities = await usersQueryable
				.Join(_dbContext.IdentityUserRoles, u => u.Id, ur => ur.UserId, (u, ur) => new { UserId = u.Id, ur.RoleId })
				.Join(_dbContext.RoleActivities, n => n.RoleId, ra => ra.RoleId, (n, ra) => new { n.UserId, ra.Activity.Name })
				.ToListAsync(cancellationToken);

			return users.Select(u => new TUserBusinessModel
			{
				User = u,
				Roles = userRoles.Where(ur => ur.UserId.Equals(u.Id)).Select(ur => ur.Name).Distinct(),
				Activities = userRoleActivities.Where(ur => ur.UserId.Equals(u.Id)).Select(ur => ur.Name).Distinct()
			});
		}
	}
}