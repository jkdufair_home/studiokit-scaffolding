﻿using Microsoft.AspNet.Identity.EntityFramework;
using StudioKit.Cloud.Storage.Queue;
using StudioKit.Data.Entity.Identity;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.ExternalProvider.Models.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Interfaces.Authorization;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.Common.Interfaces;
using StudioKit.Scaffolding.DataAccess;
using StudioKit.Scaffolding.DataAccess.Queues.QueueMessages;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Models.Interfaces;
using StudioKit.UserInfoService;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Services
{
	public class BaseGroupUserRoleService<TContext, TUser, TGroup, TConfiguration, TEntityUserRoleBusinessModel, TCreateEntityUserRoleBusinessModel> :
		EntityUserRoleService<TContext, TUser, TGroup, TConfiguration, GroupUserRole, TEntityUserRoleBusinessModel, TCreateEntityUserRoleBusinessModel>
		where TEntityUserRoleBusinessModel : GroupUserRoleBusinessModel<TUser>, new()
		where TCreateEntityUserRoleBusinessModel : CreateEntityUserRolesBusinessModel<TUser>, new()
		where TContext : BaseDbContext<TUser, TGroup, TConfiguration>
		where TUser : IdentityUser<string, UserLogin, UserRole, UserClaim>, IBaseUser, new()
		where TGroup : BaseGroup, new()
		where TConfiguration : BaseConfiguration, new()
	{
		private readonly TContext _dbContext;
		private readonly IQueueManager<SyncGroupRosterMessage> _syncGroupRosterQueueManager;

		public BaseGroupUserRoleService(
			TContext dbContext,
			IShardKeyProvider shardKeyProvider,
			IUserInfoService userInfoService,
			IEntityUserRoleAuthorizationService authorizationService,
			RoleManager roleManager,
			IQueueManager<SyncGroupRosterMessage> syncGroupRosterQueueManager) : base(dbContext, shardKeyProvider, userInfoService, authorizationService, roleManager,
			BaseRole.GroupRoles)
		{
			_dbContext = dbContext;
			_syncGroupRosterQueueManager = syncGroupRosterQueueManager ?? throw new ArgumentNullException(nameof(syncGroupRosterQueueManager));
		}

		protected override async Task<IEnumerable<TEntityUserRoleBusinessModel>> GetEntityUserRoleBusinessModelsAsync(int entityId, CancellationToken cancellationToken = default(CancellationToken))
		{
			return (await GetEntityQueryable(entityId)
					.Join(_dbContext.Users, gur => gur.UserId, u => u.Id, (eur, u) => new { GroupUserRole = eur, User = u })
					.Join(_dbContext.Roles, guru => guru.GroupUserRole.RoleId, r => r.Id,
						(guru, r) => new { guru.GroupUserRole, guru.User, Role = r }).ToListAsync(cancellationToken)).GroupBy(n => n.User.Id)
				.Select(g => new TEntityUserRoleBusinessModel
				{
					User = g.First().User,
					EntityId = entityId,
					Roles = g.Select(n => n.Role.Name).ToList(),
					IsExternal = g.Any(n => n.GroupUserRole.IsExternal)
				});
		}

		protected override IQueryable<GroupUserRole> GetEntityQueryable(int entityId)
		{
			return _dbContext.GroupUserRoles.Where(gur => gur.GroupId.Equals(entityId));
		}

		protected override GroupUserRole CreateEntityUserRole(int entityId)
		{
			return new GroupUserRole
			{
				GroupId = entityId
			};
		}

		protected override void OnBeforeAdd(GroupUserRole entityUserRole)
		{
			_dbContext.GroupUserRoleLogs.Add(new GroupUserRoleLog
			{
				UserId = entityUserRole.UserId,
				GroupId = entityUserRole.GroupId,
				RoleId = entityUserRole.RoleId,
				Type = GroupUserRoleLogType.Added
			});
		}

		protected override void OnBeforeAddRange(List<GroupUserRole> entityUserRoles)
		{
			_dbContext.GroupUserRoleLogs.AddRange(entityUserRoles.Select(entityUserRole => new GroupUserRoleLog
			{
				UserId = entityUserRole.UserId,
				GroupId = entityUserRole.GroupId,
				RoleId = entityUserRole.RoleId,
				Type = GroupUserRoleLogType.Added
			}));
		}

		protected override void OnBeforeDelete(int entityId, string userId, string roleId)
		{
			_dbContext.GroupUserRoleLogs.Add(new GroupUserRoleLog
			{
				UserId = userId,
				GroupId = entityId,
				RoleId = roleId,
				Type = GroupUserRoleLogType.Removed
			});
		}

		protected override async Task OnAfterDeleteAsync(int entityId, string userId, string roleName, string shardKey, CancellationToken cancellationToken = default(CancellationToken))
		{
			await RemoveExternalGroupsIfNeededAsync(entityId, userId, shardKey, cancellationToken);
		}

		#region Private Methods

		private async Task RemoveExternalGroupsIfNeededAsync(int groupId, string instructorUserId, string shardKey, CancellationToken cancellationToken = default(CancellationToken))
		{
			var externalGroups = await _dbContext.ExternalGroups
				.Where(eg =>
					eg.GroupId.Equals(groupId) &&
					eg.UserId.Equals(instructorUserId))
				.ToListAsync(cancellationToken);
			if (!externalGroups.Any()) return;

			var isRemovingRosterSyncedExternalGroups = await _dbContext.ExternalGroups
				.AnyAsync(eg =>
					eg.GroupId.Equals(groupId) &&
					eg.UserId.Equals(instructorUserId) &&
					eg.ExternalProvider.RosterSyncEnabled, cancellationToken);

			_dbContext.ExternalGroups.RemoveRange(externalGroups);
			await _dbContext.SaveChangesAsync(cancellationToken);

			if (!isRemovingRosterSyncedExternalGroups) return;
			await _syncGroupRosterQueueManager.AddMessageAsync(
				new SyncGroupRosterMessage
				{
					ShardKey = shardKey,
					GroupId = groupId
				}, cancellationToken: cancellationToken);
		}

		#endregion Private Methods
	}
}