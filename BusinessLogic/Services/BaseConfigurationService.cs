﻿using Microsoft.AspNet.Identity.EntityFramework;
using StudioKit.Caliper.Interfaces;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.ExternalProvider.Lti.Models.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.DataAccess;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Models.Interfaces;
using StudioKit.Scaffolding.Properties;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Services
{
	public class BaseConfigurationService<TContext, TUser, TGroup, TConfiguration> : IBaseConfigurationService<TConfiguration>
		where TContext : BaseDbContext<TUser, TGroup, TConfiguration>
		where TUser : IdentityUser<string, UserLogin, UserRole, UserClaim>, IBaseUser, new()
		where TGroup : BaseGroup, new()
		where TConfiguration : BaseConfiguration, new()
	{
		private readonly TContext _dbContext;

		public BaseConfigurationService(TContext dbContext)
		{
			_dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
		}

		public async Task<TConfiguration> GetConfigurationAsync(CancellationToken cancellationToken = default(CancellationToken))
		{
			var configuration = await _dbContext.Configurations.OrderByDescending(c => c.Id).FirstOrDefaultAsync(cancellationToken);
			if (configuration == null)
				throw new EntityNotFoundException(Strings.ConfigurationNotFound);
			return configuration;
		}

		public async Task<ICaliperConfiguration> GetCaliperConfigurationAsync(CancellationToken cancellationToken = default(CancellationToken))
		{
			return await GetConfigurationAsync(cancellationToken);
		}

		public async Task<ILtiConfiguration> GetLtiConfigurationAsync(CancellationToken cancellationToken = default(CancellationToken))
		{
			return await GetConfigurationAsync(cancellationToken);
		}
	}
}