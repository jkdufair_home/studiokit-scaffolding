using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.Scaffolding.Models.Interfaces;
using System.Security.Claims;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Services
{
	public class SignInManager<T> : SignInManager<T, string>
		where T : IdentityUser<string, UserLogin, UserRole, UserClaim>, IBaseUser, new()
	{
		public SignInManager(UserManager<T> userManager, IAuthenticationManager authenticationManager)
			: base(userManager, authenticationManager)
		{
		}

		public override Task<ClaimsIdentity> CreateUserIdentityAsync(T user)
		{
			return ((UserManager<T>)UserManager).CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
		}
	}
}