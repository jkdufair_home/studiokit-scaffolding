﻿using FluentValidation;
using StudioKit.ExternalProvider.Lti.DataAccess;
using StudioKit.ExternalProvider.Lti.Models;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Interfaces.Authorization;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.DataAccess.Interfaces;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Properties;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Services
{
	public class LtiLaunchService<TContext> : ILtiLaunchService
		where TContext : DbContext, IBaseDbContext, ILtiLaunchDbContext<GroupUserRole, GroupUserRoleLog, ExternalGroup, ExternalGroupUser>
	{
		private readonly TContext _dbContext;
		private readonly ILtiLaunchAuthorizationService _authorizationService;

		public LtiLaunchService(
			TContext dbContext,
			ILtiLaunchAuthorizationService authorizationService)
		{
			_dbContext = dbContext;
			_authorizationService = authorizationService;
		}

		public async Task<LtiLaunch> GetLtiLaunchAsync(int ltiLaunchId, IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken))
		{
			if (principal == null) throw new ArgumentNullException(nameof(principal));

			await _authorizationService.AssertCanReadAsync(ltiLaunchId, principal, cancellationToken);

			return await _dbContext.FindEntityAsync<LtiLaunch>(ltiLaunchId, cancellationToken: cancellationToken);
		}

		public async Task<(List<ExternalGroup>, List<LtiLaunch>)> GetExternalGroupsToAddAsync(
			int groupId,
			List<ExternalGroup> existingExternalGroups,
			IEnumerable<IExternalGroupEditBusinessModel> externalGroups,
			CancellationToken cancellationToken = default(CancellationToken))
		{
			var incomingLtiLaunches = externalGroups
				.OfType<LtiLaunchExternalGroupEditBusinessModel>()
				.ToList();
			if (!incomingLtiLaunches.Any())
				return (new List<ExternalGroup>(), new List<LtiLaunch>());

			var ltiLaunchIds = incomingLtiLaunches.Select(ug => ug.Id).ToList();
			var ltiLaunches = await _dbContext.LtiLaunches
				.Where(l => ltiLaunchIds.Contains(l.Id))
				.Include(l => l.ExternalProvider)
				.ToListAsync(cancellationToken);

			if (ltiLaunches.Any(ltiLaunch =>
				existingExternalGroups.Any(externalGroup =>
					externalGroup.ExternalProviderId.Equals(ltiLaunch.ExternalProviderId) &&
					externalGroup.ExternalId.Equals(ltiLaunch.ExternalId))))
				throw new ValidationException(string.Format(Strings.ExternalGroupAlreadyExists, nameof(LtiLaunch)));

			if (incomingLtiLaunches.Any(incoming =>
				ltiLaunches.Any(ltiLaunch =>
					ltiLaunch.Id.Equals(incoming.Id) &&
					!ltiLaunch.CreatedById.Equals(incoming.UserId))))
				throw new ValidationException(string.Format(Strings.ExternalGroupsInvalidUserIds, nameof(LtiLaunch)));

			if (incomingLtiLaunches.Any(incoming =>
				ltiLaunches.Any(ltiLaunch =>
					ltiLaunch.Id.Equals(incoming.Id) &&
					// IsAutoGradePushEnabled has value when it should not
					(incoming.IsAutoGradePushEnabled.HasValue &&
					(!ltiLaunch.ExternalProvider.GradePushEnabled ||
					ltiLaunch.GradesUrl == null ||
					ltiLaunch.GradesUrl.Trim() == string.Empty) ||
					// IsAutoGradePushEnabled has no value when it should
					!incoming.IsAutoGradePushEnabled.HasValue &&
					ltiLaunch.ExternalProvider.GradePushEnabled &&
					ltiLaunch.GradesUrl != null &&
					ltiLaunch.GradesUrl.Trim() != string.Empty))))
				throw new ValidationException(string.Format(Strings.ExternalGroupsInvalidAutoGradePushEnabled, nameof(LtiLaunch)));

			return (ltiLaunches
				.Select(ltiLaunch =>
					{
						var incoming = incomingLtiLaunches.First(i => i.Id.Equals(ltiLaunch.Id));
						return new ExternalGroup
						{
							GroupId = groupId,
							Name = ltiLaunch.Name,
							Description = ltiLaunch.Description,
							ExternalProviderId = ltiLaunch.ExternalProviderId,
							ExternalId = ltiLaunch.ExternalId,
							RosterUrl = ltiLaunch.RosterUrl,
							GradesUrl = ltiLaunch.GradesUrl,
							UserId = ltiLaunch.CreatedById,
							IsAutoGradePushEnabled = incoming.IsAutoGradePushEnabled
						};
					})
				.ToList(),
				ltiLaunches);
		}
	}
}