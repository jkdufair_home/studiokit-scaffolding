﻿using Microsoft.IdentityModel.Tokens;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.OpenSsl;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Services
{
	public class JsonWebKeyService : IJsonWebKeyService
	{
		private readonly ILtiConfigurationProvider _configurationService;

		public JsonWebKeyService(ILtiConfigurationProvider configurationService)
		{
			_configurationService = configurationService;
		}

		public async Task<JsonWebKeySet> GetJsonWebKeySetAsync(CancellationToken cancellationToken = default(CancellationToken))
		{
			var config = await _configurationService.GetLtiConfigurationAsync(cancellationToken);
			var keySet = new JsonWebKeySet();
			var publicKeyBytes = config.PublicRsaKey;

			if (publicKeyBytes == null || publicKeyBytes.Length == 0)
				return keySet;

			var publicKeyString = System.Text.Encoding.UTF8.GetString(publicKeyBytes);

			using (var publicKeyReader = new StringReader(publicKeyString))
			{
				var publicKeyParams = (RsaKeyParameters)new PemReader(publicKeyReader).ReadObject();

				var key = new JsonWebKey
				{
					Alg = "RS256",
					Kty = "RSA",
					Use = "sig",
					Kid = config.DateLastUpdated.Ticks.ToString(),
					E = Base64UrlEncoder.Encode(publicKeyParams.Exponent.ToByteArrayUnsigned()),
					N = Base64UrlEncoder.Encode(publicKeyParams.Modulus.ToByteArrayUnsigned())
				};

				keySet.Keys.Add(key);
				return keySet;
			}
		}
	}
}