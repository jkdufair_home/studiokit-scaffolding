﻿using Autofac;
using Autofac.Integration.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataHandler.Serializer;
using Microsoft.Owin.Security.Infrastructure;
using StudioKit.Security;
using StudioKit.Security.Entity.Identity.Interfaces;
using StudioKit.Security.Entity.Identity.Models;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Helpers;

namespace StudioKit.Scaffolding.BusinessLogic.Services.OAuth
{
	public class RefreshTokenProvider<TContext> : IAuthenticationTokenProvider
		where TContext : DbContext, IIdentityClientDbContext<AppClient, IdentityServiceClient, ServiceClientRole, RefreshToken>
	{
		private const int ExpirationMonths = 3;
		private readonly TicketSerializer _serializer = new TicketSerializer();

		public void Create(AuthenticationTokenCreateContext context)
		{
			throw new NotImplementedException();
		}

		public async Task CreateAsync(AuthenticationTokenCreateContext context)
		{
			var expires = DateTime.UtcNow.AddMonths(ExpirationMonths);

			//copy properties from the current context's ticket and set the desired lifetime of refresh token
			var refreshTokenProperties = new AuthenticationProperties(context.Ticket.Properties.Dictionary)
			{
				IssuedUtc = DateTime.UtcNow,
				ExpiresUtc = expires
			};
			var refreshTokenTicket = new AuthenticationTicket(context.Ticket.Identity, refreshTokenProperties);

			var guid = Guid.NewGuid().ToString();

			var refreshToken = Crypto.Hash(guid);
			var scope = context.OwinContext.GetAutofacLifetimeScope();
			var dbContext = scope.Resolve<TContext>();
			dbContext.RefreshTokens.Add(new RefreshToken
			{
				Token = refreshToken,
				AuthenticationTicket = _serializer.Serialize(refreshTokenTicket),
				AuthenticationTicketExpiration = expires
			});
			await dbContext.SaveChangesAsync();

			context.SetToken(refreshToken);
		}

		public void Receive(AuthenticationTokenReceiveContext context)
		{
			throw new NotImplementedException();
		}

		public async Task ReceiveAsync(AuthenticationTokenReceiveContext context)
		{
			var scope = context.OwinContext.GetAutofacLifetimeScope();
			var dbContext = scope.Resolve<TContext>();

			// Get the refresh token that is identified by the token in the context
			// Also get any expired tokens so they can be deleted (for cleanup purposes) along with the refresh token that is about to be regenerated
			// This seems to be a good trade-off between the RefreshToken table bloat and having to run a cleanup daemon
			var tokens =
				await
					dbContext.RefreshTokens.Where(
						t => t.Token == context.Token || t.AuthenticationTicketExpiration < DateTime.UtcNow)
						.ToListAsync();

			var refreshToken = tokens.SingleOrDefault(t => t.Token == context.Token);
			if (refreshToken != null)
			{
				var ticket = _serializer.Deserialize(refreshToken.AuthenticationTicket);
				if (ticket?.Properties.ExpiresUtc != null && ticket.Properties.ExpiresUtc > DateTime.UtcNow)
				{
					context.SetTicket(ticket);

					// delete used refreshToken
					dbContext.RefreshTokens.Remove(refreshToken);
					await dbContext.SaveChangesAsync();
				}
			}

			// delete expired tokens
			dbContext.RefreshTokens.RemoveRange(tokens.Where(t => t.Token != context.Token).ToList());
			await dbContext.SaveChangesAsync();
		}
	}
}