﻿using Microsoft.Owin.Security.DataHandler.Serializer;
using Microsoft.Owin.Security.Infrastructure;
using System;
using System.Threading.Tasks;
using System.Web.Security;

namespace StudioKit.Scaffolding.BusinessLogic.Services.OAuth
{
	public class AuthorizationCodeProvider : AuthenticationTokenProvider
	{
		public override async Task ReceiveAsync(AuthenticationTokenReceiveContext context)
		{
			try
			{
				var encryptedTicket = Convert.FromBase64String(context.Token);
				var serializedTicket = MachineKey.Unprotect(encryptedTicket);
				var serializer = new TicketSerializer();
				var ticket = serializer.Deserialize(serializedTicket);

				ticket.Properties.ExpiresUtc = DateTime.UtcNow.AddMinutes(1);
				var formVars = await context.Request.ReadFormAsync();
				ticket.Properties.Dictionary.Add("client_id", formVars["client_id"]);
				context.SetTicket(ticket);
			}
			catch (Exception)
			{
				// any failures here should be silent
				// most likely requests coming in that are not valid
			}
			//return Task.FromResult<object>(null);
		}
	}
}