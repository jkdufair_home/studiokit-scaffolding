﻿using Autofac;
using Autofac.Integration.Owin;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.Scaffolding.DataAccess.Interfaces;
using StudioKit.Security;
using StudioKit.Security.Entity.Identity.Interfaces;
using StudioKit.Security.Entity.Identity.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Services.OAuth
{
	public class ApplicationOAuthProvider<TContext> : OAuthAuthorizationServerProvider
		where TContext : DbContext, IBaseDbContext, IIdentityClientDbContext<AppClient, IdentityServiceClient, ServiceClientRole, RefreshToken>
	{
		private const string PublicClientId = "self";

		public override Task GrantAuthorizationCode(OAuthGrantAuthorizationCodeContext context)
		{
			context.Validated(context.Ticket);
			context.Request.Context.Authentication.SignIn(context.Ticket.Identity);
			return Task.FromResult<object>(null);
		}

		public override Task GrantRefreshToken(OAuthGrantRefreshTokenContext context)
		{
			var originalClient = context.Ticket.Properties.Dictionary["client_id"];
			var currentClient = context.OwinContext.Get<string>("as:client_id");

			if (originalClient != currentClient)
			{
				context.Rejected();
				return Task.FromResult<object>(null);
			}

			var newId = new ClaimsIdentity(context.Ticket.Identity);
			var newTicket = new AuthenticationTicket(newId, context.Ticket.Properties);
			context.Validated(newTicket);
			return Task.FromResult<object>(null);
		}

		public override async Task TokenEndpoint(OAuthTokenEndpointContext context)
		{
			foreach (var property in context.Properties.Dictionary)
			{
				context.AdditionalResponseParameters.Add(property.Key, property.Value);
			}

			if (!context.TokenEndpointRequest.GrantType.Equals("refresh_token"))
			{
				// Track Login
				var scope = context.OwinContext.GetAutofacLifetimeScope();
				var dbContext = scope.Resolve<TContext>();
				dbContext.Logins.Add(new Login
				{
					UserId = context.Identity.GetUserId(),
					IpAddress = context.OwinContext.Request.RemoteIpAddress,
					UserAgent = string.Join(" ", context.Request.Headers.GetValues("User-Agent"))
				});
				await dbContext.SaveChangesAsync();
			}
		}

		public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
		{
			if (!context.TryGetFormCredentials(out string id, out string secret))
				return Task.FromResult<object>(null);
			if (!ValidateClient(id, secret, context))
				return Task.FromResult<object>(null);
			context.OwinContext.Set("as:client_id", id);
			context.Validated();
			return Task.FromResult<object>(null);
		}

		private static bool ValidateClient(string id, string secret, OAuthValidateClientAuthenticationContext context)
		{
			var grantType = context.Parameters.Get("grant_type");
			if (string.IsNullOrWhiteSpace(grantType))
				return false;
			var scope = context.OwinContext.GetAutofacLifetimeScope();
			var dbContext = scope.Resolve<TContext>();
			return dbContext.AppClients.Any(c => c.ClientId == id && c.Secret == secret && c.Grants.Contains(grantType)) ||
				dbContext.ServiceClients.Any(c => c.ClientId == id && c.Secret == secret && c.Grants.Contains(grantType));
		}

		public override Task ValidateClientRedirectUri(OAuthValidateClientRedirectUriContext context)
		{
			if (context.ClientId != PublicClientId)
				return Task.FromResult<object>(null);

			var expectedRootUri = new Uri(context.Request.Uri, "/");
			if (expectedRootUri.AbsoluteUri == context.RedirectUri)
				context.Validated();

			return Task.FromResult<object>(null);
		}

		public override Task GrantClientCredentials(OAuthGrantClientCredentialsContext context)
		{
			var scope = context.OwinContext.GetAutofacLifetimeScope();
			var dbContext = scope.Resolve<TContext>();
			var serviceClientManager = scope.Resolve<ServiceClientManager>();

			var serviceClient = dbContext.ServiceClients.Single(c => c.ClientId == context.ClientId);
			var identity = serviceClientManager.CreateServiceClientIdentity(serviceClient);
			var ticket = new AuthenticationTicket(identity, new AuthenticationProperties());
			context.Validated(ticket);
			return base.GrantClientCredentials(context);
		}
	}

	/// <summary>
	/// Static helper methods for <see cref="ApplicationOAuthProvider{TContext}"/>
	/// </summary>
	public static class ApplicationOAuthProvider
	{
		public static AuthenticationProperties CreateProperties(string userName)
		{
			IDictionary<string, string> data = new Dictionary<string, string>
			{
				{"userName", userName}
			};
			return new AuthenticationProperties(data);
		}
	}
}