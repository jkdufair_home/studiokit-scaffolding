﻿using Microsoft.Owin.Security.OAuth;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Services.OAuth
{
	public class ApplicationOAuthBearerProvider : OAuthBearerAuthenticationProvider
	{
		public override Task ValidateIdentity(OAuthValidateIdentityContext context)
		{
			if (context == null)
				throw new ArgumentNullException(nameof(context));
			if (context.Ticket.Identity.Claims.All(c => c.Issuer != ClaimsIdentity.DefaultIssuer))
				context.Rejected();

			// TODO: figure out how to re-validate against external IdPs
			//var claimIdentities = context.Ticket.Identity.Claims
			//	.ToList()
			//	.GroupBy(c => c.Issuer)
			//	.Select(g => new ClaimsIdentity(g, context.Ticket.Identity.AuthenticationType))
			//	.ToArray();

			return Task.FromResult<object>(null);
		}
	}
}