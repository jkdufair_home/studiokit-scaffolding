﻿using Autofac;
using Autofac.Integration.Owin;
using NuGet;
using StudioKit.Security;
using StudioKit.Security.Entity.Identity.Interfaces;
using StudioKit.Security.Entity.Identity.Models;
using StudioKit.Security.Extensions;
using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace StudioKit.Scaffolding.BusinessLogic.Services
{
	public static class AppClientService
	{
		#region UserAgent Version Checks

		public static string AppName { get; set; }

		private static DateTime? _iosClientLastLoaded;
		private static SemanticVersion _iosSemanticMinVersion;

		private static DateTime? _androidClientLastLoaded;
		private static SemanticVersion _androidSemanticMinVersion;

		/// <summary>
		/// Checks if a UserAgent contains iOS specific strings.
		/// e.g. Pattern/1.3.0/254 (iPhone; iPhone 6 Plus; Global+GSM+CDMA; A1522 / A1524 / A1593; iOS 8.1.3; Scale/3.00)
		/// </summary>
		/// <param name="userAgent">The UserAgent of an HttpRequest</param>
		/// <returns>true if the UserAgent is from an iOS App</returns>
		public static bool IsUserAgentIOS(string userAgent)
		{
			if (string.IsNullOrEmpty(AppName))
				throw new NullReferenceException($"{nameof(AppName)} must have a value");
			return userAgent.Contains(AppName) && userAgent.Contains("; iOS ");
		}

		public static bool IsUserAgentAndroid(string userAgent)
		{
			if (string.IsNullOrEmpty(AppName))
				throw new NullReferenceException($"{nameof(AppName)} must have a value");
			return userAgent.Contains(AppName) && userAgent.Contains("Android");
		}

		public static bool IsIOSUserAgentVersionSupported(HttpRequest request)
		{
			var userAgent = HttpContext.Current.Request.UserAgent;
			var scope = HttpContext.Current.GetOwinContext().GetAutofacLifetimeScope();
			var db = scope.Resolve<IIdentityClientDbContext<AppClient, IdentityServiceClient, ServiceClientRole, RefreshToken>>();

			if (userAgent == null || db == null || !IsUserAgentIOS(userAgent))
				return true;

			// Get Client to check min version support
			if (_iosClientLastLoaded == null || DateTime.UtcNow - _iosClientLastLoaded.Value >= new TimeSpan(0, 10, 0))
			// 10 minutes
			{
				// TODO - CACHING: Cache Clients somewhere other than static var
				var iosClient = db.AppClients.FirstOrDefault(c => c.ClientId.Equals("iOS"));
				if (iosClient == null)
					return true;
				_iosClientLastLoaded = DateTime.UtcNow;
				_iosSemanticMinVersion = iosClient.SemanticMinVersion();
			}

			if (_iosSemanticMinVersion == null)
				return true;

			var comparisonResult = UserAgentVersionCompare(userAgent, _iosSemanticMinVersion);
			return comparisonResult >= 0;
		}

		public static bool IsAndroidUserAgentVersionSupported(HttpRequest request)
		{
			var userAgent = HttpContext.Current.Request.UserAgent;
			var scope = HttpContext.Current.GetOwinContext().GetAutofacLifetimeScope();
			var db = scope.Resolve<IIdentityClientDbContext<AppClient, IdentityServiceClient, ServiceClientRole, RefreshToken>>();

			if (db == null || userAgent == null || !IsUserAgentAndroid(userAgent) && !userAgent.Contains("Dalvik"))
				return true;

			// Get Client to check min version support
			if (_androidClientLastLoaded == null || DateTime.UtcNow - _androidClientLastLoaded.Value >= new TimeSpan(0, 10, 0))
			// 10 minutes
			{
				// TODO - CACHING: - Cache Clients somewhere other than static var
				var androidClient = db.AppClients.FirstOrDefault(c => c.ClientId.Equals("android"));
				if (androidClient == null)
					return true;
				_androidClientLastLoaded = DateTime.UtcNow;
				_androidSemanticMinVersion = androidClient.SemanticMinVersion();
			}

			if (_androidSemanticMinVersion == null)
				return true;

			var comparisonResult = UserAgentVersionCompare(userAgent, _androidSemanticMinVersion);
			return comparisonResult >= 0;
		}

		/// <summary>
		/// Compare the Version included in an iOS UserAgent string to a given SemanticVersion.
		/// Returns -1 if the UserAgent Version is less than comparisonVersion, or if it could not be found.
		/// Returns 0 if the UserAgent Version could not be parsed, or if the versions match.
		/// Returns 1 if the UserAgent Version is greater than comparisonVersion.
		/// </summary>
		/// <param name="userAgent">The UserAgent of an HttpRequest</param>
		/// <param name="comparisionVersion">The version against to compare the UserAgent Version</param>
		/// <returns></returns>
		public static int UserAgentVersionCompare(string userAgent, SemanticVersion comparisionVersion)
		{
			if (userAgent == null || (!IsUserAgentIOS(userAgent) && !IsUserAgentAndroid(userAgent)))
				return 0;

			// From UserAgent match "Name/Version/Build" or "Name/Build"
			// e.g. "Pattern/1.3.0/254 (iPhone..." or "Pattern/218 (iPad..."
			var matchVersionBuild = Regex.Match(userAgent, @"^[a-zA-Z ]*/(.*) \(");
			if (!matchVersionBuild.Success || matchVersionBuild.Groups.Count != 2)
				// Could not match it, let it through. it didn't come from our app
				return 0;

			// From "Name/Version/Build" or "Name/Build" try and find "Version"
			var matchVersion = Regex.Match(matchVersionBuild.Groups[1].Value, @"^(.*)/");
			if (!matchVersion.Success || matchVersion.Groups.Count != 2)
				// has "Name/Build", but no "Version", automatically not supported (everything before iOS version 1.3.0)
				return -1;

			SemanticVersion.TryParse(matchVersion.Groups[1].ToString(), out var version);
			return version?.Version.CompareTo(comparisionVersion.Version) ?? -1;
		}

		#endregion UserAgent Version Checks
	}
}