﻿using FluentValidation;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using StudioKit.Configuration;
using StudioKit.Data.Entity.Identity;
using StudioKit.Diagnostics;
using StudioKit.Encryption;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.Common;
using StudioKit.Scaffolding.Service.Models.RequestModels;
using StudioKit.UserInfoService;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace StudioKit.Scaffolding.BusinessLogic.Services
{
	public class PurdueCasService
	{
		public const string CasLoginProvider = "https://www.purdue.edu/apps/account/cas";
		private readonly ILogger _logger;
		private readonly IUserInfoService _userInfoService;

		public PurdueCasService(ILogger logger, IUserInfoService userInfoService)
		{
			_logger = logger;
			_userInfoService = userInfoService;
		}

		public async Task<ExternalLoginInfo> ValidateV1CredentialsAsync(LoginRequestModel model, CancellationToken cancellationToken = default(CancellationToken))
		{
			if (model == null)
				throw new ArgumentNullException(nameof(model));

			var ticketUrl = new Uri($"{CasLoginProvider}/v1/tickets");
			var ticketParameters = new NameValueCollection
			{
				{"username", model.Username},
				{"password", model.Password}
			};
			var webClient = new WebClient();
			try
			{
				await webClient.UploadValuesTaskAsync(ticketUrl, "POST", ticketParameters);
			}
			catch (WebException)
			{
				throw new ValidationException("Invalid career account password");
			}

			var casUser = new PurdueCasUser
			{
				Login = model.Username,
				EmailAddress = $"{model.Username}@purdue.edu"
			};

			var userInfo = _userInfoService.GetUser(casUser.Login);
			if (userInfo != null)
			{
				casUser.FirstName = userInfo.FirstName;
				casUser.LastName = userInfo.LastName;
				casUser.Puid = userInfo.Puid;
				casUser.EmailAddress = userInfo.Email;
			}

			var externalLoginInfo = GetExternalLoginInfo(casUser);

			var tier = EncryptedConfigurationManager.GetSetting(BaseAppSetting.Tier);
			if (tier != Tier.Prod)
			{
				// Setup user as a Instructor on LOCAL/DEV
				externalLoginInfo.ExternalIdentity.AddClaim(new Claim(ClaimTypes.Role, BaseRole.Creator));
			}
			else
			{
				var superAdminEmailAddresses =
					new List<string>(EncryptedConfigurationManager.GetSetting(BaseAppSetting.SuperAdminEmailAddresses).Split(','));
				if (!superAdminEmailAddresses.Contains(casUser.EmailAddress))
					throw new ForbiddenException();
				// Setup user as a SuperAdmin on PROD
				externalLoginInfo.ExternalIdentity.AddClaim(new Claim(ClaimTypes.Role, BaseRole.SuperAdmin));
			}

			return externalLoginInfo;
		}

		public async Task<ExternalLoginInfo> ValidateTicketAsync(string ticket, string service, CancellationToken cancellationToken = default(CancellationToken))
		{
			if (string.IsNullOrEmpty(ticket))
				throw new ArgumentNullException(nameof(ticket));
			if (string.IsNullOrEmpty(service))
				throw new ArgumentNullException(nameof(service));

			_logger.Debug($"CAS Ticket: {ticket}");
			var validateUrl = new Uri($"{CasLoginProvider}/serviceValidate?ticket={ticket}&service={service}");
			var webClient = new WebClient();
			var result = await webClient.DownloadStringTaskAsync(validateUrl);
			_logger.Info($"Ticket validation result: {result}");
			XNamespace ns = "http://www.yale.edu/tp/cas";
			var parsedDocument = XDocument.Parse(result);

			if (parsedDocument.Descendants().Any(n => n.Name == ns + "authenticationFailure"))
				throw new Exception("Could not validate CAS ticket");

			var userNode =
				parsedDocument
					.Descendants(ns + "serviceResponse")
					.Descendants(ns + "authenticationSuccess")
					.Descendants(ns + "attributes")
					.ToList();
			var login = userNode.Descendants(ns + "login").First().Value;
			// remove middle initial from first name
			var firstName = Regex.Replace(userNode.Descendants(ns + "firstname").First().Value, " .$", string.Empty);
			var lastName = userNode.Descendants(ns + "lastname").First().Value;
			var emailAddress = userNode.Descendants(ns + "email").First().Value;
			var puid = userNode.Descendants(ns + "puid").First().Value;
			var i2A2Characteristics = userNode.Descendants(ns + "i2a2characteristics").First().Value.Split(',');
			var casUser = new PurdueCasUser
			{
				Login = login,
				FirstName = firstName,
				LastName = lastName,
				EmailAddress = emailAddress,
				Puid = puid,
				I2A2Characteristics = i2A2Characteristics
			};
			return GetExternalLoginInfo(casUser);
		}

		private static ExternalLoginInfo GetExternalLoginInfo(PurdueCasUser purdueCasUser)
		{
			var claims = new List<Claim>();

			if (!string.IsNullOrWhiteSpace(purdueCasUser.Login))
			{
				// Common
				claims.Add(new Claim(ClaimTypes.Name, purdueCasUser.Login, ClaimValueTypes.String, CasLoginProvider));
				// CAS
				claims.Add(new Claim(ScaffoldingClaimTypes.CasLogin, purdueCasUser.Login, ClaimValueTypes.String, CasLoginProvider));
			}

			if (!string.IsNullOrWhiteSpace(purdueCasUser.Puid))
			{
				// Common
				claims.Add(new Claim(ClaimTypes.NameIdentifier, purdueCasUser.Puid, ClaimValueTypes.String, CasLoginProvider));
				// CAS
				claims.Add(new Claim(ScaffoldingClaimTypes.CasPuid, purdueCasUser.Puid, ClaimValueTypes.String, CasLoginProvider));
			}

			if (!string.IsNullOrWhiteSpace(purdueCasUser.EmailAddress))
				claims.Add(new Claim(ClaimTypes.Email, purdueCasUser.EmailAddress, ClaimValueTypes.String, CasLoginProvider));

			if (!string.IsNullOrWhiteSpace(purdueCasUser.FirstName))
				claims.Add(new Claim(ClaimTypes.GivenName, purdueCasUser.FirstName, ClaimValueTypes.String, CasLoginProvider));

			if (!string.IsNullOrWhiteSpace(purdueCasUser.LastName))
				claims.Add(new Claim(ClaimTypes.Surname, purdueCasUser.LastName, ClaimValueTypes.String, CasLoginProvider));

			if (purdueCasUser.I2A2Characteristics != null && purdueCasUser.I2A2Characteristics.Any())
			{
				claims.Add(new Claim(ScaffoldingClaimTypes.CasI2A2Characteristics, string.Join(",", purdueCasUser.I2A2Characteristics),
					ClaimValueTypes.String, CasLoginProvider));

				// Application Role
				if (PurdueI2A2Characteristics.IsPurdueInstructor(purdueCasUser.I2A2Characteristics))
					claims.Add(new Claim(ClaimTypes.Role, BaseRole.Creator));
			}

			return new ExternalLoginInfo
			{
				DefaultUserName = purdueCasUser.EmailAddress,
				Email = purdueCasUser.EmailAddress,
				ExternalIdentity = new ClaimsIdentity(claims),
				Login = new UserLoginInfo(CasLoginProvider, purdueCasUser.Login)
			};
		}
	}
}