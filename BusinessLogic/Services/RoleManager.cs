using Microsoft.AspNet.Identity;
using StudioKit.Data.Entity.Identity.Models;

namespace StudioKit.Scaffolding.BusinessLogic.Services
{
	public class RoleManager : RoleManager<Role>
	{
		public RoleManager(RoleStore store) : base(store)
		{
		}
	}
}