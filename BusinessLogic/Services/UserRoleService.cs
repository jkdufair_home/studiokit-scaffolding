﻿using FluentValidation;
using Microsoft.AspNet.Identity.EntityFramework;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Interfaces.Authorization;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.Common.Interfaces;
using StudioKit.Scaffolding.DataAccess;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Models.Interfaces;
using StudioKit.Scaffolding.Properties;
using StudioKit.UserInfoService;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Services
{
	public class UserRoleService<TContext, TUser, TGroup, TConfiguration, TUserRoleBusinessModel> : IUserRoleService<TUser, TUserRoleBusinessModel>
		where TContext : BaseDbContext<TUser, TGroup, TConfiguration>
		where TUser : IdentityUser<string, UserLogin, UserRole, UserClaim>, IBaseUser, new()
		where TGroup : BaseGroup, new()
		where TConfiguration : BaseConfiguration, new()
		where TUserRoleBusinessModel : UserRoleBusinessModel<TUser>, new()
	{
		private readonly TContext _dbContext;
		private readonly IShardKeyProvider _shardKeyProvider;
		private readonly IUserInfoService _userInfoService;
		private readonly IUserRoleAuthorizationService _authorizationService;
		private readonly RoleManager _roleManager;

		public UserRoleService(TContext dbContext,
			IShardKeyProvider shardKeyProvider,
			IUserInfoService userInfoService,
			IUserRoleAuthorizationService authorizationService,
			RoleManager roleManager)
		{
			_dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
			_shardKeyProvider = shardKeyProvider ?? throw new ArgumentNullException(nameof(shardKeyProvider));
			_userInfoService = userInfoService ?? throw new ArgumentNullException(nameof(userInfoService));
			_authorizationService = authorizationService ?? throw new ArgumentNullException(nameof(authorizationService));
			_roleManager = roleManager ?? throw new ArgumentNullException(nameof(roleManager));
		}

		public async Task<IEnumerable<TUserRoleBusinessModel>> GetUserRolesAsync(string roleName, IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken))
		{
			if (principal == null) throw new ArgumentNullException(nameof(principal));
			if (string.IsNullOrEmpty(roleName)) throw new ArgumentNullException(nameof(roleName));
			await _authorizationService.AssertCanReadAnyAsync(principal, cancellationToken);

			var role = await _roleManager.FindByNameAsync(roleName);
			if (role == null)
				throw new EntityNotFoundException(string.Format(Strings.RoleNotFound, roleName));

			return await GetUserRoleBusinessModelAsync(roleName, cancellationToken);
		}

		public async Task<CreateUserRolesBusinessModel<TUser>> CreateUserRolesAsync(string roleName, List<string> identifiers, IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken))
		{
			if (principal == null) throw new ArgumentNullException(nameof(principal));
			if (identifiers == null || !identifiers.Any())
				throw new ArgumentNullException(nameof(identifiers));
			if (string.IsNullOrWhiteSpace(roleName)) throw new ArgumentNullException(nameof(roleName));

			await _authorizationService.AssertCanCreateAsync(principal, cancellationToken);

			var role = await _roleManager.FindByNameAsync(roleName);
			if (role == null)
				throw new EntityNotFoundException(string.Format(Strings.RoleNotFound, roleName));

			var shardKey = GetShardKey();
			var configuration = await _dbContext.GetConfigurationAsync(cancellationToken);
			var usersResult = await _dbContext.GetOrCreateUsersByIdentifiersAsync(
				identifiers, configuration, shardKey, _userInfoService, cancellationToken);
			var userIds = usersResult.Users.Select(user => user.Id).ToList();

			var roleId = (await _roleManager.FindByNameAsync(roleName)).Id;
			// find users requested to be added that already have the role
			var existingUsersInRole = await _dbContext.IdentityUserRoles.Where(iur => userIds.Contains(iur.UserId)
				&& iur.RoleId == roleId)
				.Select(iur => iur.UserId)
				.ToListAsync(cancellationToken);

			// add role to users that did not already have it
			var newUserIds = userIds.Except(existingUsersInRole).ToList();
			if (newUserIds.Any())
			{
				_dbContext.IdentityUserRoles.AddRange(newUserIds.Select(userId => new UserRole { RoleId = role.Id, UserId = userId }));
				await _dbContext.SaveChangesAsync(cancellationToken);
			}

			return new CreateUserRolesBusinessModel<TUser>
			{
				AddedUsers = usersResult.Users.Where(u => newUserIds.Contains(u.Id)),
				ExistingUsers = usersResult.Users.Where(u => !newUserIds.Contains(u.Id)),
				AllowedDomains = usersResult.AllowedDomains,
				InvalidIdentifiers = usersResult.InvalidIdentifiers,
				InvalidDomainIdentifiers = usersResult.InvalidDomainIdentifiers
			};
		}

		public async Task DeleteUserRoleAsync(string userId, string roleName, IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken))
		{
			if (string.IsNullOrWhiteSpace(userId)) throw new ArgumentNullException(nameof(userId));
			if (string.IsNullOrWhiteSpace(roleName)) throw new ArgumentNullException(nameof(roleName));
			if (principal == null) throw new ArgumentNullException(nameof(principal));

			await _authorizationService.AssertCanDeleteAnyAsync(principal, cancellationToken);

			var role = await _roleManager.FindByNameAsync(roleName);
			if (role == null)
				throw new EntityNotFoundException(string.Format(Strings.RoleNotFound, roleName));

			var userRole = await _dbContext.IdentityUserRoles
				.SingleOrDefaultAsync(iur => iur.UserId.Equals(userId) && iur.RoleId.Equals(role.Id), cancellationToken);
			if (userRole == null)
				return;

			_dbContext.IdentityUserRoles.Remove(userRole);
			await _dbContext.SaveChangesAsync(cancellationToken);
		}

		#region Private Methods

		private async Task<IEnumerable<TUserRoleBusinessModel>> GetUserRoleBusinessModelAsync(string roleName, CancellationToken cancellationToken = default(CancellationToken))
		{
			return (await _dbContext.Roles.Where(r => r.Name == roleName)
					.Join(_dbContext.IdentityUserRoles, r => r.Id, iur => iur.RoleId, (r, iur) => new { RoleName = r.Name, iur.UserId })
					.Join(_dbContext.Users, o => o.UserId, u => u.Id, (o, u) => new { o.RoleName, User = u }).ToListAsync(cancellationToken))
				.GroupBy(o => o.User)
				.Select(g => new TUserRoleBusinessModel { User = g.Key, Roles = g.Select(o => o.RoleName).ToList() });
		}

		private string GetShardKey()
		{
			var shardKey = _shardKeyProvider.GetShardKey();
			if (string.IsNullOrWhiteSpace(shardKey))
				throw new ValidationException(Strings.ShardKeyRequired);
			return shardKey;
		}

		#endregion Private Methods
	}
}