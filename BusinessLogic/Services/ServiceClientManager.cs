﻿using Microsoft.AspNet.Identity;
using StudioKit.Security;
using StudioKit.Security.Entity.Identity.Interfaces;
using StudioKit.Security.Entity.Identity.Models;
using System;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Services
{
	public class ServiceClientManager : IDisposable
	{
		private readonly IIdentityClientDbContext<AppClient, IdentityServiceClient, ServiceClientRole, RefreshToken> _dbContext;

		public ServiceClientManager(IIdentityClientDbContext<AppClient, IdentityServiceClient, ServiceClientRole, RefreshToken> dbContext)
		{
			_dbContext = dbContext;
		}

		public ClaimsIdentity CreateServiceClientIdentity(IdentityServiceClient serviceClient, string authenticationType = DefaultAuthenticationTypes.ApplicationCookie)
		{
			var identity = new ClaimsIdentity(new[]
				{
					new Claim(ClaimTypes.NameIdentifier, serviceClient.ClientId)
				},
				authenticationType);
			var roles = serviceClient.Roles.Select(r => r.Role).ToList();
			foreach (var role in roles)
			{
				identity.AddClaim(new Claim(ClaimTypes.Role, role.Name));
			}
			return identity;
		}

		public void Dispose()
		{
		}

		public async Task<string> GetClientIdForNameAsync(string name, CancellationToken cancellationToken = default(CancellationToken))
		{
			var clientId = name
				.ToLowerInvariant()
				.Replace(" ", "-");
			clientId = clientId.Substring(0, Math.Min(clientId.Length, 100));

			var id = clientId;
			var clientIdExists = await _dbContext.ServiceClients.AnyAsync(c => c.ClientId == id, cancellationToken);
			while (clientIdExists)
			{
				var suffix = $"-{Guid.NewGuid()}";
				clientId = $"{clientId.Substring(0, Math.Min(clientId.Length, 100 - suffix.Length))}{suffix}";
				clientIdExists = await _dbContext.ServiceClients.AnyAsync(c => c.ClientId == id, cancellationToken);
			}
			return clientId;
		}
	}
}