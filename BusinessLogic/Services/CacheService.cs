﻿using EFCache.Redis;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Interfaces.Authorization;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Services
{
	public class CacheService : ICacheService
	{
		private readonly DbContext _dbContext;
		private readonly ICacheAuthorizationService _cacheAuthorizationService;
		//private readonly IShardedCache _shardedCache;

		public CacheService(DbContext dbContext, ICacheAuthorizationService cacheAuthorizationService
			//IShardedCache shardedCache
			)
		{
			_dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
			_cacheAuthorizationService = cacheAuthorizationService ?? throw new ArgumentNullException(nameof(cacheAuthorizationService));
			//_shardedCache = shardedCache ?? throw new ArgumentNullException(nameof(shardedCache));
		}

		public async Task<IEnumerable<QueryStatistics>> GetCacheStatisticsAsync(IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken))
		{
			if (principal == null) throw new ArgumentNullException(nameof(principal));

			await _cacheAuthorizationService.AssertCanReadStatisticsAsync(principal, cancellationToken);

			//var cache = _shardedCache.GetShard(_dbContext.Database.Connection.Database);
			//var redisCache = (IRedisCache)cache;
			//return redisCache?.GetStatistics();
			return new List<QueryStatistics>();
		}

		public async Task PurgeCacheAsync(IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken))
		{
			if (principal == null) throw new ArgumentNullException(nameof(principal));

			await _cacheAuthorizationService.AssertCanPurgeAsync(principal, cancellationToken);

			// TODO implement cache purge in EFCache

			throw new NotImplementedException();
		}
	}
}