﻿using Microsoft.AspNet.Identity.EntityFramework;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Interfaces.Authorization;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.DataAccess;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Services
{
	public class ExternalProviderService<TContext, TUser, TGroup, TConfiguration> : IExternalProviderService
		where TContext : BaseDbContext<TUser, TGroup, TConfiguration>
		where TUser : IdentityUser<string, UserLogin, UserRole, UserClaim>, IBaseUser, new()
		where TGroup : BaseGroup, new()
		where TConfiguration : BaseConfiguration, new()
	{
		private readonly TContext _dbContext;
		private readonly IExternalProviderAuthorizationService _externalProviderAuthorizationService;

		public ExternalProviderService(
			TContext dbContext,
			IExternalProviderAuthorizationService externalProviderAuthorizationService)
		{
			_dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
			_externalProviderAuthorizationService = externalProviderAuthorizationService ?? throw new ArgumentNullException(nameof(externalProviderAuthorizationService));
		}

		public async Task<IEnumerable<ExternalProvider.Models.ExternalProvider>> GetAllAsync(IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken), IDictionary<string, int> relationIds = null)
		{
			if (principal == null) throw new ArgumentNullException(nameof(principal));
			await _externalProviderAuthorizationService.AssertCanReadAnyAsync(principal, cancellationToken);
			return await _dbContext.ExternalProviders.ToListAsync(cancellationToken);
		}

		public Task<ExternalProvider.Models.ExternalProvider> GetAsync(int entityId, IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken), IDictionary<string, int> relationIds = null)
		{
			throw new NotImplementedException();
		}

		public Task<ExternalProvider.Models.ExternalProvider> CreateAsync(ExternalProviderBusinessModel businessModel, IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken), IDictionary<string, int> relationIds = null)
		{
			throw new NotImplementedException();
		}

		public Task<ExternalProvider.Models.ExternalProvider> UpdateAsync(int entityId, ExternalProviderBusinessModel businessModel, IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken), IDictionary<string, int> relationIds = null)
		{
			throw new NotImplementedException();
		}

		public Task DeleteAsync(int entityId, IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken), IDictionary<string, int> relationIds = null)
		{
			throw new NotImplementedException();
		}
	}
}