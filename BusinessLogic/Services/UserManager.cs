﻿using FluentValidation;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.DataHandler.Serializer;
using StudioKit.Data.Entity.Identity;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.Common;
using StudioKit.Scaffolding.DataAccess;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Models.Interfaces;
using StudioKit.Shibboleth;
using StudioKit.Utilities.Extensions;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Security;

namespace StudioKit.Scaffolding.BusinessLogic.Services
{
	/// <summary>
	/// The application user manager used in this application. UserManager is defined in ASP.NET Identity and is used by the application.
	/// </summary>
	public class UserManager<TContext, TUser, TGroup, TConfiguration> : UserManager<TUser>, IUserManager<TUser>
		where TContext : BaseDbContext<TUser, TGroup, TConfiguration>
		where TUser : IdentityUser<string, UserLogin, UserRole, UserClaim>, IBaseUser, new()
		where TGroup : BaseGroup, new()
		where TConfiguration : BaseConfiguration, new()
	{
		private readonly TContext _dbContext;
		private readonly IInstructorSandboxService<TUser> _instructorSandboxService;
		private readonly IUserSetupService<TUser> _userSetupService;

		public UserManager(
			IUserStore<TUser> store,
			IdentityFactoryOptions<UserManager<TContext, TUser, TGroup, TConfiguration>> options,
			TContext dbContext,
			IInstructorSandboxService<TUser> instructorSandboxService,
			IUserSetupService<TUser> userSetupService)
			: base(store)
		{
			// Configure validation logic for usernames
			UserValidator = new UserValidator<TUser>(this)
			{
				AllowOnlyAlphanumericUserNames = false,
				RequireUniqueEmail = true
			};

			// Configure validation logic for passwords
			PasswordValidator = new PasswordValidator
			{
				RequiredLength = 6,
				RequireNonLetterOrDigit = true,
				RequireDigit = true,
				RequireLowercase = true,
				RequireUppercase = true,
			};

			// Configure user lockout defaults
			UserLockoutEnabledByDefault = true;
			DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
			MaxFailedAccessAttemptsBeforeLockout = 5;

			// Register two factor authentication providers. This application uses Phone and Emails as a step of receiving a code for verifying the user
			// You can write your own provider and plug it in here.
			//RegisterTwoFactorProvider("Phone Code", new PhoneNumberTokenProvider<T>
			//{
			//	MessageFormat = "Your security code is {0}"
			//});
			//RegisterTwoFactorProvider("Email Code", new EmailTokenProvider<T>
			//{
			//	Subject = "Security Code",
			//	BodyFormat = "Your security code is {0}"
			//});
			//EmailService = new EmailService();
			SmsService = new SmsService();

			var dataProtectionProvider = options.DataProtectionProvider;
			if (dataProtectionProvider != null)
			{
				UserTokenProvider =
					new DataProtectorTokenProvider<TUser>(dataProtectionProvider.Create("ASP.NET Identity"));
			}

			_dbContext = dbContext;
			_instructorSandboxService = instructorSandboxService;
			_userSetupService = userSetupService;
		}

		public IList<string> GetRoles(string userId)
		{
			return GetRolesAsync(userId).ConfigureAwait(false).GetAwaiter().GetResult();
		}

		public TUser FindById(string userId)
		{
			return FindByIdAsync(userId).ConfigureAwait(false).GetAwaiter().GetResult();
		}

		public override async Task<TUser> FindByEmailAsync(string email)
		{
			return await Users.FirstOrDefaultAsync(x => x.Email == email);
		}

		#region Authentication

		private async Task AddUserToRolesAsync(TUser user, IEnumerable<Claim> roleClaims)
		{
			await AddToRolesAsync(user.Id, roleClaims.Select(r => r.Value).ToArray());
		}

		private async Task StoreClaimsAsync(TUser user, List<Claim> claims, CancellationToken cancellationToken = default(CancellationToken))
		{
			var currentClaims = await _dbContext.IdentityUserClaims.Where(c => c.UserId == user.Id).ToListAsync(cancellationToken);
			// remove current claims that have a match (type and issuer) in the new set
			var claimsToRemove = currentClaims.Where(cc => claims.Any(c => c.Type == cc.ClaimType && c.Issuer == cc.ClaimIssuer)).ToList();
			// add new claims that do not have a match in remaining current claims
			var claimsToAdd = claims.Where(c => currentClaims.Except(claimsToRemove).All(cc => cc.ClaimType != c.Type && cc.ClaimIssuer != c.Issuer))
				.Select(c => new UserClaim
				{
					UserId = user.Id,
					ClaimType = c.Type,
					ClaimValue = c.Value,
					ClaimIssuer = c.Issuer,
					ClaimOriginalIssuer = c.OriginalIssuer,
					ClaimValueType = c.ValueType
				}).ToList();

			_dbContext.IdentityUserClaims.RemoveRange(claimsToRemove);
			_dbContext.IdentityUserClaims.AddRange(claimsToAdd);
			await _dbContext.SaveChangesAsync(cancellationToken);
		}

		public async Task<string> FinishSignInAsync(TUser user, List<Claim> claims, UserLoginInfo userLoginInfo, string shardKey, CancellationToken cancellationToken = default(CancellationToken))
		{
			if (user == null) throw new ArgumentNullException(nameof(user));
			if (string.IsNullOrWhiteSpace(shardKey)) throw new ArgumentNullException(nameof(shardKey));

			// Update User
			UpdateUserWithClaims(user, claims);
			var updatedUser = await CreateOrUpdateAsync(user, cancellationToken);

			// Store UserLoginInfo
			if (userLoginInfo != null)
			{
				if (updatedUser.Logins.All(x => x.LoginProvider != userLoginInfo.LoginProvider))
				{
					var result = await AddLoginAsync(updatedUser.Id, userLoginInfo);
					if (result == null)
						throw new Exception();
					if (!result.Succeeded)
						throw new ValidationException(result.Errors != null ? string.Join(",", result.Errors) : null);
				}
			}

			// Use Role Claims to create UserRoles
			var applicationRoleClaims = claims.Where(c => c.Type == ClaimTypes.Role && c.Issuer == "LOCAL AUTHORITY").ToList();
			await AddUserToRolesAsync(updatedUser, applicationRoleClaims);

			// Store Remaining Claims (excluding UserRole claims)
			var remainingClaims = claims.Except(applicationRoleClaims).ToList();
			await StoreClaimsAsync(updatedUser, remainingClaims, cancellationToken);

			// Setup Instructor Sandbox (if needed)
			if (applicationRoleClaims.Any(c => c.Value == BaseRole.Creator))
				await _instructorSandboxService.SetUpInstructorSandboxAsync(updatedUser, cancellationToken);

			// Add to Global Group (if needed)
			await _userSetupService.AddUserToGlobalGroupAsync(shardKey, updatedUser, cancellationToken);

			// Generate an OAuth code that can be exchanged for a token at /token
			var code = await CreateOAuthCodeAsync(updatedUser, cancellationToken);

			return code;
		}

		public async Task<string> CreateOAuthCodeAsync(TUser user, CancellationToken cancellationToken = default(CancellationToken))
		{
			var cookiesIdentity = await CreateIdentityAsync(user, CookieAuthenticationDefaults.AuthenticationType);
			var ticket = new AuthenticationTicket(cookiesIdentity, new AuthenticationProperties());
			var serializer = new TicketSerializer();
			var serializedTicket = serializer.Serialize(ticket);
			var encryptedTicket = MachineKey.Protect(serializedTicket);
			return Convert.ToBase64String(encryptedTicket);
		}

		#endregion Authentication

		#region CRUD

		public virtual async Task<TUser> CreateUserAsync(TUser user, CancellationToken cancellationToken = default(CancellationToken))
		{
			var result = await CreateAsync(user);
			if (!result.Succeeded)
				throw new Exception(string.Join(", ", result.Errors));
			return user;
		}

		public virtual async Task<TUser> CreateOrUpdateAsync(TUser user, CancellationToken cancellationToken = default(CancellationToken))
		{
			// Find User by UserName
			var existingUser = await FindByNameAsync(user.UserName);

			// Legacy => Find User by Email (old method) and save UserName
			if (existingUser == null)
			{
				existingUser = await FindByEmailAsync(user.Email);
				if (existingUser != null)
				{
					existingUser.UserName = user.UserName;
				}
			}

			// Add New User
			if (existingUser == null)
			{
				existingUser = await CreateUserAsync(user, cancellationToken);
				// no more updates required
				return existingUser;
			}

			// Update User Details
			await UpdateExistingUserAsync(existingUser, user, cancellationToken);

			return existingUser;
		}

		public virtual async Task UpdateExistingUserAsync(TUser existingUser, TUser user, CancellationToken cancellationToken = default(CancellationToken))
		{
			// Update User Details
			if (existingUser.Uid != user.Uid && !string.IsNullOrWhiteSpace(user.Uid))
			{
				existingUser.Uid = user.Uid;
			}
			if (existingUser.EmployeeNumber != user.EmployeeNumber && !string.IsNullOrWhiteSpace(user.EmployeeNumber))
			{
				existingUser.EmployeeNumber = user.EmployeeNumber;
			}
			if (existingUser.FirstName != user.FirstName && !string.IsNullOrWhiteSpace(user.FirstName))
			{
				existingUser.FirstName = user.FirstName;
			}
			if (existingUser.LastName != user.LastName && !string.IsNullOrWhiteSpace(user.LastName))
			{
				existingUser.LastName = user.LastName;
			}
			if (existingUser.Email != user.Email && !string.IsNullOrWhiteSpace(user.Email))
			{
				existingUser.Email = user.Email;
			}
			await Store.UpdateAsync(existingUser);
		}

		public void UpdateUserWithClaims(TUser user, List<Claim> claims)
		{
			var firstNameClaim = claims.FirstOrDefault(x => x.Type == ClaimTypes.GivenName);
			if (firstNameClaim?.Value != null)
				user.FirstName = firstNameClaim.Value.Split(' ')[0];

			var lastNameClaim = claims.FirstOrDefault(x => x.Type == ClaimTypes.Surname);
			if (lastNameClaim?.Value != null)
				user.LastName = lastNameClaim.Value;

			// CAS

			var loginClaim = claims.FirstOrDefault(c => c.Type == ScaffoldingClaimTypes.CasLogin);
			if (loginClaim?.Value != null)
				user.Uid = loginClaim.Value;

			var puidClaim = claims.FirstOrDefault(c => c.Type == ScaffoldingClaimTypes.CasPuid);
			if (puidClaim?.Value != null)
				user.EmployeeNumber = puidClaim.Value;

			// Shibboleth

			var eduPersonPrincipalNameClaim = claims.FirstOrDefault(c => c.Type == ShibbolethSaml2Attributes.EduPersonPrincipalName);
			if (eduPersonPrincipalNameClaim?.Value != null)
				user.UserName = eduPersonPrincipalNameClaim.Value;

			var uidClaim = claims.FirstOrDefault(c => c.Type == ShibbolethSaml2Attributes.Uid);
			if (uidClaim?.Value != null)
				user.Uid = uidClaim.Value;

			var employeeNumberClaim = claims.FirstOrDefault(c => c.Type == ShibbolethSaml2Attributes.EmployeeNumber);
			if (employeeNumberClaim?.Value != null)
				user.EmployeeNumber = employeeNumberClaim.Value;
		}

		#endregion CRUD

		public async Task<UserSearchResult<TUser>> GetOrCreateUsersAsync(string identifiers, BaseConfiguration configuration, CancellationToken cancellationToken = default(CancellationToken))
		{
			var identifiersList = new List<string>(identifiers.Split(new[] { "\r\n", "\n" }, StringSplitOptions.None))
				.Distinct()
				.Select(e => e.Trim())
				.ToList();

			var result = new UserSearchResult<TUser>
			{
				AllowedDomains = configuration.AllowedDomains
			};

			// valid identifier = scoped variable (email or ePPN), e.g. alias@domain.edu
			var validIdentifiers = identifiersList.Where(id => id.HasValidDomainScope() && configuration.ValidateAllowedDomains(id)).ToList();
			var invalidIdentifiers = identifiersList.Where(id => !id.HasValidDomainScope() || !configuration.ValidateAllowedDomains(id)).ToList();
			result.InvalidIdentifiers = invalidIdentifiers;

			// Find users
			var users = await Users.Where(u => validIdentifiers.Contains(u.UserName) || validIdentifiers.Contains(u.Email)).ToListAsync(cancellationToken);
			// Find identifiers with no user match
			var newIdentifiers = validIdentifiers.Where(id => users.All(u => u.UserName != id) && users.All(u => u.Email != id)).ToList();

			// No new users need to be created
			if (!newIdentifiers.Any())
			{
				result.Users = users;
				return result;
			}

			// Create users that were not found
			foreach (var newIdentifier in newIdentifiers)
			{
				await CreateAsync(new TUser
				{
					UserName = newIdentifier,
					Email = newIdentifier,
				});
			}

			// Re-query users after creation
			result.Users = await Users.Where(u => identifiersList.Contains(u.UserName) || identifiersList.Contains(u.Email)).ToListAsync(cancellationToken);
			return result;
		}
	}
}