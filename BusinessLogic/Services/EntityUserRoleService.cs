﻿using FluentValidation;
using Microsoft.AspNet.Identity.EntityFramework;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Interfaces.Authorization;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.Common.Interfaces;
using StudioKit.Scaffolding.DataAccess;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Models.Interfaces;
using StudioKit.Scaffolding.Properties;
using StudioKit.UserInfoService;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Services
{
	public abstract class EntityUserRoleService<TContext, TUser, TGroup, TConfiguration, TEntityUserRole, TEntityUserRoleBusinessModel, TCreateEntityUserRolesBusinessModel> :
			IEntityUserRoleService<TEntityUserRoleBusinessModel, TCreateEntityUserRolesBusinessModel>
		where TContext : BaseDbContext<TUser, TGroup, TConfiguration>
		where TUser : IdentityUser<string, UserLogin, UserRole, UserClaim>, IBaseUser, new()
		where TGroup : BaseGroup, new()
		where TConfiguration : BaseConfiguration, new()
		where TEntityUserRole : EntityUserRole, new()
		where TEntityUserRoleBusinessModel : EntityUserRoleBusinessModel<TUser>, new()
		where TCreateEntityUserRolesBusinessModel : CreateEntityUserRolesBusinessModel<TUser>, new()
	{
		private readonly TContext _dbContext;
		private readonly IShardKeyProvider _shardKeyProvider;
		private readonly IUserInfoService _userInfoService;
		private readonly IEntityUserRoleAuthorizationService _authorizationService;
		private readonly RoleManager _roleManager;
		private readonly List<string> _validEntityRoleNames;

		protected EntityUserRoleService(TContext dbContext,
			IShardKeyProvider shardKeyProvider,
			IUserInfoService userInfoService,
			IEntityUserRoleAuthorizationService authorizationService,
			RoleManager roleManager,
			List<string> validEntityRoleNames)
		{
			_dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
			_shardKeyProvider = shardKeyProvider ?? throw new ArgumentNullException(nameof(shardKeyProvider));
			_userInfoService = userInfoService ?? throw new ArgumentNullException(nameof(userInfoService));
			_authorizationService = authorizationService ?? throw new ArgumentNullException(nameof(authorizationService));
			_roleManager = roleManager ?? throw new ArgumentNullException(nameof(roleManager));
			_validEntityRoleNames = validEntityRoleNames ?? throw new ArgumentNullException(nameof(validEntityRoleNames));
		}

		public async Task<IEnumerable<TEntityUserRoleBusinessModel>> GetEntityUserRolesAsync(int entityId, IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken))
		{
			if (principal == null) throw new ArgumentNullException(nameof(principal));
			await _authorizationService.AssertCanReadEntityUserRolesAsync(entityId, principal, cancellationToken);
			return await GetEntityUserRoleBusinessModelsAsync(entityId, cancellationToken);
		}

		public async Task<TCreateEntityUserRolesBusinessModel> CreateEntityUserRolesAsync(int entityId, List<string> identifiers, string roleName, IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken))
		{
			if (principal == null) throw new ArgumentNullException(nameof(principal));
			if (identifiers == null || identifiers.Count == 0) throw new ArgumentNullException(nameof(identifiers));
			if (string.IsNullOrWhiteSpace(roleName)) throw new ArgumentNullException(nameof(roleName));

			await _authorizationService.AssertCanModifyEntityUserRolesAsync(entityId, principal, cancellationToken);

			AssertEntityRoleIsValid(roleName);

			var role = await _roleManager.FindByNameAsync(roleName);
			if (role == null)
				throw new EntityNotFoundException(string.Format(Strings.RoleNotFound, roleName));

			var shardKey = GetShardKey();
			var configuration = await _dbContext.GetConfigurationAsync(cancellationToken);
			var usersResult = await _dbContext.GetOrCreateUsersByIdentifiersAsync(
				identifiers, configuration, shardKey, _userInfoService, cancellationToken);
			var userIds = usersResult.Users.Select(user => user.Id).ToList();

			// find users to add that are already in the group, with their role(s)
			var currentEntityUserRolesById = await GetEntityQueryable(entityId)
				.Where(eur => userIds.Contains(eur.UserId))
				.Select(eur => new { eur.UserId, eur.Role.Name })
				.GroupBy(n => n.UserId)
				.ToDictionaryAsync(g => g.Key, g => g.Select(n => n.Name).Distinct().ToList(), cancellationToken);

			// find users to add that are already in the requested group role
			var currentUserIds = currentEntityUserRolesById
				.Where(kvp => kvp.Value.Contains(roleName))
				.Select(kvp => kvp.Key)
				.ToList();

			// only add users to the role that do not have it yet
			var newEntityUserIds = userIds.Except(currentUserIds).ToList();

			var existingUserIds = await GetEntityQueryable(entityId)
				.Where(eur => eur.RoleId.Equals(role.Id))
				.Select(m => m.UserId)
				.ToListAsync(cancellationToken);
			var newUserIds = userIds.Where(userId => !existingUserIds.Contains(userId)).ToList();
			if (newUserIds.Any())
			{
				var entityUserRoles = newUserIds
					.Select(userId =>
					{
						var entityUserRole = CreateEntityUserRole(entityId);
						entityUserRole.UserId = userId;
						entityUserRole.RoleId = role.Id;
						return entityUserRole;
					})
					.ToList();
				OnBeforeAddRange(entityUserRoles);
				_dbContext.Set<TEntityUserRole>().AddRange(entityUserRoles);
				await _dbContext.SaveChangesAsync(cancellationToken);
			}

			// create response
			var users = usersResult.Users
				.Select(user =>
				{
					var roles = currentEntityUserRolesById.ContainsKey(user.Id)
						? currentEntityUserRolesById[user.Id]
						: new List<string>();
					if (newEntityUserIds.Contains(user.Id))
						roles.Add(roleName);
					return new TEntityUserRoleBusinessModel
					{
						User = user,
						EntityId = entityId,
						Roles = roles
					};
				})
				.ToList();

			return new TCreateEntityUserRolesBusinessModel
			{
				AddedUsers = users.Where(u => newEntityUserIds.Contains(u.User.Id)),
				ExistingUsers = users.Where(u => !newEntityUserIds.Contains(u.User.Id)),
				AllowedDomains = usersResult.AllowedDomains,
				InvalidIdentifiers = usersResult.InvalidIdentifiers,
				InvalidDomainIdentifiers = usersResult.InvalidDomainIdentifiers
			};
		}

		public async Task DeleteEntityUserRoleAsync(int entityId, string userId, string roleName, IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken))
		{
			if (principal == null) throw new ArgumentNullException(nameof(principal));
			if (string.IsNullOrWhiteSpace(userId)) throw new ArgumentNullException(nameof(userId));
			if (string.IsNullOrWhiteSpace(roleName)) throw new ArgumentNullException(nameof(roleName));

			var shardKey = GetShardKey();
			await _authorizationService.AssertCanModifyEntityUserRolesAsync(entityId, principal, cancellationToken);

			AssertEntityRoleIsValid(roleName);
			var role = await _roleManager.FindByNameAsync(roleName);
			if (role == null)
				throw new EntityNotFoundException(string.Format(Strings.RoleNotFound, roleName));

			var entityUserRole = await GetEntityQueryable(entityId)
				.SingleOrDefaultAsync(m =>
					m.UserId.Equals(userId) &&
					m.RoleId.Equals(role.Id), cancellationToken);
			if (entityUserRole == null)
				return;

			OnBeforeDelete(entityId, userId, role.Id);
			_dbContext.Set<TEntityUserRole>().Remove(entityUserRole);
			await _dbContext.SaveChangesAsync(cancellationToken);

			await OnAfterDeleteAsync(entityId, userId, roleName, shardKey, cancellationToken);
		}

		public async Task SwapEntityUserRolesAsync(int entityId, string userId, string newRoleName, string currentRoleName, IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken))
		{
			if (principal == null) throw new ArgumentNullException(nameof(principal));
			if (string.IsNullOrWhiteSpace(userId)) throw new ArgumentNullException(nameof(userId));
			if (string.IsNullOrWhiteSpace(newRoleName)) throw new ArgumentNullException(nameof(newRoleName));
			if (string.IsNullOrWhiteSpace(currentRoleName)) throw new ArgumentNullException(nameof(currentRoleName));

			var shardKey = GetShardKey();

			await _authorizationService.AssertCanModifyEntityUserRolesAsync(entityId, principal, cancellationToken);

			AssertEntityRoleIsValid(newRoleName);
			var newRole = await _roleManager.FindByNameAsync(newRoleName);
			if (newRole == null)
				throw new EntityNotFoundException(string.Format(Strings.RoleNotFound, newRoleName));

			AssertEntityRoleIsValid(currentRoleName);
			var currentRole = await _roleManager.FindByNameAsync(currentRoleName);
			if (currentRole == null)
				throw new EntityNotFoundException(string.Format(Strings.RoleNotFound, currentRoleName));

			var userEntityRoles = await GetEntityQueryable(entityId)
				.Where(gur => gur.UserId.Equals(userId))
				.ToListAsync(cancellationToken);
			var currentUserRole = userEntityRoles.SingleOrDefault(u => u.RoleId.Equals(currentRole.Id));

			if (userEntityRoles.Any(u => u.RoleId.Equals(newRole.Id)))
				throw new ValidationException(string.Format(Strings.UserHasNewRole, newRoleName));

			if (currentUserRole == null)
				throw new ValidationException(string.Format(Strings.UserCurrentRoleNotFound, currentRoleName));

			var entityUserRole = CreateEntityUserRole(entityId);
			entityUserRole.UserId = userId;
			entityUserRole.RoleId = newRole.Id;

			OnBeforeAdd(entityUserRole);
			_dbContext.Set<TEntityUserRole>().Add(entityUserRole);

			OnBeforeDelete(entityId, userId, currentRole.Id);
			_dbContext.Set<TEntityUserRole>().Remove(currentUserRole);

			await _dbContext.SaveChangesAsync(cancellationToken);

			await OnAfterDeleteAsync(entityId, userId, currentRoleName, shardKey, cancellationToken);
		}

		#region Protected Methods

		protected abstract IQueryable<TEntityUserRole> GetEntityQueryable(int entityId);

		protected abstract TEntityUserRole CreateEntityUserRole(int entityId);

		protected virtual async Task<IEnumerable<TEntityUserRoleBusinessModel>> GetEntityUserRoleBusinessModelsAsync(int entityId, CancellationToken cancellationToken = default(CancellationToken))
		{
			return (await GetEntityQueryable(entityId)
				.Join(_dbContext.Users, eur => eur.UserId, u => u.Id, (eur, u) => new { EntityUserRole = eur, User = u })
				.Join(_dbContext.Roles, euru => euru.EntityUserRole.RoleId, r => r.Id,
					(euru, r) => new { euru.EntityUserRole, euru.User, Role = r }).ToListAsync(cancellationToken)).GroupBy(eur => eur.User.Id).Select(g =>
				  new TEntityUserRoleBusinessModel
				  {
					  User = g.First().User,
					  EntityId = entityId,
					  Roles = g.Select(eur => eur.Role.Name).ToList()
				  });
		}

		protected virtual void OnBeforeAdd(TEntityUserRole entityUserRole)
		{
		}

		protected virtual void OnBeforeAddRange(List<TEntityUserRole> entityUserRoles)
		{
		}

		protected virtual void OnBeforeDelete(int entityId, string userId, string roleId)
		{
		}

		protected virtual Task OnAfterDeleteAsync(int entityId, string userId, string roleName, string shardKey, CancellationToken cancellationToken = default(CancellationToken))
		{
			return Task.CompletedTask;
		}

		#endregion Protected Methods

		#region Private Methods

		private void AssertEntityRoleIsValid(string roleName)
		{
			if (!_validEntityRoleNames.Contains(roleName))
				throw new ArgumentOutOfRangeException(nameof(roleName), string.Format(Strings.EntityRoleNotValid, roleName));
		}

		private string GetShardKey()
		{
			var shardKey = _shardKeyProvider.GetShardKey();
			if (string.IsNullOrWhiteSpace(shardKey))
				throw new ValidationException(Strings.ShardKeyRequired);
			return shardKey;
		}

		#endregion Private Methods
	}
}