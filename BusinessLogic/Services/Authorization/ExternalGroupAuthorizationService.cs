﻿using StudioKit.ErrorHandling.Exceptions;
using StudioKit.Scaffolding.BusinessLogic.Interfaces.Authorization;
using StudioKit.Scaffolding.Common;
using StudioKit.Scaffolding.DataAccess.Interfaces;
using System;
using System.Data.Entity;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Services.Authorization
{
	public class ExternalGroupAuthorizationService<TContext> : AuthorizationService<TContext>, IExternalGroupAuthorizationService
		where TContext : DbContext, IBaseDbContext
	{
		public ExternalGroupAuthorizationService(TContext dbContext) : base(dbContext)
		{
		}

		public async Task AssertCanReadExternalGroupsAnyAsync(IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken))
		{
			if (principal == null) throw new ArgumentNullException(nameof(principal));

			if (!await CanPerformActivitiesAsync(principal, cancellationToken, BaseActivity.ExternalGroupRead))
				throw new ForbiddenException();
		}
	}
}