﻿using Microsoft.AspNet.Identity;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.ExternalProvider.Lti.DataAccess;
using StudioKit.ExternalProvider.Lti.Models;
using StudioKit.Scaffolding.BusinessLogic.Interfaces.Authorization;
using StudioKit.Scaffolding.Common;
using StudioKit.Scaffolding.DataAccess.Interfaces;
using StudioKit.Scaffolding.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Services.Authorization
{
	public class LtiLaunchAuthorizationService<TContext> : AuthorizationService<TContext>, ILtiLaunchAuthorizationService
		where TContext : DbContext, IBaseDbContext, ILtiLaunchDbContext<GroupUserRole, GroupUserRoleLog, ExternalGroup, ExternalGroupUser>
	{
		private readonly TContext _dbContext;

		public LtiLaunchAuthorizationService(TContext dbContext) : base(dbContext)
		{
			_dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
		}

		public async Task AssertCanReadAnyAsync(IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken), IDictionary<string, int> relationIds = null)
		{
			if (principal == null) throw new ArgumentNullException(nameof(principal));

			var canReadyAny = await CanPerformActivitiesAsync(principal, cancellationToken, BaseActivity.LtiLaunchReadAny);
			if (!canReadyAny)
				throw new ForbiddenException();
		}

		public async Task AssertCanReadAsync(int entityId, IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken), IDictionary<string, int> relationIds = null)
		{
			if (principal == null) throw new ArgumentNullException(nameof(principal));

			var ltiLaunch = await _dbContext.FindEntityAsync<LtiLaunch>(entityId, false, cancellationToken: cancellationToken);

			var canRead = await CanPerformActivitiesAsync(principal, cancellationToken, BaseActivity.LtiLaunchReadAny) ||
						await CanPerformActivitiesAsync(principal, cancellationToken, BaseActivity.LtiLaunchReadOwn) &&
						ltiLaunch.CreatedById.Equals(principal.Identity.GetUserId());

			if (!canRead)
				throw new ForbiddenException();
		}

		public Task AssertCanUpdateAsync(int entityId, IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken), IDictionary<string, int> relationIds = null)
		{
			throw new NotImplementedException();
		}

		public Task AssertCanCreateAsync(IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken), IDictionary<string, int> relationIds = null)
		{
			throw new NotImplementedException();
		}

		public Task AssertCanDeleteAsync(int entityId, IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken), IDictionary<string, int> relationIds = null)
		{
			throw new NotImplementedException();
		}
	}
}