﻿using StudioKit.ErrorHandling.Exceptions;
using StudioKit.Scaffolding.BusinessLogic.Interfaces.Authorization;
using StudioKit.Scaffolding.Common;
using StudioKit.Scaffolding.DataAccess.Interfaces;
using System;
using System.Data.Entity;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Services.Authorization
{
	public class UserRoleAuthorizationService<TContext> : AuthorizationService<TContext>, IUserRoleAuthorizationService
		where TContext : DbContext, IBaseDbContext
	{
		public UserRoleAuthorizationService(TContext dbContext) : base(dbContext)
		{
		}

		public async Task AssertCanReadAnyAsync(IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken))
		{
			if (principal == null) throw new ArgumentNullException(nameof(principal));

			var canReadAny = await CanPerformActivitiesAsync(principal, cancellationToken, BaseActivity.UserRoleReadAny);
			if (!canReadAny)
				throw new ForbiddenException();
		}

		public async Task AssertCanCreateAsync(IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken))
		{
			if (principal == null) throw new ArgumentNullException(nameof(principal));

			var canCreate = await CanPerformActivitiesAsync(principal, cancellationToken, BaseActivity.UserRoleModifyAny);
			if (!canCreate)
				throw new ForbiddenException();
		}

		public async Task AssertCanDeleteAnyAsync(IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken))
		{
			if (principal == null) throw new ArgumentNullException(nameof(principal));

			var canDelete = await CanPerformActivitiesAsync(principal, cancellationToken, BaseActivity.UserRoleModifyAny);
			if (!canDelete)
				throw new ForbiddenException();
		}
	}
}