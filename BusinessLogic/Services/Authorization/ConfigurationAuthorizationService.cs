﻿using StudioKit.ErrorHandling.Exceptions;
using StudioKit.Scaffolding.BusinessLogic.Interfaces.Authorization;
using StudioKit.Scaffolding.Common;
using StudioKit.Scaffolding.DataAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Services.Authorization
{
	public class ConfigurationAuthorizationService<TContext> : AuthorizationService<TContext>, IConfigurationAuthorizationService
		where TContext : DbContext, IBaseDbContext
	{
		public ConfigurationAuthorizationService(TContext dbContext) : base(dbContext)
		{
		}

		public Task AssertCanReadAnyAsync(IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken), IDictionary<string, int> relationIds = null)
		{
			if (principal == null) throw new ArgumentNullException(nameof(principal));

			// Note: everyone can read

			return Task.CompletedTask;
		}

		public Task AssertCanReadAsync(int configurationId, IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken), IDictionary<string, int> relationIds = null)
		{
			if (principal == null) throw new ArgumentNullException(nameof(principal));

			// Note: everyone can read

			return Task.CompletedTask;
		}

		public async Task AssertCanUpdateAsync(int configurationId, IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken), IDictionary<string, int> relationIds = null)
		{
			if (principal == null) throw new ArgumentNullException(nameof(principal));

			var canUpdate = await CanPerformActivitiesAsync(principal, cancellationToken, BaseActivity.ConfigurationModify);
			if (!canUpdate)
				throw new ForbiddenException();
		}

		public async Task AssertCanCreateAsync(IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken), IDictionary<string, int> relationIds = null)
		{
			if (principal == null) throw new ArgumentNullException(nameof(principal));

			var canCreate = await CanPerformActivitiesAsync(principal, cancellationToken, BaseActivity.ConfigurationModify);
			if (!canCreate)
				throw new ForbiddenException();
		}

		public async Task AssertCanDeleteAsync(int configurationId, IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken), IDictionary<string, int> relationIds = null)
		{
			if (principal == null) throw new ArgumentNullException(nameof(principal));

			var canDelete = await CanPerformActivitiesAsync(principal, cancellationToken, BaseActivity.ConfigurationModify);
			if (!canDelete)
				throw new ForbiddenException();
		}
	}
}