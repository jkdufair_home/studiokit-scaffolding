﻿using Microsoft.AspNet.Identity;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.Scaffolding.BusinessLogic.Interfaces.Authorization;
using StudioKit.Scaffolding.Common;
using StudioKit.Scaffolding.DataAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Services.Authorization
{
	public class UserAuthorizationService<TContext> : AuthorizationService<TContext>, IUserAuthorizationService
		where TContext : DbContext, IBaseDbContext
	{
		public UserAuthorizationService(TContext dbContext) : base(dbContext)
		{
		}

		public async Task AssertCanReadAnyAsync(IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken), IDictionary<string, int> relationIds = null)
		{
			if (principal == null) throw new ArgumentNullException(nameof(principal));

			var canReadAny = await CanPerformActivitiesAsync(principal, cancellationToken, BaseActivity.UserReadAny);
			if (!canReadAny)
				throw new ForbiddenException();
		}

		public async Task AssertCanReadAsync(string entityId, IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken), IDictionary<string, int> relationIds = null)
		{
			if (principal == null) throw new ArgumentNullException(nameof(principal));

			var currentUserId = principal.Identity.GetUserId();
			var isCurrentUser = currentUserId == entityId;
			var canReadAny = await CanPerformActivitiesAsync(principal, cancellationToken, BaseActivity.UserReadAny);

			if (!canReadAny && !isCurrentUser)
				throw new ForbiddenException();
		}

		public Task AssertCanUpdateAsync(string entityId, IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken), IDictionary<string, int> relationIds = null)
		{
			throw new NotImplementedException();
		}

		public Task AssertCanCreateAsync(IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken), IDictionary<string, int> relationIds = null)
		{
			throw new NotImplementedException();
		}

		public Task AssertCanDeleteAsync(string entityId, IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken), IDictionary<string, int> relationIds = null)
		{
			throw new NotImplementedException();
		}
	}
}