﻿using StudioKit.ErrorHandling.Exceptions;
using StudioKit.Scaffolding.BusinessLogic.Interfaces.Authorization;
using StudioKit.Scaffolding.Common;
using StudioKit.Scaffolding.DataAccess.Interfaces;
using System;
using System.Data.Entity;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Services.Authorization
{
	public class CacheAuthorizationService<TContext> : AuthorizationService<TContext>, ICacheAuthorizationService
		where TContext : DbContext, IBaseDbContext
	{
		public CacheAuthorizationService(TContext dbContext) : base(dbContext)
		{
		}

		public async Task AssertCanReadStatisticsAsync(IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken))
		{
			if (principal == null) throw new ArgumentNullException(nameof(principal));

			var canRead = await CanPerformActivitiesAsync(principal, cancellationToken, BaseActivity.CacheStatisticsRead);
			if (!canRead)
				throw new ForbiddenException();
		}

		public async Task AssertCanPurgeAsync(IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken))
		{
			if (principal == null) throw new ArgumentNullException(nameof(principal));

			var canPurge = await CanPerformActivitiesAsync(principal, cancellationToken, BaseActivity.CachePurge);
			if (!canPurge)
				throw new ForbiddenException();
		}
	}
}