﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using StudioKit.Data.Entity.Identity.Models;
using System.Data.Entity;

namespace StudioKit.Scaffolding.BusinessLogic.Services
{
	public class RoleStore : RoleStore<Role, string, UserRole>, IQueryableRoleStore<Role>
	{
		public RoleStore() : base(new IdentityDbContext())
		{
			DisposeContext = true;
		}

		public RoleStore(DbContext context) : base(context)
		{
		}
	}
}