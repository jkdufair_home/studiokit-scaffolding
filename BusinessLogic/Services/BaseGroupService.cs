using FluentValidation;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using StudioKit.Cloud.Storage.Queue;
using StudioKit.Data.Entity.Identity;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.ExternalProvider.Lti.Models;
using StudioKit.ExternalProvider.Models.Interfaces;
using StudioKit.ExternalProvider.UniTime.Models;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Interfaces.Authorization;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.BusinessLogic.Validators;
using StudioKit.Scaffolding.Common.Interfaces;
using StudioKit.Scaffolding.DataAccess;
using StudioKit.Scaffolding.DataAccess.Queues.QueueMessages;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Models.Interfaces;
using StudioKit.Scaffolding.Properties;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Services
{
	/// <summary>
	/// This class handles basic CRUD operations for groups of users. Subclasses will need to
	/// implement additional logic and call superclass methods.
	/// </summary>
	/// <typeparam name="TContext">The <see cref="DbContext" /> or subtype to use for persistence</typeparam>
	/// <typeparam name="TUser">The Identity user type</typeparam>
	/// <typeparam name="TGroup"><see cref="BaseGroup" /> or a derived type</typeparam>
	/// <typeparam name="TConfiguration">Shard configuration type - <see cref="BaseConfiguration" /> or subtype</typeparam>
	/// <typeparam name="TGroupBusinessModel"><see cref="BaseGroupBusinessModel{TGroup,TUser}" /> or subtype</typeparam>
	/// <typeparam name="TGroupEditBusinessModel"><see cref="BaseGroupEditBusinessModel{TGroup}" /> or subtype</typeparam>
	public class BaseGroupService<TContext, TUser, TGroup, TConfiguration, TGroupBusinessModel, TGroupEditBusinessModel> : IBaseGroupService<TGroup, TGroupBusinessModel, TGroupEditBusinessModel, TUser>
		where TContext : BaseDbContext<TUser, TGroup, TConfiguration>
		where TUser : IdentityUser<string, UserLogin, UserRole, UserClaim>, IBaseUser, new()
		where TGroup : BaseGroup, new()
		where TConfiguration : BaseConfiguration, new()
		where TGroupBusinessModel : BaseGroupBusinessModel<TGroup, TUser>, new()
		where TGroupEditBusinessModel : BaseGroupEditBusinessModel<TGroup>, new()
	{
		private readonly IGroupAuthorizationService _authorizationService;
		private readonly BaseGroupBusinessModelValidator<TGroup> _businessModelValidator;
		private readonly TContext _dbContext;
		private readonly IQueueManager<SyncAllRostersMessage> _syncAllRostersQueueManager;
		private readonly IQueueManager<SyncGroupRosterMessage> _syncGroupRosterQueueManager;
		private readonly ILtiLaunchService _ltiLaunchService;
		private readonly IUniTimeService _uniTimeService;
		private readonly IShardKeyProvider _shardKeyProvider;

		private readonly
			BaseGroupUserRoleService<TContext, TUser, TGroup, TConfiguration, GroupUserRoleBusinessModel<TUser>,
				CreateEntityUserRolesBusinessModel<TUser>> _baseGroupUserRoleService;

		public BaseGroupService(TContext dbContext,
			IGroupAuthorizationService authorizationService,
			BaseGroupBusinessModelValidator<TGroup> businessModelValidator,
			IQueueManager<SyncAllRostersMessage> syncAllRostersQueueManager,
			IQueueManager<SyncGroupRosterMessage> syncGroupRosterQueueManager,
			ILtiLaunchService ltiLaunchService,
			IUniTimeService uniTimeService,
			IShardKeyProvider shardKeyProvider,
			BaseGroupUserRoleService<TContext, TUser, TGroup, TConfiguration, GroupUserRoleBusinessModel<TUser>,
				CreateEntityUserRolesBusinessModel<TUser>> baseGroupUserRoleService)
		{
			_dbContext = dbContext;
			_authorizationService = authorizationService;
			_businessModelValidator = businessModelValidator;
			_syncAllRostersQueueManager = syncAllRostersQueueManager;
			_syncGroupRosterQueueManager = syncGroupRosterQueueManager;
			_ltiLaunchService = ltiLaunchService;
			_uniTimeService = uniTimeService;
			_shardKeyProvider = shardKeyProvider;
			_baseGroupUserRoleService = baseGroupUserRoleService;
		}

		public Task<IEnumerable<TGroupBusinessModel>> GetAllAsync(IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken), IDictionary<string, int> relationIds = null)
		{
			throw new NotSupportedException();
		}

		public async Task<TGroupBusinessModel> GetAsync(int groupId, IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken), IDictionary<string, int> relationIds = null)
		{
			if (principal == null) throw new ArgumentNullException(nameof(principal));
			await _authorizationService.AssertCanReadAsync(groupId, principal, cancellationToken);
			var group = await _dbContext.FindEntityAsync<TGroup>(groupId, false, cancellationToken: cancellationToken);
			return await CreateBusinessModelAsync(group, principal.Identity.GetUserId(), cancellationToken);
		}

		public async Task<TGroupBusinessModel> CreateAsync(TGroupEditBusinessModel businessModel, IPrincipal principal,
			CancellationToken cancellationToken = default(CancellationToken), IDictionary<string, int> relationIds = null)
		{
			if (businessModel == null) throw new ArgumentNullException(nameof(businessModel));
			if (principal == null) throw new ArgumentNullException(nameof(principal));

			await _authorizationService.AssertCanCreateAsync(principal, cancellationToken);
			if (businessModel.ExternalGroups != null)
				await _authorizationService.AssertCanConnectOwnExternalGroupAsync(principal, cancellationToken);
			await _businessModelValidator.AssertIsValidAsync(businessModel, cancellationToken);

			// TODO: wrap in a transaction to make group + externalGroup creation atomic
			// NOTE: how to replace/share dbContext in all injected services?

			var shardKey = GetShardKey();
			var userId = principal.Identity.GetUserId();
			var groupOwnerRoleId = (await _dbContext.Roles.SingleAsync(r => r.Name.Equals(BaseRole.GroupOwner), cancellationToken)).Id;
			var group = businessModel.Map();

			if (businessModel.ExternalTermId.HasValue)
			{
				var externalTerm = await _dbContext.FindEntityAsync<ExternalTerm>(businessModel.ExternalTermId.Value, cancellationToken: cancellationToken);
				group.ExternalTerm = externalTerm;
				group.StartDate = null;
				group.EndDate = null;
			}
			else
			{
				group.ExternalTermId = null;
				group.StartDate = businessModel.StartDate;
				group.EndDate = businessModel.EndDate;
			}
			_dbContext.Groups.Add(group);
			_dbContext.GroupUserRoles.Add(new GroupUserRole
			{
				Group = group,
				UserId = userId,
				RoleId = groupOwnerRoleId
			});
			_dbContext.GroupUserRoleLogs.Add(new GroupUserRoleLog
			{
				Group = group,
				UserId = userId,
				RoleId = groupOwnerRoleId,
				Type = GroupUserRoleLogType.Added
			});
			await _dbContext.SaveChangesAsync(cancellationToken);

			if (businessModel.ExternalGroups != null)
				await UpdateExternalGroupsAsync(group.Id, group.ExternalTermId, new List<ExternalGroup>(), businessModel.ExternalGroups.ToList(), principal, true, cancellationToken);

			await _syncGroupRosterQueueManager.AddMessageAsync(
				new SyncGroupRosterMessage
				{
					ShardKey = shardKey,
					GroupId = group.Id
				}, cancellationToken: cancellationToken);

			return await CreateBusinessModelAsync(group, userId, cancellationToken);
		}

		public async Task<TGroupBusinessModel> UpdateAsync(int groupId, TGroupEditBusinessModel businessModel, IPrincipal principal,
			CancellationToken cancellationToken = default(CancellationToken), IDictionary<string, int> relationIds = null)
		{
			if (businessModel == null) throw new ArgumentNullException(nameof(businessModel));
			if (principal == null) throw new ArgumentNullException(nameof(principal));

			await _authorizationService.AssertCanUpdateAsync(groupId, principal, cancellationToken);
			await _businessModelValidator.AssertIsValidAsync(businessModel, cancellationToken);

			// Finding the group whether or not it's deleted
			var existingGroup = await _dbContext.FindEntityAsync<TGroup>(groupId, false, cancellationToken: cancellationToken);
			var shardKey = GetShardKey();

			var shouldSyncRoster = false;
			var existingExternalGroups = await _dbContext.ExternalGroups
				.Where(eg => eg.GroupId.Equals(groupId))
				.Include(eg => eg.ExternalProvider)
				.ToListAsync(cancellationToken);
			var existingUniTimeExternalGroups = existingExternalGroups
				.Where(e => e.ExternalProvider is UniTimeExternalProvider)
				.ToList();
			var existingLtiExternalGroups = existingExternalGroups
				.Where(e => e.ExternalProvider is LtiExternalProvider)
				.ToList();

			var incomingExternalGroups = businessModel.ExternalGroups.ToList();

			if (existingGroup.IsDeleted)
			{
				var hasChanges = businessModel.Name != existingGroup.Name ||
								businessModel.StartDate != existingGroup.StartDate ||
								businessModel.EndDate != existingGroup.EndDate ||
								businessModel.ExternalTermId != existingGroup.ExternalTermId ||
								// new external groups
								incomingExternalGroups.OfType<ExternalGroupEditBusinessModel>().Count() != incomingExternalGroups.Count ||
								// changes to existing external groups
								existingExternalGroups.Any() && incomingExternalGroups.OfType<ExternalGroupEditBusinessModel>().Count() != existingExternalGroups.Count;

				// If trying to make changes or not trying to restore the group
				if (hasChanges || !businessModel.IsDeleted.HasValue)
					throw new ForbiddenException(string.Format(Strings.GroupCannotUpdateDeleted, groupId));

				// If trying to delete an already deleted group do nothing. PUT should be idempotent
				if (businessModel.IsDeleted.Value)
					return await CreateBusinessModelAsync(existingGroup, principal.Identity.GetUserId(), cancellationToken);

				// We got here because IsDeleted is specified and set to false. Restore group
				existingGroup.IsDeleted = false;
				shouldSyncRoster = existingExternalGroups.Any();
			}
			else if (businessModel.IsDeleted.HasValue && businessModel.IsDeleted.Value)
			{
				// If trying to delete a group, you are not allowed any other updates
				await _authorizationService.AssertCanDeleteAsync(groupId, principal, cancellationToken);
				if (existingLtiExternalGroups.Any())
					throw new ForbiddenException(string.Format(Strings.GroupCannotDeleteIfHasLtiExternalGroups, groupId));
				await _dbContext.DeleteGroupAsync(existingGroup, cancellationToken);
			}
			else
			{
				// If updating ExternalTerm an has UniTime ExternalGroups
				if (existingGroup.ExternalTermId.HasValue &&
					businessModel.ExternalTermId != existingGroup.ExternalTermId && existingUniTimeExternalGroups.Any())
				{
					AssertCanRemoveExternalGroups(existingUniTimeExternalGroups);

					// remove UniTime ExternalGroups if changing/removing term
					_dbContext.ExternalGroups.RemoveRange(existingUniTimeExternalGroups);

					// exclude already removed groups from UpdateExternalGroupsAsync
					existingExternalGroups = existingExternalGroups.Except(existingUniTimeExternalGroups).ToList();

					shouldSyncRoster = true;
				}

				if (businessModel.ExternalTermId.HasValue)
				{
					// ensure externalTerm exists using FindEntityAsync
					var externalTerm =
						await _dbContext.FindEntityAsync<ExternalTerm>(businessModel.ExternalTermId.Value, cancellationToken: cancellationToken);
					existingGroup.ExternalTermId = externalTerm.Id;
					existingGroup.StartDate = null;
					existingGroup.EndDate = null;
				}
				else
				{
					existingGroup.ExternalTermId = null;
					existingGroup.StartDate = businessModel.StartDate;
					existingGroup.EndDate = businessModel.EndDate;
				}

				existingGroup.Name = businessModel.Name;

				// update ExternalGroups in the dbContext, but don't save yet
				await UpdateExternalGroupsAsync(existingGroup.Id, existingGroup.ExternalTermId, existingExternalGroups,
					incomingExternalGroups, principal, false, cancellationToken);
			}

			await _dbContext.SaveChangesAsync(cancellationToken);

			if (shouldSyncRoster)
			{
				await _syncGroupRosterQueueManager.AddMessageAsync(new SyncGroupRosterMessage
				{
					ShardKey = shardKey,
					GroupId = groupId
				}, cancellationToken: cancellationToken);
			}

			return await CreateBusinessModelAsync(existingGroup, principal.Identity.GetUserId(), cancellationToken);
		}

		public async Task DeleteAsync(int groupId, IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken), IDictionary<string, int> relationIds = null)
		{
			if (principal == null) throw new ArgumentNullException(nameof(principal));
			await _authorizationService.AssertCanDeleteAsync(groupId, principal, cancellationToken);
			throw new NotImplementedException();
		}

		public async Task<IEnumerable<TGroupBusinessModel>> GetAsync(
			IPrincipal principal,
			string keywords = null,
			DateTime? date = null,
			bool queryAll = false,
			string userId = null,
			bool includeIfSoftDeleted = false,
			CancellationToken cancellationToken = default(CancellationToken))
		{
			if (principal == null) throw new ArgumentNullException(nameof(principal));
			var principalUserId = principal.Identity.GetUserId();
			if (queryAll || userId != null && userId != principalUserId)
				await _authorizationService.AssertCanReadAnyAsync(principal, cancellationToken);

			if (string.IsNullOrWhiteSpace(keywords) && date == null && queryAll)
				throw new ValidationException(Strings.GroupQueryAllRequiredArguments);

			var queryUserId = userId ?? principalUserId;
			var groupsQueryable = _dbContext.GetUserGroupsQueryable(queryAll, queryUserId, includeIfSoftDeleted);

			var keywordList = keywords?.Split(' ').Select(k => k.ToLower()).ToList();
			if (keywordList != null && keywordList.Any())
			{
				groupsQueryable = groupsQueryable
					.Where(g => keywordList.All(k =>
						g.Id.ToString().Contains(k) ||
						g.Name.ToLower().Contains(k) ||
						g.ExternalTermId != null &&
						g.ExternalTerm.Name.ToLower().Contains(k) ||
						g.ExternalGroups.Any(eg =>
							eg.ExternalId.ToLower().Contains(k) ||
							eg.Name.ToLower().Contains(k) ||
							eg.Description.ToLower().Contains(k))));
			}

			if (date != null)
			{
				// date is assumed to be at midnight of the current user's timezone
				// filter the search based on the entire day
				var startDate = date.Value;
				var endDate = date.Value.AddDays(1);
				groupsQueryable = groupsQueryable
					.Where(g =>
						// within ExternalTerm date range
						g.ExternalTerm != null &&
						(startDate >= g.ExternalTerm.StartDate && startDate <= g.ExternalTerm.EndDate ||
						endDate >= g.ExternalTerm.StartDate && endDate <= g.ExternalTerm.EndDate) ||
						// within Group date range
						g.StartDate.HasValue && g.EndDate.HasValue &&
						(startDate >= g.StartDate && startDate <= g.EndDate ||
						endDate >= g.StartDate && endDate <= g.EndDate));
			}

			var groups = await groupsQueryable.ToListAsync(cancellationToken);
			var activitiesAndRolesByGroup = await _dbContext.UserEntityRolesAndActivitiesAsync<TGroup, GroupUserRole>(queryUserId, groupsQueryable, cancellationToken);
			var groupOwners = await GetOwnersAsync(groupsQueryable, cancellationToken);
			var externalGroups = await groupsQueryable
				.SelectMany(g => g.ExternalGroups)
				.ToListAsync(cancellationToken);
			var groupsWithRosterSync = await groupsQueryable
				.Where(g => g.ExternalGroups
					.Select(eg => eg.ExternalProvider)
					.Any(ep => ep.RosterSyncEnabled))
				.Select(g => g.Id)
				.ToListAsync(cancellationToken);
			// Note: does not use CreateBusinessModel method. More efficient this way
			return groups.Select(group =>
			{
				var (roles, activities) = activitiesAndRolesByGroup[group.Id];
				return new TGroupBusinessModel
				{
					Group = group,
					Roles = roles,
					Activities = activities,
					Owners = groupOwners.Where(go => go.EntityId.Equals(group.Id)),
					ExternalGroups = externalGroups.Where(eg => eg.GroupId.Equals(group.Id))
						.Select(eg => new ExternalGroupBusinessModel(eg)),
					IsRosterSyncEnabled = groupsWithRosterSync.Contains(group.Id)
				};
			});
		}

		public async Task<TGroupBusinessModel> CopyAsync(TGroupEditBusinessModel businessEditModel, int sourceGroupId, IPrincipal principal,
			CancellationToken cancellationToken = default(CancellationToken))
		{
			if (principal == null) throw new ArgumentNullException(nameof(principal));

			//can read the source group
			await _authorizationService.AssertCanReadAsync(sourceGroupId, principal, cancellationToken);

			//can update and add entity user roles in the source group - the user should be GroupOwner of the group to be able to copy
			await _authorizationService.AssertCanUpdateAsync(sourceGroupId, principal, cancellationToken);
			await _authorizationService.AssertCanModifyEntityUserRolesAsync(sourceGroupId, principal,
				cancellationToken);

			var groupBusinessModel = await CreateAsync(businessEditModel, principal, cancellationToken);

			//can update and add entity user roles in the target group - the user should be GroupOwner of the new group
			await _authorizationService.AssertCanUpdateAsync(groupBusinessModel.Group.Id, principal, cancellationToken);
			await _authorizationService.AssertCanModifyEntityUserRolesAsync(groupBusinessModel.Group.Id, principal,
				cancellationToken);

			await CopyOwnersAndGradersAsync(sourceGroupId, groupBusinessModel.Group.Id, principal, cancellationToken);

			return await CreateBusinessModelAsync(groupBusinessModel.Group, principal.Identity.GetUserId(), cancellationToken);
		}

		#region ExternalProviders + Roster Sync

		private static void AssertCanRemoveExternalGroups(List<ExternalGroup> externalGroups)
		{
			// cannot remove LTI ExternalGroups
			if (externalGroups.Any(eg => eg.ExternalProvider is LtiExternalProvider))
				throw new ForbiddenException(Strings.ExternalGroupsCannotRemoveIfLtiExternalProvider);
		}

		public async Task<IEnumerable<ExternalGroupBusinessModel>> GetExternalGroupsAsync(int groupId, IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken))
		{
			if (principal == null) throw new ArgumentNullException(nameof(principal));

			await _authorizationService.AssertCanReadExternalGroupsAsync(groupId, principal, cancellationToken);
			var externalGroups = await _dbContext.ExternalGroups.Where(eg => eg.GroupId.Equals(groupId)).ToListAsync(cancellationToken);
			return externalGroups.Select(eg => new ExternalGroupBusinessModel(eg));
		}

		private async Task UpdateExternalGroupsAsync(int groupId, int? externalTermId, List<ExternalGroup> existingExternalGroups, List<IExternalGroupEditBusinessModel> externalGroups, IPrincipal principal, bool shouldSave, CancellationToken cancellationToken = default(CancellationToken))
		{
			var currentUserId = principal.Identity.GetUserId();

			// no changes, just return
			if (!existingExternalGroups.Any() && !externalGroups.Any())
				return;

			// add new ExternalGroups from UniTimeGroups, if any
			var uniTimeExternalGroupsToAdd = await _uniTimeService.GetExternalGroupsToAddAsync(
				groupId,
				externalTermId,
				existingExternalGroups,
				externalGroups,
				cancellationToken);
			if (uniTimeExternalGroupsToAdd.Any())
			{
				if (uniTimeExternalGroupsToAdd.Any(eg => !eg.UserId.Equals(currentUserId)))
					await _authorizationService.AssertCanConnectAnyExternalGroupAsync(principal, cancellationToken);
				else
					await _authorizationService.AssertCanConnectExternalGroupsAsync(groupId, principal, cancellationToken);

				_dbContext.ExternalGroups.AddRange(uniTimeExternalGroupsToAdd);
			}

			// add new ExternalGroups from LtiLaunches, if any
			var (ltiExternalGroupsToAdd, ltiLaunchesToRemove) = await _ltiLaunchService.GetExternalGroupsToAddAsync(
				groupId,
				existingExternalGroups,
				externalGroups,
				cancellationToken);
			if (ltiExternalGroupsToAdd.Any())
			{
				if (ltiExternalGroupsToAdd.Any(eg => !eg.UserId.Equals(currentUserId)))
					await _authorizationService.AssertCanConnectAnyExternalGroupAsync(principal, cancellationToken);
				else
					await _authorizationService.AssertCanConnectExternalGroupsAsync(groupId, principal, cancellationToken);

				_dbContext.ExternalGroups.AddRange(ltiExternalGroupsToAdd);
				_dbContext.LtiLaunches.RemoveRange(ltiLaunchesToRemove);
			}

			// existing ExternalGroups
			var externalGroupsToKeep = externalGroups
				.OfType<ExternalGroupEditBusinessModel>()
				.ToList();

			// update existing ExternalGroups
			var externalGroupsToUpdate = existingExternalGroups
				.Where(e => externalGroupsToKeep.Any(egk => egk.Id.Equals(e.Id) && e.IsAutoGradePushEnabled != egk.IsAutoGradePushEnabled))
				.Select(e => new { Existing = e, Incoming = externalGroupsToKeep.First(egk => egk.Id.Equals(e.Id)) })
				.ToList();
			if (externalGroupsToUpdate.Any())
			{
				if (externalGroupsToUpdate.Any(eg => !eg.Existing.UserId.Equals(currentUserId)))
					await _authorizationService.AssertCanConnectAnyExternalGroupAsync(principal, cancellationToken);
				else
					await _authorizationService.AssertCanConnectExternalGroupsAsync(groupId, principal, cancellationToken);

				if (externalGroupsToUpdate.Any(externalGroupPair =>
						// IsAutoGradePushEnabled has value when it should not
						externalGroupPair.Incoming.IsAutoGradePushEnabled.HasValue &&
						(!externalGroupPair.Existing.ExternalProvider.GradePushEnabled ||
						externalGroupPair.Existing.GradesUrl == null ||
						externalGroupPair.Existing.GradesUrl.Trim() == string.Empty) ||
						// IsAutoGradePushEnabled has no value when it should
						!externalGroupPair.Incoming.IsAutoGradePushEnabled.HasValue &&
						externalGroupPair.Existing.ExternalProvider.GradePushEnabled &&
						externalGroupPair.Existing.GradesUrl != null &&
						externalGroupPair.Existing.GradesUrl.Trim() != string.Empty))
					throw new ValidationException(Strings.ExternalGroupsUpdateInvalidAutoGradePushEnabled);

				foreach (var externalGroupPair in externalGroupsToUpdate)
				{
					externalGroupPair.Existing.IsAutoGradePushEnabled = externalGroupPair.Incoming.IsAutoGradePushEnabled;
				}
			}

			// remove existing ExternalGroups
			var externalGroupsToRemove = existingExternalGroups
				.Where(e => !externalGroupsToKeep.Any(egk => egk.Id.Equals(e.Id)))
				.ToList();
			if (externalGroupsToRemove.Any())
			{
				if (externalGroupsToRemove.Any(eg => !eg.UserId.Equals(currentUserId)))
					await _authorizationService.AssertCanConnectAnyExternalGroupAsync(principal, cancellationToken);
				else
					await _authorizationService.AssertCanConnectExternalGroupsAsync(groupId, principal, cancellationToken);

				AssertCanRemoveExternalGroups(externalGroupsToRemove);
				_dbContext.ExternalGroups.RemoveRange(externalGroupsToRemove);
			}

			if (!shouldSave)
				return;

			await _dbContext.SaveChangesAsync(cancellationToken);
		}

		public async Task SyncRosterAsync(int groupId, IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken))
		{
			if (principal == null) throw new ArgumentNullException(nameof(principal));

			await _authorizationService.AssertCanSyncRosterAsync(groupId, principal, cancellationToken);
			var shardKey = GetShardKey();

			await _syncGroupRosterQueueManager.AddMessageAsync(
				new SyncGroupRosterMessage
				{
					ShardKey = shardKey,
					GroupId = groupId
				}, cancellationToken: cancellationToken);
		}

		public async Task SyncAllRostersAsync(IPrincipal principal,
			CancellationToken cancellationToken = default(CancellationToken))
		{
			if (principal == null) throw new ArgumentNullException(nameof(principal));

			await _authorizationService.AssertCanSyncAllRostersAsync(principal, cancellationToken);
			var shardKey = GetShardKey();

			await _syncAllRostersQueueManager.AddMessageAsync(
				new SyncAllRostersMessage
				{
					ShardKey = shardKey
				}, cancellationToken: cancellationToken);
		}

		#endregion ExternalProviders + Roster Sync

		#region Private Methods

		private string GetShardKey()
		{
			var shardKey = _shardKeyProvider.GetShardKey();
			if (string.IsNullOrWhiteSpace(shardKey))
				throw new ValidationException(Strings.ShardKeyRequired);
			return shardKey;
		}

		private async Task<IEnumerable<EntityUserBusinessModel<TUser>>> GetOwnersAsync(IQueryable<TGroup> groupQueryable, CancellationToken cancellationToken = default(CancellationToken))
		{
			return await groupQueryable.SelectMany(g => g.GroupUserRoles)
				.Where(gur => gur.Role.Name.Equals(BaseRole.GroupOwner))
				.Join(_dbContext.Users, gur => gur.UserId, u => u.Id,
					(gur, u) => new EntityUserBusinessModel<TUser>
					{
						User = u,
						EntityId = gur.GroupId
					})
				.ToListAsync(cancellationToken);
		}

		private async Task<IEnumerable<EntityUserBusinessModel<TUser>>> GetGradersAsync(IQueryable<TGroup> groupQueryable, CancellationToken cancellationToken = default(CancellationToken))
		{
			return await groupQueryable.SelectMany(g => g.GroupUserRoles)
				.Where(gur => gur.Role.Name.Equals(BaseRole.GroupGrader))
				.Join(_dbContext.Users, gur => gur.UserId, u => u.Id,
					(gur, u) => new EntityUserBusinessModel<TUser>
					{
						User = u,
						EntityId = gur.GroupId
					})
				.ToListAsync(cancellationToken);
		}

		private async Task<TGroupBusinessModel> CreateBusinessModelAsync(TGroup group, string userId, CancellationToken cancellationToken = default(CancellationToken))
		{
			var (userGroupRoles, userGroupActivities) = await _dbContext.UserEntityRolesAndActivitiesAsync<BaseGroup, GroupUserRole>(userId, group, cancellationToken);
			var groupOwners = await GetOwnersAsync(_dbContext.Groups.Where(g => g.Id.Equals(group.Id)), cancellationToken);
			var groupGraders = await GetGradersAsync(_dbContext.Groups.Where(g => g.Id.Equals(group.Id)), cancellationToken);
			var isRosterSyncEnabled = await _dbContext.ExternalGroups
				.Where(eg => eg.GroupId.Equals(group.Id))
				.Select(eg => eg.ExternalProvider)
				.AnyAsync(ep => ep.RosterSyncEnabled, cancellationToken);
			var externalGroups = await _dbContext.ExternalGroups
				.Where(eg => eg.GroupId.Equals(group.Id))
				.ToListAsync(cancellationToken);
			var businessModel = new TGroupBusinessModel
			{
				Group = group,
				IsRosterSyncEnabled = isRosterSyncEnabled,
				Roles = userGroupRoles,
				Activities = userGroupActivities,
				Owners = groupOwners,
				Graders = groupGraders,
				ExternalGroups = externalGroups.Select(e => new ExternalGroupBusinessModel(e))
			};
			return businessModel;
		}

		private async Task CopyOwnersAndGradersAsync(int sourceGroupId, int targetGroupId, IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken))
		{
			var userId = principal.Identity.GetUserId();
			var sourceGroupQueryable = _dbContext.Groups.Where(g => g.Id.Equals(sourceGroupId));
			var groupOwnerRole = (await _dbContext.Roles.SingleAsync(r => r.Name.Equals(BaseRole.GroupOwner), cancellationToken));
			var groupGraderRole = (await _dbContext.Roles.SingleAsync(r => r.Name.Equals(BaseRole.GroupGrader), cancellationToken));

			var owners = await GetOwnersAsync(sourceGroupQueryable, cancellationToken);
			var graders = await GetGradersAsync(sourceGroupQueryable, cancellationToken);

			//don't copy the user who created the group
			var ownersEmail = owners.Where(o => !o.User.Id.Equals(userId)).Select(s => s.User.Email).ToList();
			var gradersEmail = graders.Where(o => !o.User.Id.Equals(userId)).Select(s => s.User.Email).ToList();

			if (ownersEmail.Any())
				await _baseGroupUserRoleService.CreateEntityUserRolesAsync(targetGroupId, ownersEmail, groupOwnerRole.Name, principal,
					cancellationToken);
			if (gradersEmail.Any())
				await _baseGroupUserRoleService.CreateEntityUserRolesAsync(targetGroupId, gradersEmail, groupGraderRole.Name, principal,
					cancellationToken);
		}

		#endregion Private Methods
	}
}