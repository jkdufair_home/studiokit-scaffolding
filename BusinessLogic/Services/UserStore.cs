﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.Scaffolding.Models.Interfaces;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Services
{
	public class UserStore<TUser> : UserStore<TUser, Role, string, UserLogin, UserRole, UserClaim>,
		IUserStore<TUser>
		where TUser : IdentityUser<string, UserLogin, UserRole, UserClaim>, IBaseUser, new()
	{
		public UserStore(DbContext context) : base(context)
		{
		}

		public override async Task<IList<Claim>> GetClaimsAsync(TUser user)
		{
			// load claims from the db with claim issuers
			var claims = (await Context.Set<UserClaim>().Where(uc => uc.UserId == user.Id).ToListAsync())
				.Select(uc => new Claim(uc.ClaimType, uc.ClaimValue, uc.ClaimValueType, uc.ClaimIssuer, uc.ClaimOriginalIssuer))
				.ToList();

			if (claims.All(sc => sc.Type != ClaimTypes.Email))
				claims.Add(new Claim(ClaimTypes.Email, user.Email));

			return claims;
		}
	}
}