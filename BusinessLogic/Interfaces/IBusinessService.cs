﻿using System.Collections.Generic;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Interfaces
{
	public interface IBusinessService<in TKey, TModel, in TCreateModel, in TUpdateModel>
		where TModel : class
		where TCreateModel : class
		where TUpdateModel : class
	{
		Task<IEnumerable<TModel>> GetAllAsync(IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken), IDictionary<string, int> relationIds = null);

		Task<TModel> GetAsync(TKey entityId, IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken), IDictionary<string, int> relationIds = null);

		Task<TModel> CreateAsync(TCreateModel businessModel, IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken), IDictionary<string, int> relationIds = null);

		Task<TModel> UpdateAsync(TKey entityId, TUpdateModel businessModel, IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken), IDictionary<string, int> relationIds = null);

		Task DeleteAsync(TKey entityId, IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken), IDictionary<string, int> relationIds = null);
	}

	public interface IBusinessService<TModel, in TCreateModel, in TUpdateModel>
		: IBusinessService<int, TModel, TCreateModel, TUpdateModel>
		where TModel : class
		where TCreateModel : class
		where TUpdateModel : class
	{ }
}