﻿using System.Threading;
using Microsoft.IdentityModel.Tokens;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Interfaces
{
	public interface IJsonWebKeyService
	{
		Task<JsonWebKeySet> GetJsonWebKeySetAsync(CancellationToken cancellationToken = default(CancellationToken));
	}
}