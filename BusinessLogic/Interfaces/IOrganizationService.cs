﻿using StudioKit.Sharding.Models;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Interfaces
{
	public interface IOrganizationService
	{
		Task<Organization> GetOrganizationAsync(string shardKey, CancellationToken cancellationToken = default(CancellationToken));
	}
}