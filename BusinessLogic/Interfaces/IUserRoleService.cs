﻿using Microsoft.AspNet.Identity.EntityFramework;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.Models.Interfaces;
using System.Collections.Generic;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Interfaces
{
	public interface IUserRoleService<TUser, TUserRoleBusinessModel>
		where TUser : IdentityUser<string, UserLogin, UserRole, UserClaim>, IBaseUser, new()
	{
		Task<IEnumerable<TUserRoleBusinessModel>> GetUserRolesAsync(string roleName, IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken));

		Task<CreateUserRolesBusinessModel<TUser>> CreateUserRolesAsync(string roleName, List<string> identifiers, IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken));

		Task DeleteUserRoleAsync(string userId, string roleName, IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken));
	}
}