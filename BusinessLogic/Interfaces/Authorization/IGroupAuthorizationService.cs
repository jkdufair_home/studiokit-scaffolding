﻿using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Interfaces.Authorization
{
	public interface IGroupAuthorizationService : IAuthorizationService<int>, IEntityUserRoleAuthorizationService
	{
		Task AssertCanReadExternalGroupsAsync(int groupId, IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken));

		Task AssertCanConnectExternalGroupsAsync(int groupId, IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken));

		Task AssertCanConnectAnyExternalGroupAsync(IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken));

		Task AssertCanConnectOwnExternalGroupAsync(IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken));

		Task AssertCanSyncRosterAsync(int groupId, IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken));

		Task AssertCanSyncAllRostersAsync(IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken));
	}
}