﻿using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Interfaces.Authorization
{
	public interface ICacheAuthorizationService : IAuthorizationService
	{
		Task AssertCanReadStatisticsAsync(IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken));

		Task AssertCanPurgeAsync(IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken));
	};
}