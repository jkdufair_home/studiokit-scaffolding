﻿using StudioKit.Data;
using System.Collections.Generic;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Interfaces.Authorization
{
	public interface IAuthorizationService
	{
		Task<bool> CanPerformActivitiesAsync(IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken), params string[] activities);

		Task<bool> CanPerformActivitiesForEntityByGroupAsync<T>(IPrincipal principal, T entity,
			CancellationToken cancellationToken = default(CancellationToken), params string[] activities) where T : ModelBase;

		Task<bool> CanPerformActivitiesForEntityAsync<T>(IPrincipal principal, T entity,
			CancellationToken cancellationToken = default(CancellationToken), params string[] activities) where T : ModelBase, IUserRoleAssociatedEntity;
	}

	public interface IAuthorizationService<in TKey> : IAuthorizationService
	{
		Task AssertCanReadAnyAsync(IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken), IDictionary<string, int> relationIds = null);

		Task AssertCanReadAsync(TKey entityId, IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken), IDictionary<string, int> relationIds = null);

		Task AssertCanUpdateAsync(TKey entityId, IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken), IDictionary<string, int> relationIds = null);

		Task AssertCanCreateAsync(IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken), IDictionary<string, int> relationIds = null);

		Task AssertCanDeleteAsync(TKey entityId, IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken), IDictionary<string, int> relationIds = null);
	}
}