﻿namespace StudioKit.Scaffolding.BusinessLogic.Interfaces.Authorization
{
	public interface IConfigurationAuthorizationService : IAuthorizationService<int>
	{
	}
}