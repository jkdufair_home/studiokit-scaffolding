﻿namespace StudioKit.Scaffolding.BusinessLogic.Interfaces.Authorization
{
	public interface IUserAuthorizationService : IAuthorizationService<string>
	{
	}
}