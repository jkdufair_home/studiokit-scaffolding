using System;

namespace StudioKit.Scaffolding.BusinessLogic.Interfaces.Authorization
{
	public interface IUserRoleAssociatedEntity
	{
		Type UserRoleAssociatedType { get; }
	}
}