﻿using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.Models.Interfaces;
using System.Collections.Generic;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Interfaces
{
	public interface IBaseUserService<TUser, TUserBusinessModel, in TUserEditBusinessModel> : IBusinessService<string, TUserBusinessModel, TUserEditBusinessModel, TUserEditBusinessModel>
		where TUser : IBaseUser, new()
		where TUserBusinessModel : BaseUserBusinessModel<TUser>, new()
		where TUserEditBusinessModel : BaseUserEditBusinessModel<TUser>, new()
	{
		Task<IEnumerable<TUserBusinessModel>> GetAsync(IPrincipal principal, string keywords, CancellationToken cancellationToken = default(CancellationToken));
	}
}