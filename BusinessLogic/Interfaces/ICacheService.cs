﻿using EFCache.Redis;
using System.Collections.Generic;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Interfaces
{
	public interface ICacheService
	{
		Task<IEnumerable<QueryStatistics>> GetCacheStatisticsAsync(IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken));

		Task PurgeCacheAsync(IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken));
	}
}