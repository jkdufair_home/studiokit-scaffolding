﻿using StudioKit.Scaffolding.Models.Interfaces;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Interfaces
{
	public interface IUserSetupService<in TUser> : IDisposable
		where TUser : IBaseUser
	{
		Task AddUserToGlobalGroupAsync(string shardKey, TUser user, CancellationToken cancellationToken = default(CancellationToken));
	}
}