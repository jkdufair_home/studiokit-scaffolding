﻿using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.Models;
using System.Collections.Generic;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Interfaces
{
	public interface IUniTimeService
	{
		Task<InstructorScheduleBusinessModel> GetInstructorScheduleAsync(string userId, int externalTermId, IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken));

		Task<List<ExternalGroup>> GetExternalGroupsToAddAsync(
			int groupId,
			int? externalTermId,
			List<ExternalGroup> existingExternalGroups,
			IEnumerable<IExternalGroupEditBusinessModel> externalGroups,
			CancellationToken cancellationToken = default(CancellationToken));
	}
}