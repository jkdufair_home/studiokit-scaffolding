﻿using Microsoft.AspNet.Identity;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Models.Interfaces;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Interfaces
{
	public interface IUserManager<TUser> where TUser : IBaseUser
	{
		#region Microsoft.AspNet.Identity

		IList<string> GetRoles(string userId);

		Task<IList<string>> GetRolesAsync(string userId);

		Task<bool> IsInRoleAsync(string userId, string roleName);

		Task<IdentityResult> AddToRoleAsync(string userId, string roleName);

		Task<IdentityResult> RemoveFromRoleAsync(string userId, string roleName);

		Task<ClaimsIdentity> CreateIdentityAsync(TUser user, string authenticationType);

		Task<TUser> FindAsync(string userName, string password);

		Task<TUser> FindAsync(UserLoginInfo login);

		Task<TUser> FindByNameAsync(string userName);

		TUser FindById(string userId);

		Task<TUser> FindByIdAsync(string userId);

		Task<TUser> FindByEmailAsync(string email);

		Task<IdentityResult> AddLoginAsync(string userId, UserLoginInfo login);

		Task<IdentityResult> AddClaimAsync(string userId, Claim claim);

		Task<IdentityResult> RemoveClaimAsync(string userId, Claim claim);

		Task<IList<Claim>> GetClaimsAsync(string userId);

		#endregion Microsoft.AspNet.Identity

		#region StudioKit.Scaffolding

		/// <summary>
		/// Persists the given user, claims, and completes other setup logic. Then generates and returns an OAuth Code.
		/// </summary>
		/// <param name="user">The user to sign in. Should contain Email and/or Username.</param>
		/// <param name="claims">The claims for the user, if any.</param>
		/// <param name="userLoginInfo">The login info for the current sign in method.</param>
		/// <param name="shardKey">The shardKey for the db the user is signing into.</param>
		/// <param name="cancellationToken">A <see cref="CancellationToken"/> from the caller</param>
		/// <returns>The OAuth Code</returns>
		Task<string> FinishSignInAsync(TUser user, List<Claim> claims, UserLoginInfo userLoginInfo, string shardKey, CancellationToken cancellationToken = default(CancellationToken));

		Task<string> CreateOAuthCodeAsync(TUser user, CancellationToken cancellationToken = default(CancellationToken));

		Task<bool> CheckPasswordAsync(TUser user, string password);

		Task<IdentityResult> CreateAsync(TUser user, string password);

		Task<TUser> CreateUserAsync(TUser user, CancellationToken cancellationToken = default(CancellationToken));

		Task<TUser> CreateOrUpdateAsync(TUser user, CancellationToken cancellationToken = default(CancellationToken));

		Task UpdateExistingUserAsync(TUser existingUser, TUser user, CancellationToken cancellationToken = default(CancellationToken));

		Task<UserSearchResult<TUser>> GetOrCreateUsersAsync(string identifiers, BaseConfiguration configuration, CancellationToken cancellationToken = default(CancellationToken));

		#endregion StudioKit.Scaffolding
	}
}