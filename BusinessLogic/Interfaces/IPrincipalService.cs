﻿namespace StudioKit.Scaffolding.BusinessLogic.Interfaces
{
	public interface IPrincipalService
	{
		bool PrincipalExists(string id);
	}
}