﻿using StudioKit.ExternalProvider.Lti.Models;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.Models;
using System.Collections.Generic;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Interfaces
{
	public interface ILtiLaunchService
	{
		Task<LtiLaunch> GetLtiLaunchAsync(int ltiLaunchId, IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken));

		Task<(List<ExternalGroup>, List<LtiLaunch>)> GetExternalGroupsToAddAsync(
			int groupId,
			List<ExternalGroup> existingExternalGroups,
			IEnumerable<IExternalGroupEditBusinessModel> externalGroups,
			CancellationToken cancellationToken = default(CancellationToken));
	}
}