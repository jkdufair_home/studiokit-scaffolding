using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.Models;
using System.Collections.Generic;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Interfaces
{
	public interface IArtifactService : IBusinessService<Artifact, IArtifactBusinessModel, IArtifactBusinessModel>
	{
		Task<string> GenerateTokenAsync(int artifactId, IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken), IDictionary<string, int> relationIds = null);

		Task<string> CopyBlobAsync(Artifact source, Artifact destination, CancellationToken cancellationToken = default(CancellationToken));
	}
}