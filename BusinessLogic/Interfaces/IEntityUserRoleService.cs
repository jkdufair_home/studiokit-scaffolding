﻿using System.Collections.Generic;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Interfaces
{
	public interface IEntityUserRoleService<TEntityUserRoleBusinessModel, TCreateEntityUserRoleBusinessModel>
	{
		Task<IEnumerable<TEntityUserRoleBusinessModel>> GetEntityUserRolesAsync(
			int entityId,
			IPrincipal principal,
			CancellationToken cancellationToken = default(CancellationToken));

		Task<TCreateEntityUserRoleBusinessModel> CreateEntityUserRolesAsync(
			int entityId,
			List<string> identifiers,
			string roleName,
			IPrincipal principal,
			CancellationToken cancellationToken = default(CancellationToken));

		Task DeleteEntityUserRoleAsync(
			int entityId,
			string userId,
			string roleName,
			IPrincipal principal,
			CancellationToken cancellationToken = default(CancellationToken));

		Task SwapEntityUserRolesAsync(
			int entityId,
			string userId,
			string newRoleName,
			string currentRoleName,
			IPrincipal principal,
			CancellationToken cancellationToken = default(CancellationToken));
	}
}