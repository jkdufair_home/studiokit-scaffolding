﻿using FluentValidation;
using FluentValidation.Results;
using StudioKit.Scaffolding.Properties;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Validators
{
	public class BaseValidator<T> : AbstractValidator<T> where T : class
	{
		public void AssertIsValid(T entity)
		{
			var validationResult = Validate(entity);
			if (!validationResult.IsValid)
				throw new ValidationException(validationResult.Errors);
		}

		public override ValidationResult Validate(ValidationContext<T> context)
		{
			var typeName = typeof(T).Name;
			var instance = context?.InstanceToValidate;
			return instance == null
				? new ValidationResult(new[]
					{new ValidationFailure(typeName, string.Format(Strings.RequestModelIsEmpty, typeName))})
				: base.Validate(context);
		}

		public async Task AssertIsValidAsync(T entity, CancellationToken cancellationToken = default(CancellationToken))
		{
			var validationResult = await ValidateAsync(entity, cancellationToken);
			if (!validationResult.IsValid)
				throw new ValidationException(validationResult.Errors);
		}

		public override async Task<ValidationResult> ValidateAsync(ValidationContext<T> context,
			CancellationToken cancellation = new CancellationToken())
		{
			var typeName = typeof(T).Name;
			var instance = context?.InstanceToValidate;
			return instance == null
				? new ValidationResult(new[]
					{new ValidationFailure(typeName, string.Format(Strings.RequestModelIsEmpty, typeName))})
				: await base.ValidateAsync(context, cancellation);
		}
	}
}