﻿using FluentValidation;
using StudioKit.Scaffolding.BusinessLogic.Models;

namespace StudioKit.Scaffolding.BusinessLogic.Validators
{
	public class ArtifactBusinessModelValidator : BaseValidator<IArtifactBusinessModel>
	{
		public ArtifactBusinessModelValidator()
		{
			When(x => x != null, () =>
			{
				RuleFor(x => x)
				.SetValidator(new PolymorphicValidator<IArtifactBusinessModel>()
				.Add(new TextArtifactBusinessModelValidator())
				.Add(new FileArtifactBusinessModelValidator())
				.Add(new UrlArtifactBusinessModelValidator()))
				.OverridePropertyName(nameof(IArtifactBusinessModel));
			});
		}
	}

	public class FileArtifactBusinessModelValidator : BaseValidator<FileArtifactBusinessModel>
	{
		public FileArtifactBusinessModelValidator()
		{
			RuleFor(x => x.FileName).NotEmpty();
			RuleFor(x => x.ContentType).NotEmpty();
			RuleFor(x => x.FileStream).NotEmpty();
		}
	}

	public class TextArtifactBusinessModelValidator : BaseValidator<TextArtifactBusinessModel>
	{
		public TextArtifactBusinessModelValidator()
		{
			RuleFor(x => x.Text).NotEmpty();
			RuleFor(x => x.WordCount).NotEmpty();
		}
	}

	public class UrlArtifactBusinessModelValidator : BaseValidator<UrlArtifactBusinessModel>
	{
		public UrlArtifactBusinessModelValidator()
		{
			RuleFor(x => x.Url).NotEmpty();
		}
	}
}