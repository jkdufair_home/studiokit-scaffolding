﻿using FluentValidation;
using FluentValidation.Results;
using FluentValidation.Validators;
using System;
using System.Collections.Generic;
using System.Linq;

namespace StudioKit.Scaffolding.BusinessLogic.Validators
{
	// https://github.com/JeremySkinner/FluentValidation/issues/111

	public class PolymorphicValidator<TBaseClass> : NoopPropertyValidator
		where TBaseClass : class
	{
		private readonly Dictionary<Type, IValidator> _derivedValidators = new Dictionary<Type, IValidator>();

		public PolymorphicValidator<TBaseClass> Add<TDerived>(IValidator<TDerived> derivedValidator)
			where TDerived : TBaseClass
		{
			_derivedValidators[typeof(TDerived)] = derivedValidator;
			return this;
		}

		public override IEnumerable<ValidationFailure> Validate(PropertyValidatorContext context)
		{
			// Bail out if the property is null
			if (context?.PropertyValue == null)
			{
				return Enumerable.Empty<ValidationFailure>();
			}

			// Make sure property is of correct type
			IEnumerable<TBaseClass> instancesToValidate = null;
			switch (context.PropertyValue)
			{
				case IEnumerable<TBaseClass> e:
					instancesToValidate = e;
					break;

				case TBaseClass c:
					instancesToValidate = new[] { c };
					break;
			}
			if (instancesToValidate == null)
			{
				return Enumerable.Empty<ValidationFailure>();
			}

			var index = 0;
			var results = new List<ValidationFailure>();
			foreach (var instanceToValidate in instancesToValidate)
			{
				var derivedValidator = _derivedValidators
					.Where(kvp => instanceToValidate.GetType() == kvp.Key || instanceToValidate.GetType().IsSubclassOf(kvp.Key))
					.Select(kvp => kvp.Value)
					.FirstOrDefault();

				// Find the validator to use based on actual type of the element.
				if (derivedValidator != null)
				{
					var newContext = context.ParentContext.Clone(instanceToValidate: instanceToValidate);
					newContext.PropertyChain.Add(context.Rule.PropertyName);
					newContext.PropertyChain.AddIndexer(index);
					// Execute child validator.
					results.AddRange(derivedValidator.Validate(newContext).Errors);
				}
				index++;
			}

			return results;
		}
	}
}