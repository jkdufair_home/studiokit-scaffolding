﻿using FluentValidation;
using StudioKit.Scaffolding.BusinessLogic.Models;

namespace StudioKit.Scaffolding.BusinessLogic.Validators
{
	public class ExternalGroupBusinessModelValidator : BaseValidator<ExternalGroupBusinessModel>
	{
		public ExternalGroupBusinessModelValidator()
		{
			RuleFor(x => x.ExternalId).NotEmpty();
			RuleFor(x => x.Name).NotEmpty();
		}
	}
}