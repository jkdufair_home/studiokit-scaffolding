using FluentValidation;
using StudioKit.Cloud.Storage.Blob;
using StudioKit.Data;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.DataAccess.Interfaces;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Models.Interfaces;
using StudioKit.Scaffolding.Properties;
using System;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Utils
{
	public static class ArtifactHelpers
	{
		public const string PlainTextContentType = "text/plain";
		public const string TextStorageLocation = "textartifacts/";
		public const string FileStorageLocation = "fileartifacts/";
		public const string GeneralTextArtifactFileName = "TextArtifact";
		public const string GeneralTextArtifactFileExtension = ".txt";
		public const string AllowedFileExtensionsPattern = @"^\.(pdf|txt|csv|doc|docx|xls|xlsx|ppt|pptx|jpg|jpeg|png|gif|bmp|mp4|mov|mp3)$";

		public static async Task<Artifact> SaveArtifactAsync(
			IArtifactBusinessModel requestModel, IBlobStorage artifactStorage, IBaseDbContext dbContext, string userId, CancellationToken cancellationToken = default(CancellationToken))
		{
			if (requestModel == null) throw new ArgumentNullException(nameof(requestModel));
			if (artifactStorage == null) throw new ArgumentNullException(nameof(artifactStorage));
			if (dbContext == null) throw new ArgumentNullException(nameof(dbContext));
			if (string.IsNullOrWhiteSpace(userId)) throw new ArgumentNullException(nameof(userId));

			Artifact artifact;
			switch (requestModel)
			{
				case TextArtifactBusinessModel t:
					var textBlobPath = GenerateTextBlobPath();
					var sanitizedText = ArtifactHtmlSanitizeSettings.Sanitize(t.Text);
					using (var textStream = GenerateStreamFromString(sanitizedText))
					{
						await artifactStorage.UploadBlockBlobAsync(textStream, textBlobPath, PlainTextContentType, cancellationToken);
					}

					artifact = new TextArtifact
					{
						WordCount = t.WordCount,
						Url = artifactStorage.UriForBlockBlob(textBlobPath).ToString(),
						CreatedById = userId
					};
					break;

				case FileArtifactBusinessModel f:
					var filename = f.FileName;
					var fileBlobPath = GenerateFileBlobPath(filename);
					await artifactStorage.UploadBlockBlobAsync(f.FileStream, fileBlobPath, f.ContentType, cancellationToken);

					artifact = new FileArtifact
					{
						FileName = filename,
						Url = artifactStorage.UriForBlockBlob(fileBlobPath).ToString(),
						CreatedById = userId
					};
					break;

				case UrlArtifactBusinessModel u:
					artifact = new UrlArtifact
					{
						Url = u.Url.Trim(),
						CreatedById = userId
					};
					break;

				default:
					throw new InvalidEnumArgumentException();
			}

			await dbContext.SaveChangesAsync(cancellationToken);

			return artifact;
		}

		public static async Task<Artifact> SaveArtifactAsync<TEntity, TEntityArtifact>(TEntity artifactsContainer,
			IArtifactBusinessModel requestModel, IBlobStorage artifactStorage, IBaseDbContext dbContext, string userId, CancellationToken cancellationToken = default(CancellationToken))
			where TEntity : ModelBase, IArtifactsContainer<TEntityArtifact>
			where TEntityArtifact : ModelBase, IEntityArtifact, new()
		{
			if (artifactsContainer == null) throw new ArgumentNullException(nameof(artifactsContainer));

			var artifact = await SaveArtifactAsync(requestModel, artifactStorage, dbContext, userId, cancellationToken);

			artifactsContainer.EntityArtifacts.Add(new TEntityArtifact
			{
				Artifact = artifact
			});
			await dbContext.SaveChangesAsync(cancellationToken);

			return artifact;
		}

		public static async Task<Artifact> UpdateArtifactAsync(Artifact artifact, IArtifactBusinessModel requestModel,
			IBlobStorage artifactStorage, IBaseDbContext dbContext, CancellationToken cancellationToken = default(CancellationToken))
		{
			if (artifact == null) throw new ArgumentNullException(nameof(artifact));
			if (requestModel == null) throw new ArgumentNullException(nameof(requestModel));
			if (artifactStorage == null) throw new ArgumentNullException(nameof(artifactStorage));
			if (dbContext == null) throw new ArgumentNullException(nameof(dbContext));

			switch (requestModel)
			{
				case TextArtifactBusinessModel t:
					if (!(artifact is TextArtifact textArtifact))
					{
						throw new ValidationException(Strings.UpdateArtifactTypeMismatch);
					}

					// To update, delete the existing blob
					var existingTextBlobId = textArtifact.Url.Substring(textArtifact.Url.LastIndexOf("/", StringComparison.Ordinal) + 1);
					var existingTextBlobPath = $"{TextStorageLocation}{existingTextBlobId}";
					await artifactStorage.DeleteBlockBlobAsync(existingTextBlobPath, cancellationToken);

					// And upload the new blob
					var textBlobPath = GenerateTextBlobPath();
					var sanitizedText = ArtifactHtmlSanitizeSettings.Sanitize(t.Text);
					using (var textStream = GenerateStreamFromString(sanitizedText))
					{
						await artifactStorage.UploadBlockBlobAsync(textStream, textBlobPath, PlainTextContentType, cancellationToken);
					}

					textArtifact.WordCount = t.WordCount;
					textArtifact.Url = artifactStorage.UriForBlockBlob(textBlobPath).ToString();

					await dbContext.SaveChangesAsync(cancellationToken);
					return textArtifact;

				case FileArtifactBusinessModel f:
					if (!(artifact is FileArtifact fileArtifact))
					{
						throw new ValidationException(Strings.UpdateArtifactTypeMismatch);
					}

					// To update, delete the existing blob
					var existingFileBlobId = fileArtifact.Url.Substring(fileArtifact.Url.LastIndexOf("/", StringComparison.Ordinal) + 1);
					var existingFileBlobPath = $"{FileStorageLocation}{existingFileBlobId}";
					await artifactStorage.DeleteBlockBlobAsync(existingFileBlobPath, cancellationToken);

					// And upload the new blob
					var filename = f.FileName;
					var fileBlobPath = GenerateFileBlobPath(filename);
					await artifactStorage.UploadBlockBlobAsync(f.FileStream, fileBlobPath, f.ContentType, cancellationToken);

					fileArtifact.FileName = filename;
					fileArtifact.Url = artifactStorage.UriForBlockBlob(fileBlobPath).ToString();

					await dbContext.SaveChangesAsync(cancellationToken);
					return fileArtifact;

				case UrlArtifactBusinessModel u:
					if (!(artifact is UrlArtifact urlArtifact))
					{
						throw new ValidationException(Strings.UpdateArtifactTypeMismatch);
					}

					urlArtifact.Url = u.Url;
					await dbContext.SaveChangesAsync(cancellationToken);
					return urlArtifact;

				default:
					throw new InvalidEnumArgumentException();
			}
		}

		public static async Task StreamFileArtifactAsync(FileArtifact artifact, Stream stream, IBlobStorage artifactStorage, CancellationToken cancellationToken = default(CancellationToken))
		{
			if (artifact == null)
				throw new ArgumentNullException(nameof(artifact));
			if (artifact.Url == null)
				throw new ArgumentNullException(nameof(artifact.Url));
			if (stream == null)
				throw new ArgumentNullException(nameof(stream));
			if (artifactStorage == null)
				throw new ArgumentNullException(nameof(artifactStorage));
			var blobPath = GetFileBlobPathFromUrl(artifact.Url);
			await artifactStorage.WriteBlockBlobToStreamAsync(stream, blobPath, cancellationToken);
		}

		public static void AssertAssignmentFileExtensionIsValid(IArtifactBusinessModel businessModel)
		{
			if (businessModel == null) throw new ArgumentNullException(nameof(businessModel));

			if (!(businessModel is FileArtifactBusinessModel fileArtifactBusinessModel))
			{
				return;
			}

			var currentFileExtension = Path.GetExtension(fileArtifactBusinessModel.FileName)?.ToLowerInvariant();
			if (currentFileExtension == null || !Regex.IsMatch(currentFileExtension, AllowedFileExtensionsPattern))
			{
				throw new ValidationException(Strings.FileArtifactExtensionNotAllowed);
			}
		}

		public static void AssertFileExtensionIsValid(IArtifactBusinessModel businessModel, string allowedFileExtensions)
		{
			if (businessModel == null) throw new ArgumentNullException(nameof(businessModel));
			if (string.IsNullOrWhiteSpace(allowedFileExtensions)) throw new ArgumentNullException(nameof(allowedFileExtensions));

			if (!(businessModel is FileArtifactBusinessModel fileArtifactBusinessModel))
			{
				return;
			}

			var allowedFileExtensionParts = allowedFileExtensions.Split(',').ToList();
			var currentFileExtension = Path.GetExtension(fileArtifactBusinessModel.FileName)?.ToLowerInvariant();
			if (currentFileExtension == null || !allowedFileExtensionParts.Contains(currentFileExtension))
			{
				throw new ValidationException(Strings.FileArtifactExtensionNotAllowed);
			}
		}

		public static async Task<string> CopyBlobAsync(Artifact source, Artifact destination,
			IBlobStorage artifactStorage, CancellationToken cancellationToken = default(CancellationToken))
		{
			var sourcePath = "";
			var destinationPath = "";
			var shouldCopy = false;
			switch (source)
			{
				case TextArtifact t:
					sourcePath = GetTextBlobPathFromUrl(t.Url);
					destinationPath = GenerateTextBlobPath();
					shouldCopy = true;
					break;

				case FileArtifact f:
					sourcePath = GetFileBlobPathFromUrl(f.Url);
					destinationPath = GenerateFileBlobPath(f.FileName);
					shouldCopy = true;
					break;
			}

			if (!shouldCopy) return null;
			await artifactStorage.CopyBlockBlobAsync(sourcePath, destinationPath, cancellationToken);
			return artifactStorage.UriForBlockBlob(destinationPath).ToString();
		}

		private static string GenerateTextBlobPath()
		{
			return $"{TextStorageLocation}{GeneralTextArtifactFileName}-{Guid.NewGuid()}{GeneralTextArtifactFileExtension}";
		}

		private static string GenerateFileBlobPath(string fileName)
		{
			var fileExtension = Path.GetExtension(fileName);
			return $"{FileStorageLocation}{Guid.NewGuid()}{fileExtension}";
		}

		private static string GetFileBlobPathFromUrl(string url)
		{
			var match = Regex.Match(url, $"({FileStorageLocation}.+$)");
			if (match.Groups.Count != 2 || string.IsNullOrEmpty(match.Groups[1].Value))
				throw new ApplicationException(Strings.ArtifactUrlInvalid);
			return match.Groups[1].Value;
		}

		private static string GetTextBlobPathFromUrl(string url)
		{
			var match = Regex.Match(url, $"({TextStorageLocation}.+$)");
			if (match.Groups.Count != 2 || string.IsNullOrEmpty(match.Groups[1].Value))
				throw new ApplicationException(Strings.ArtifactUrlInvalid);
			return match.Groups[1].Value;
		}

		private static Stream GenerateStreamFromString(string content)
		{
			var stream = new MemoryStream();
			var writer = new StreamWriter(stream);
			writer.Write(content);
			writer.Flush();
			stream.Position = 0;
			return stream;
		}
	}
}