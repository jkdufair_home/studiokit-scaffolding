﻿using StudioKit.Scaffolding.Models.Interfaces;
using System.Collections.Generic;

namespace StudioKit.Scaffolding.BusinessLogic.Models
{
	public class BaseUserBusinessModel<TUser>
		where TUser : IBaseUser
	{
		public TUser User { get; set; }

		public IEnumerable<string> Roles { get; set; } = new List<string>();

		public IEnumerable<string> Activities { get; set; } = new List<string>();
	}
}