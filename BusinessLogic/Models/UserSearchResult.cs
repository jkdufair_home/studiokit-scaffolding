﻿using StudioKit.Scaffolding.Models.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace StudioKit.Scaffolding.BusinessLogic.Models
{
	public class UserSearchResult<T>
		where T : IBaseUser
	{
		public UserSearchResult()
		{
			InvalidIdentifiers = new List<string>();
			InvalidDomainIdentifiers = new List<string>();
			Users = new List<T>();
		}

		public string AllowedDomains { get; set; }

		public List<string> InvalidIdentifiers { get; set; }

		public List<string> InvalidDomainIdentifiers { get; set; }

		public List<T> Users { get; set; }

		public string GetErrorMessage()
		{
			string errorMessage = null;
			if (InvalidIdentifiers.Any())
				errorMessage += $"The following identifiers are invalid:<br/> {string.Join("<br/>", InvalidIdentifiers)}";
			if (InvalidDomainIdentifiers.Any())
				errorMessage +=
					(errorMessage != null ? "</br>" : "") +
					$"The following identifiers do not match the allowed domains of \"{AllowedDomains.Replace(",", ", ")}\":<br/>{string.Join("<br/>", InvalidIdentifiers)}";
			return errorMessage;
		}
	}
}