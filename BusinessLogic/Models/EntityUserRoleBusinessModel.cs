﻿using System.Collections.Generic;

namespace StudioKit.Scaffolding.BusinessLogic.Models
{
	public class EntityUserRoleBusinessModel<TUser> : EntityUserBusinessModel<TUser>
	{
		public EntityUserRoleBusinessModel()
		{
		}

		public EntityUserRoleBusinessModel(TUser user, int entityId, List<string> roles) : base(user, entityId)
		{
			Roles = roles;
		}

		public List<string> Roles { get; set; }
	}
}