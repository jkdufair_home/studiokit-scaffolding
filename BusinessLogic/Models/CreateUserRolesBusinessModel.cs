﻿using Microsoft.AspNet.Identity.EntityFramework;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.Scaffolding.Models.Interfaces;
using System.Collections.Generic;

namespace StudioKit.Scaffolding.BusinessLogic.Models
{
	public class CreateUserRolesBusinessModel<TUser>
		where TUser : IdentityUser<string, UserLogin, UserRole, UserClaim>, IBaseUser, new()
	{
		/// <summary>
		/// Users that were added to the Role, sent in the CreateUserRoles request
		/// </summary>
		public IEnumerable<TUser> AddedUsers { get; set; }

		/// <summary>
		/// Users that were already in the Role, sent in the CreateUserRoles request
		/// </summary>
		public IEnumerable<TUser> ExistingUsers { get; set; }

		public IEnumerable<string> InvalidIdentifiers { get; set; }

		public string AllowedDomains { get; set; }

		public IEnumerable<string> InvalidDomainIdentifiers { get; set; }
	}
}