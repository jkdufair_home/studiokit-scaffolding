﻿using StudioKit.ExternalProvider.Models.Interfaces;

namespace StudioKit.Scaffolding.BusinessLogic.Models
{
	public interface IExternalGroupEditBusinessModel
	{
		int Id { get; set; }

		string UserId { get; set; }

		/// <summary>
		/// See <see cref="IExternalGroup.IsAutoGradePushEnabled"/>
		/// </summary>
		bool? IsAutoGradePushEnabled { get; set; }
	}

	public class ExternalGroupEditBusinessModel : IExternalGroupEditBusinessModel
	{
		public int Id { get; set; }

		public string UserId { get; set; }

		public bool? IsAutoGradePushEnabled { get; set; }
	}

	public class LtiLaunchExternalGroupEditBusinessModel : IExternalGroupEditBusinessModel
	{
		public int Id { get; set; }

		public string UserId { get; set; }

		public bool? IsAutoGradePushEnabled { get; set; }
	}

	public class UniTimeGroupExternalGroupEditBusinessModel : IExternalGroupEditBusinessModel
	{
		public int Id { get; set; }

		public string UserId { get; set; }

		public bool? IsAutoGradePushEnabled { get; set; }
	}
}