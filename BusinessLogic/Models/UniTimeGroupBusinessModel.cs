﻿using StudioKit.ExternalProvider.UniTime.Models;

namespace StudioKit.Scaffolding.BusinessLogic.Models
{
	public class UniTimeGroupBusinessModel : IExternalGroupEditBusinessModel
	{
		public UniTimeGroupBusinessModel()
		{
		}

		public UniTimeGroupBusinessModel(UniTimeGroup uniTimeGroup)
		{
			Id = uniTimeGroup.Id;
			ExternalProviderId = uniTimeGroup.ExternalProviderId;
			ExternalId = uniTimeGroup.ExternalId;
			Name = uniTimeGroup.Name;
			Description = uniTimeGroup.Description;
			UserId = uniTimeGroup.CreatedById;
		}

		public int Id { get; set; }

		public int ExternalProviderId { get; set; }

		public string ExternalId { get; set; }

		public string Name { get; set; }

		public string Description { get; set; }

		public string UserId { get; set; }

		public bool? IsAutoGradePushEnabled { get; set; }
	}
}