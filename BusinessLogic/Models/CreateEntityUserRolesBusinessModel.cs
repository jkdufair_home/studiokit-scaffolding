﻿using System.Collections.Generic;

namespace StudioKit.Scaffolding.BusinessLogic.Models
{
	public class CreateEntityUserRolesBusinessModel<TUser>
	{
		/// <summary>
		/// Users that were added to the Entity Role, sent in the CreateEntityUserRoles request
		/// </summary>
		public IEnumerable<EntityUserRoleBusinessModel<TUser>> AddedUsers { get; set; }

		/// <summary>
		/// Users that were already in the Entity Role, sent in the CreateEntityUserRoles request
		/// </summary>
		public IEnumerable<EntityUserRoleBusinessModel<TUser>> ExistingUsers { get; set; }

		public IEnumerable<string> InvalidIdentifiers { get; set; }

		public string AllowedDomains { get; set; }

		public IEnumerable<string> InvalidDomainIdentifiers { get; set; }
	}
}