﻿using System.Collections.Generic;

namespace StudioKit.Scaffolding.BusinessLogic.Models
{
	public class GroupUserRoleBusinessModel<TUser> : EntityUserRoleBusinessModel<TUser>
	{
		public GroupUserRoleBusinessModel()
		{
		}

		public GroupUserRoleBusinessModel(TUser user, int entityId, List<string> roles, bool isExternal) : base(user, entityId, roles)
		{
			IsExternal = isExternal;
		}

		public bool IsExternal { get; set; }
	}
}