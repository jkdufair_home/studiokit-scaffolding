﻿using System.Collections.Generic;

namespace StudioKit.Scaffolding.BusinessLogic.Models
{
	public class UserRoleBusinessModel<TUser>
	{
		public UserRoleBusinessModel()
		{
		}

		public UserRoleBusinessModel(TUser user, List<string> roles)
		{
			User = user;
			Roles = roles;
		}

		public TUser User { get; set; }

		public List<string> Roles { get; set; }
	}
}