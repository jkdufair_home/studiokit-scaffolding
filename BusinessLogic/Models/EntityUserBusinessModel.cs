﻿namespace StudioKit.Scaffolding.BusinessLogic.Models
{
	public class EntityUserBusinessModel<TUser>
	{
		public EntityUserBusinessModel()
		{
		}

		public EntityUserBusinessModel(TUser user, int entityId)
		{
			User = user;
			EntityId = entityId;
		}

		public TUser User { get; set; }

		public int EntityId { get; set; }
	}
}