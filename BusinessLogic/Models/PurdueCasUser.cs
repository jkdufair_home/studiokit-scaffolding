﻿namespace StudioKit.Scaffolding.BusinessLogic.Models
{
	public class PurdueCasUser
	{
		public string Login { get; set; }

		public string Puid { get; set; }

		public string EmailAddress { get; set; }

		public string FirstName { get; set; }

		public string LastName { get; set; }

		public string[] I2A2Characteristics { get; set; }
	}
}