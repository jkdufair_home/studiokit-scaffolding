﻿using StudioKit.Scaffolding.Models.Interfaces;

namespace StudioKit.Scaffolding.BusinessLogic.Models
{
	public class BaseUserEditBusinessModel<TUser>
		where TUser : IBaseUser, new()
	{
		public TUser Map()
		{
			return new TUser
			{
				FirstName = FirstName,
				LastName = LastName
			};
		}

		public string FirstName { get; set; }

		public string LastName { get; set; }
	}
}