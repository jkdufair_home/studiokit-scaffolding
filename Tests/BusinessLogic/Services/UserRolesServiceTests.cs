﻿using Effort;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MSTestExtensions;
using StudioKit.Data.Entity.Identity;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.BusinessLogic.Services;
using StudioKit.Scaffolding.BusinessLogic.Services.Authorization;
using StudioKit.Scaffolding.DataAccess;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Tests.TestData.DataAccess;
using StudioKit.Scaffolding.Tests.TestData.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.Tests.BusinessLogic.Services
{
	[TestClass]
	public class UserRolesServiceTests : BaseTest
	{
		private BaseTestDependencies _deps;

		[TestInitialize]
		public void Startup()
		{
			_deps = new BaseTestDependencies(BaseTestDependencies.StartDate);
		}

		#region GetUserRolesAsync

		[TestMethod]
		public async Task GetUserRolesAsync_ShouldThrowWithNullArguments()
		{
			using (var dbContext =
				new TestBaseDbContext(DbConnectionFactory.CreateTransient(), _deps.DateTimeProvider,
					_deps.AdminPrincipalProvider))
			{
				await _deps.SeedRolesAndUsersAsync(dbContext);
				var service =
					new UserRoleService<BaseDbContext<TestBaseUser, TestBaseGroup, BaseConfiguration>, TestBaseUser, TestBaseGroup, BaseConfiguration, UserRoleBusinessModel<TestBaseUser>>(
						dbContext, _deps.ShardKeyProvider, _deps.UserInfoService,
						new UserRoleAuthorizationService<BaseDbContext<TestBaseUser, TestBaseGroup, BaseConfiguration>>(dbContext),
						new RoleManager(new RoleStore(dbContext)));
				Assert.ThrowsAsync<ArgumentNullException>(Task.Run(async () =>
				{
					await service.GetUserRolesAsync(null, _deps.BasicUserPrincipal);
				}));
				Assert.ThrowsAsync<ArgumentNullException>(Task.Run(async () =>
				{
					await service.GetUserRolesAsync(BaseRole.Admin, null);
				}));
				Assert.ThrowsAsync<EntityNotFoundException>(Task.Run(async () =>
				{
					await service.GetUserRolesAsync("Foobar", _deps.AdminPrincipal);
				}));
			}
		}

		[TestMethod]
		public async Task GetUserRolesAsync_ShouldReturnAllUserRoles()
		{
			using (var dbContext =
				new TestBaseDbContext(DbConnectionFactory.CreateTransient(), _deps.DateTimeProvider,
					_deps.AdminPrincipalProvider))
			{
				await _deps.SeedRolesAndUsersAsync(dbContext);
				var service =
					new UserRoleService<BaseDbContext<TestBaseUser, TestBaseGroup, BaseConfiguration>, TestBaseUser, TestBaseGroup, BaseConfiguration, UserRoleBusinessModel<TestBaseUser>>(
						dbContext, _deps.ShardKeyProvider, _deps.UserInfoService,
						new UserRoleAuthorizationService<BaseDbContext<TestBaseUser, TestBaseGroup, BaseConfiguration>>(dbContext),
						new RoleManager(new RoleStore(dbContext)));
				var result = (await service.GetUserRolesAsync(BaseRole.Admin, _deps.AdminPrincipal)).ToList();
				Assert.AreEqual(result.Count, 2, "Two users have admin role");
				Assert.IsTrue(result.Select(r => r.User.Id).OrderBy(r => r)
					.SequenceEqual(new List<string> { _deps.SuperAdminId, _deps.AdminId }.OrderBy(l => l)), "The correct users have admin role");
				result = (await service.GetUserRolesAsync(BaseRole.SuperAdmin, _deps.SuperAdminPrincipal)).ToList();
				Assert.AreEqual(result.Count, 1, "One user has super admin role");
				Assert.IsTrue(result.Select(r => r.User.Id).OrderBy(r => r)
					.SequenceEqual(new List<string> { _deps.SuperAdminId }.OrderBy(l => l)), "The correct user has super admin role");
				result = (await service.GetUserRolesAsync(BaseRole.Creator, _deps.SuperAdminPrincipal)).ToList();
				Assert.AreEqual(result.Count, 0, "No users have creator role");
			}
		}

		[TestMethod]
		public async Task GetUserRolesAsync_ShouldThrowWithoutAccessToQueryRoles()
		{
			using (var dbContext =
				new TestBaseDbContext(DbConnectionFactory.CreateTransient(), _deps.DateTimeProvider,
					_deps.AdminPrincipalProvider))
			{
				await _deps.SeedRolesAndUsersAsync(dbContext);
				var service =
					new UserRoleService<BaseDbContext<TestBaseUser, TestBaseGroup, BaseConfiguration>, TestBaseUser, TestBaseGroup, BaseConfiguration, UserRoleBusinessModel<TestBaseUser>>(
						dbContext, _deps.ShardKeyProvider, _deps.UserInfoService,
						new UserRoleAuthorizationService<BaseDbContext<TestBaseUser, TestBaseGroup, BaseConfiguration>>(dbContext),
						new RoleManager(new RoleStore(dbContext)));
				Assert.ThrowsAsync<ForbiddenException>(Task.Run(async () =>
				{
					await service.GetUserRolesAsync(BaseRole.Admin, _deps.BasicUserPrincipal);
				}));
			}
		}

		#endregion GetUserRolesAsync

		#region CreateUserRolesAsync

		[TestMethod]
		public async Task CreateUserRoleAsync_ShouldThrowWithNullArguments()
		{
			using (var dbContext =
				new TestBaseDbContext(DbConnectionFactory.CreateTransient(), _deps.DateTimeProvider,
					_deps.AdminPrincipalProvider))
			{
				await _deps.SeedRolesAndUsersAsync(dbContext);
				var service =
					new UserRoleService<BaseDbContext<TestBaseUser, TestBaseGroup, BaseConfiguration>, TestBaseUser, TestBaseGroup, BaseConfiguration, UserRoleBusinessModel<TestBaseUser>>(
						dbContext, _deps.ShardKeyProvider, _deps.UserInfoService,
						new UserRoleAuthorizationService<BaseDbContext<TestBaseUser, TestBaseGroup, BaseConfiguration>>(dbContext),
						new RoleManager(new RoleStore(dbContext)));
				Assert.ThrowsAsync<ArgumentNullException>(Task.Run(async () =>
				{
					await service.CreateUserRolesAsync(null, new List<string> { $"{_deps.BasicUserId}@localhost.com" },
						_deps.AdminPrincipal);
				}));
				Assert.ThrowsAsync<ArgumentNullException>(Task.Run(async () =>
				{
					await service.CreateUserRolesAsync(BaseRole.Admin, null, _deps.AdminPrincipal);
				}));
				Assert.ThrowsAsync<ArgumentNullException>(Task.Run(async () =>
				{
					await service.CreateUserRolesAsync(BaseRole.Admin, new List<string>(), _deps.AdminPrincipal);
				}));
				Assert.ThrowsAsync<ArgumentNullException>(Task.Run(async () =>
				{
					await service.CreateUserRolesAsync(BaseRole.Admin, new List<string> { $"{_deps.BasicUserId}@localhost.com" }, null);
				}));
			}
		}

		[TestMethod]
		public async Task CreateUserRoleAsync_ShouldThrowWithInvalidRole()
		{
			using (var dbContext =
				new TestBaseDbContext(DbConnectionFactory.CreateTransient(), _deps.DateTimeProvider,
					_deps.AdminPrincipalProvider))
			{
				await _deps.SeedRolesAndUsersAsync(dbContext);
				var service =
					new UserRoleService<BaseDbContext<TestBaseUser, TestBaseGroup, BaseConfiguration>, TestBaseUser, TestBaseGroup, BaseConfiguration, UserRoleBusinessModel<TestBaseUser>>(
						dbContext, _deps.ShardKeyProvider, _deps.UserInfoService,
						new UserRoleAuthorizationService<BaseDbContext<TestBaseUser, TestBaseGroup, BaseConfiguration>>(dbContext),
						new RoleManager(new RoleStore(dbContext)));
				Assert.ThrowsAsync<EntityNotFoundException>(Task.Run(async () =>
				{
					await service.CreateUserRolesAsync("foobar", new List<string> { $"{_deps.BasicUserId}@localhost.com" },
						_deps.AdminPrincipal);
				}));
			}
		}

		[TestMethod]
		public async Task CreateUserRoleAsync_ShouldAddNewRole()
		{
			using (var dbContext =
				new TestBaseDbContext(DbConnectionFactory.CreateTransient(), _deps.DateTimeProvider,
					_deps.AdminPrincipalProvider))
			{
				await _deps.SeedRolesAndUsersAsync(dbContext);
				var service =
					new UserRoleService<BaseDbContext<TestBaseUser, TestBaseGroup, BaseConfiguration>, TestBaseUser, TestBaseGroup, BaseConfiguration, UserRoleBusinessModel<TestBaseUser>>(
						dbContext, _deps.ShardKeyProvider, _deps.UserInfoService,
						new UserRoleAuthorizationService<BaseDbContext<TestBaseUser, TestBaseGroup, BaseConfiguration>>(dbContext),
						new RoleManager(new RoleStore(dbContext)));
				var result = await service.CreateUserRolesAsync(BaseRole.Admin, new List<string> { $"{_deps.BasicUserId}@localhost.com" }, _deps.AdminPrincipal);
				Assert.AreEqual(1, result.AddedUsers.Count(), "AddedUsers");
				Assert.AreEqual(0, result.ExistingUsers.Count(), "ExistingUsers");
				Assert.AreEqual(0, result.InvalidIdentifiers.Count(), "InvalidIdentifiers");
				Assert.AreEqual(0, result.InvalidDomainIdentifiers.Count(), "InvalidDomainIdentifiers");

				var adminRoleId = (await BaseTestDependencies.GetRoleManager(dbContext).FindByNameAsync(BaseRole.Admin)).Id;
				var userRole = await dbContext.IdentityUserRoles
					.Where(ur => ur.RoleId.Equals(adminRoleId) && ur.UserId.Equals(_deps.BasicUserId))
					.SingleOrDefaultAsync();
				Assert.IsNotNull(userRole, "Has role");
				Assert.AreEqual(adminRoleId, userRole?.RoleId, "Has correct role");
			}
		}

		[TestMethod]
		public async Task CreateUserRoleAsync_ShouldAddExistingUserToSecondRole()
		{
			using (var dbContext =
				new TestBaseDbContext(DbConnectionFactory.CreateTransient(), _deps.DateTimeProvider,
					_deps.AdminPrincipalProvider))
			{
				await _deps.SeedRolesAndUsersAsync(dbContext);
				var service =
					new UserRoleService<BaseDbContext<TestBaseUser, TestBaseGroup, BaseConfiguration>, TestBaseUser, TestBaseGroup, BaseConfiguration, UserRoleBusinessModel<TestBaseUser>>(
						dbContext, _deps.ShardKeyProvider, _deps.UserInfoService,
						new UserRoleAuthorizationService<BaseDbContext<TestBaseUser, TestBaseGroup, BaseConfiguration>>(dbContext),
						new RoleManager(new RoleStore(dbContext)));
				var result = await service.CreateUserRolesAsync(BaseRole.SuperAdmin, new List<string> { $"{_deps.AdminId}@localhost.com" }, _deps.AdminPrincipal);
				Assert.AreEqual(1, result.AddedUsers.Count(), "AddedUsers");
				Assert.AreEqual(0, result.ExistingUsers.Count(), "ExistingUsers");
				Assert.AreEqual(0, result.InvalidIdentifiers.Count(), "InvalidIdentifiers");
				Assert.AreEqual(0, result.InvalidDomainIdentifiers.Count(), "InvalidDomainIdentifiers");

				var superAdminRoleId = (await BaseTestDependencies.GetRoleManager(dbContext).FindByNameAsync(BaseRole.SuperAdmin)).Id;
				var userRole = await dbContext.IdentityUserRoles
					.Where(ur => ur.RoleId.Equals(superAdminRoleId) && ur.UserId.Equals(_deps.AdminId))
					.SingleOrDefaultAsync();
				Assert.IsNotNull(userRole, "Has role");
				Assert.AreEqual(superAdminRoleId, userRole?.RoleId, "Has correct role");
			}
		}

		[TestMethod]
		public async Task CreateUserRoleAsync_ShouldNotAddExistingUsersToSameRole()
		{
			using (var dbContext =
				new TestBaseDbContext(DbConnectionFactory.CreateTransient(), _deps.DateTimeProvider,
					_deps.AdminPrincipalProvider))
			{
				await _deps.SeedRolesAndUsersAsync(dbContext);
				var service =
					new UserRoleService<BaseDbContext<TestBaseUser, TestBaseGroup, BaseConfiguration>, TestBaseUser, TestBaseGroup, BaseConfiguration, UserRoleBusinessModel<TestBaseUser>>(
						dbContext, _deps.ShardKeyProvider, _deps.UserInfoService,
						new UserRoleAuthorizationService<BaseDbContext<TestBaseUser, TestBaseGroup, BaseConfiguration>>(dbContext),
						new RoleManager(new RoleStore(dbContext)));
				var result = await service.CreateUserRolesAsync(BaseRole.Admin, new List<string> { $"{_deps.AdminId}@localhost.com" }, _deps.AdminPrincipal);
				Assert.AreEqual(0, result.AddedUsers.Count(), "AddedUsers");
				Assert.AreEqual(1, result.ExistingUsers.Count(), "ExistingUsers");
				Assert.AreEqual(0, result.InvalidIdentifiers.Count(), "InvalidIdentifiers");
				Assert.AreEqual(0, result.InvalidDomainIdentifiers.Count(), "InvalidDomainIdentifiers");

				var adminRoleId = (await BaseTestDependencies.GetRoleManager(dbContext).FindByNameAsync(BaseRole.Admin)).Id;
				var userRole = await dbContext.IdentityUserRoles
					.Where(ur => ur.RoleId.Equals(adminRoleId) && ur.UserId.Equals(_deps.AdminId))
					.SingleOrDefaultAsync();
				Assert.IsNotNull(userRole, "Has role");
				Assert.AreEqual(adminRoleId, userRole?.RoleId, "Has correct role");
			}
		}

		#endregion CreateUserRolesAsync

		#region DeleteUserRoleAsync

		[TestMethod]
		public async Task DeleteUserRoleAsync_ShouldThrowWithNullOrBadArguments()
		{
			using (var dbContext =
				new TestBaseDbContext(DbConnectionFactory.CreateTransient(), _deps.DateTimeProvider,
					_deps.AdminPrincipalProvider))
			{
				await _deps.SeedRolesAndUsersAsync(dbContext);
				var service =
					new UserRoleService<BaseDbContext<TestBaseUser, TestBaseGroup, BaseConfiguration>, TestBaseUser, TestBaseGroup, BaseConfiguration, UserRoleBusinessModel<TestBaseUser>>(
						dbContext, _deps.ShardKeyProvider, _deps.UserInfoService,
						new UserRoleAuthorizationService<BaseDbContext<TestBaseUser, TestBaseGroup, BaseConfiguration>>(dbContext),
						new RoleManager(new RoleStore(dbContext)));
				Assert.ThrowsAsync<ArgumentNullException>(Task.Run(async () =>
				{
					await service.DeleteUserRoleAsync(null, BaseRole.Admin, _deps.AdminPrincipal);
				}));
				Assert.ThrowsAsync<ArgumentNullException>(Task.Run(async () =>
				{
					await service.DeleteUserRoleAsync(_deps.AdminId, null, _deps.AdminPrincipal);
				}));
				Assert.ThrowsAsync<ArgumentNullException>(Task.Run(async () =>
				{
					await service.DeleteUserRoleAsync(_deps.AdminId, BaseRole.Admin, null);
				}));
				Assert.ThrowsAsync<EntityNotFoundException>(Task.Run(async () =>
				{
					await service.DeleteUserRoleAsync(_deps.AdminId, "foobar", _deps.AdminPrincipal);
				}));
			}
		}

		[TestMethod]
		public async Task DeleteUserRoleAsync_ShouldDeleteUserRole()
		{
			using (var dbContext =
				new TestBaseDbContext(DbConnectionFactory.CreateTransient(), _deps.DateTimeProvider,
					_deps.AdminPrincipalProvider))
			{
				await _deps.SeedRolesAndUsersAsync(dbContext);
				var service =
					new UserRoleService<BaseDbContext<TestBaseUser, TestBaseGroup, BaseConfiguration>, TestBaseUser, TestBaseGroup, BaseConfiguration, UserRoleBusinessModel<TestBaseUser>>(
						dbContext, _deps.ShardKeyProvider, _deps.UserInfoService,
						new UserRoleAuthorizationService<BaseDbContext<TestBaseUser, TestBaseGroup, BaseConfiguration>>(dbContext),
						new RoleManager(new RoleStore(dbContext)));
				await service.DeleteUserRoleAsync(_deps.AdminId, BaseRole.Admin, _deps.AdminPrincipal);
				var adminRoleId = (await BaseTestDependencies.GetRoleManager(dbContext).FindByNameAsync(BaseRole.Admin)).Id;
				var userRole = await dbContext.IdentityUserRoles
					.Where(ur => ur.RoleId.Equals(adminRoleId) && ur.UserId.Equals(_deps.AdminId))
					.SingleOrDefaultAsync();
				Assert.IsNull(userRole, "Does not have role");
			}
		}

		[TestMethod]
		public async Task DeleteUserRoleAsync_ShouldBeIdempotentWithoutExistingRole()
		{
			using (var dbContext = new TestBaseDbContext(DbConnectionFactory.CreateTransient(), _deps.DateTimeProvider,
				_deps.AdminPrincipalProvider))
			{
				await _deps.SeedRolesAndUsersAsync(dbContext);
				var service =
					new UserRoleService<BaseDbContext<TestBaseUser, TestBaseGroup, BaseConfiguration>, TestBaseUser, TestBaseGroup,
						BaseConfiguration, UserRoleBusinessModel<TestBaseUser>>(dbContext, _deps.ShardKeyProvider, _deps.UserInfoService,
						new UserRoleAuthorizationService<BaseDbContext<TestBaseUser, TestBaseGroup, BaseConfiguration>>(dbContext),
						new RoleManager(new RoleStore(dbContext)));
				await service.DeleteUserRoleAsync(_deps.BasicUserId, BaseRole.Admin, _deps.AdminPrincipal);
				var adminRoleId = (await BaseTestDependencies.GetRoleManager(dbContext).FindByNameAsync(BaseRole.Admin)).Id;
				var userRole = await dbContext.IdentityUserRoles
					.Where(ur => ur.RoleId.Equals(adminRoleId) && ur.UserId.Equals(_deps.BasicUserId)).SingleOrDefaultAsync();
				Assert.IsNull(userRole, "Does not have role");
			}
		}

		#endregion DeleteUserRoleAsync
	}
}