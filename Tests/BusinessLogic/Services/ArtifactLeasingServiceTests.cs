using Effort;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MSTestExtensions;
using StudioKit.Cloud.Storage.Blob;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.Scaffolding.BusinessLogic.Services;
using StudioKit.Scaffolding.Common.Interfaces;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Properties;
using StudioKit.Scaffolding.Tests.TestData.DataAccess;
using StudioKit.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.Tests.BusinessLogic.Services
{
	[TestClass]
	public class ArtifactLeasingServiceTests : BaseTest
	{
		private BaseTestDependencies _deps;
		private FileArtifact _artifact;

		[TestInitialize]
		public void Startup()
		{
			_deps = new BaseTestDependencies(BaseTestDependencies.StartDate);
			_artifact = new FileArtifact { FileName = "test.doc", Url = "http://hostname/fileartifacts/foo.doc", CreatedById = _deps.BasicUserId };
		}

		[TestMethod]
		public async Task AssociateTotp_ShouldReturnToken()
		{
			using (var dbContext = new TestBaseDbContext(DbConnectionFactory.CreateTransient(), _deps.DateTimeProvider, _deps.BasicUserPrincipalProvider))
			{
				await _deps.SeedRolesAndUsersAsync(dbContext);
				dbContext.Artifacts.Add(_artifact);
				await dbContext.SaveChangesAsync();

				const string totpSharedSecret = "so-secret";
				var service = new ArtifactLeasingService(dbContext, _deps.BlobStorage, _deps.DateTimeProvider, totpSharedSecret, 60);
				var token = service.AssociateTotp(_artifact.Id).Replace('-', '/').Replace('_', '+');
				var substrings = token.Split('|');
				Assert.AreEqual(substrings.Length, 2);
				var salt = substrings[0];
				var payload = substrings[1];
				// ReSharper disable once ReturnValueOfPureMethodIsNotUsed
				// Ensure the following do not throw
				// TODO: figure out how to mock BouncyCastle's SecureRandom seed generation
				var saltByteArray = Convert.FromBase64String(salt);
				var plaintext = Encryption.DecryptStringAES(payload, totpSharedSecret, saltByteArray);
				substrings = plaintext.Split('|');
				Assert.AreEqual(substrings.Length, 2);
			}
		}

		[TestMethod]
		public void ArtifactFromToken_ShouldThrowWithInvalidParameters()
		{
			Assert.ThrowsAsync<EntityNotFoundException>(
				Task.Run(async () =>
				{
					using (var dbContext = new TestBaseDbContext(DbConnectionFactory.CreateTransient(), _deps.DateTimeProvider, _deps.BasicUserPrincipalProvider))
					{
						const string totpSharedSecret = "so-secret";
						var service = new ArtifactLeasingService(dbContext, _deps.BlobStorage, _deps.DateTimeProvider, totpSharedSecret, 60);
						await service.ArtifactFromTokenAsync(null);
					}
				}));

			Assert.ThrowsAsync<EntityNotFoundException>(
				Task.Run(async () =>
				{
					using (var dbContext = new TestBaseDbContext(DbConnectionFactory.CreateTransient(), _deps.DateTimeProvider, _deps.BasicUserPrincipalProvider))
					{
						const string totpSharedSecret = "so-secret";
						var service = new ArtifactLeasingService(dbContext, _deps.BlobStorage, _deps.DateTimeProvider, totpSharedSecret, 60);
						await service.ArtifactFromTokenAsync("BADBAD");
					}
				}));

			Assert.ThrowsAsync<EntityNotFoundException>(
				Task.Run(async () =>
				{
					using (var dbContext = new TestBaseDbContext(DbConnectionFactory.CreateTransient(), _deps.DateTimeProvider, _deps.BasicUserPrincipalProvider))
					{
						const string totpSharedSecret = "so-secret";
						const int totpDuration = 1;
						var service = new ArtifactLeasingService(dbContext, _deps.BlobStorage, _deps.DateTimeProvider, totpSharedSecret, totpDuration);
						var totpCode = service.AssociateTotp(99);
						Thread.Sleep(totpDuration * 2);
						await service.ArtifactFromTokenAsync(totpCode);
					}
				}));

			Assert.ThrowsAsync<EntityNotFoundException>(
				Task.Run(async () =>
				{
					using (var dbContext = new TestBaseDbContext(DbConnectionFactory.CreateTransient(), _deps.DateTimeProvider, _deps.BasicUserPrincipalProvider))
					{
						const string totpSharedSecret = "so-secret";
						const int totpDuration = 1;
						var service = new ArtifactLeasingService(dbContext, _deps.BlobStorage, _deps.DateTimeProvider, totpSharedSecret, totpDuration);
						await service.ArtifactFromTokenAsync("foo|bar|baz");
					}
				}));

			Assert.ThrowsAsync<EntityNotFoundException>(
				Task.Run(async () =>
				{
					using (var dbContext = new TestBaseDbContext(DbConnectionFactory.CreateTransient(), _deps.DateTimeProvider, _deps.BasicUserPrincipalProvider))
					{
						const string totpSharedSecret = "so-secret";
						const int totpDuration = 1;
						var service = new ArtifactLeasingService(dbContext, _deps.BlobStorage, _deps.DateTimeProvider, totpSharedSecret, totpDuration);
						var (salt, encryptedPayload) = Encryption.EncryptStringAES("foobar", totpSharedSecret);

						await service.ArtifactFromTokenAsync($"{salt}|{encryptedPayload}".Replace('/', '-').Replace('+', '_'));
					}
				}));

			Assert.ThrowsAsync<EntityNotFoundException>(
				Task.Run(async () =>
				{
					using (var dbContext = new TestBaseDbContext(DbConnectionFactory.CreateTransient(), _deps.DateTimeProvider, _deps.BasicUserPrincipalProvider))
					{
						const string totpSharedSecret = "so-secret";
						const int totpDuration = 1;
						var service = new ArtifactLeasingService(dbContext, _deps.BlobStorage, _deps.DateTimeProvider, totpSharedSecret, totpDuration);
						var (salt, encryptedPayload) = Encryption.EncryptStringAES("foo|bar|baz", totpSharedSecret);

						await service.ArtifactFromTokenAsync($"{salt}|{encryptedPayload}".Replace('/', '-').Replace('+', '_'));
					}
				}));
		}

		[TestMethod]
		public void StreamArtifactAsync_ShouldThrowWithInvalidParameters()
		{
			Assert.ThrowsAsync<ArgumentNullException>(
				Task.Run(async () =>
				{
					using (var dbContext = new TestBaseDbContext(DbConnectionFactory.CreateTransient(), _deps.DateTimeProvider, _deps.BasicUserPrincipalProvider))
					{
						const string totpSharedSecret = "so-secret";
						var service = new ArtifactLeasingService(dbContext, _deps.BlobStorage, _deps.DateTimeProvider, totpSharedSecret, 60);
						await service.StreamArtifactAsync(null, new MemoryStream());
					}
				}));

			Assert.ThrowsAsync<ArgumentNullException>(
				Task.Run(async () =>
				{
					using (var dbContext = new TestBaseDbContext(DbConnectionFactory.CreateTransient(), _deps.DateTimeProvider, _deps.BasicUserPrincipalProvider))
					{
						const string totpSharedSecret = "so-secret";
						var service = new ArtifactLeasingService(dbContext, _deps.BlobStorage, _deps.DateTimeProvider, totpSharedSecret, 60);
						await service.StreamArtifactAsync(new FileArtifact(), null);
					}
				}),
				$"Value cannot be null.{Environment.NewLine}Parameter name: stream");
		}

		[TestMethod]
		public async Task StreamArtifactAsync_ShouldThrowWithExpiredCode()
		{
			var mockDateTimeProvider = new Mock<IDateTimeProvider>();
			mockDateTimeProvider.SetupGet(d => d.UtcNow).Returns(new Queue<DateTime>(new[]
			{
				BaseTestDependencies.StartDate,
				BaseTestDependencies.StartDate.AddSeconds(2)
			}).Dequeue);
			var dateTimeProvider = mockDateTimeProvider.Object;

			using (var dbContext = new TestBaseDbContext(DbConnectionFactory.CreateTransient(), _deps.DateTimeProvider, _deps.BasicUserPrincipalProvider))
			{
				await _deps.SeedRolesAndUsersAsync(dbContext);
				dbContext.Artifacts.Add(_artifact);
				await dbContext.SaveChangesAsync();

				const string totpSharedSecret = "so-secret";
				const int totpDurationSeconds = 1;
				var service = new ArtifactLeasingService(dbContext, _deps.BlobStorage, dateTimeProvider, totpSharedSecret,
					totpDurationSeconds);
				var token = service.AssociateTotp(_artifact.Id);
				Assert.ThrowsAsync<EntityNotFoundException>(
					Task.Run(async () =>
					{
						await service.ArtifactFromTokenAsync(token);
					}));
			}
		}

		[TestMethod]
		public async Task StreamArtifactAsync_ShouldThrowIfStreamIsNotWritable()
		{
			using (var dbContext = new TestBaseDbContext(DbConnectionFactory.CreateTransient(), _deps.DateTimeProvider, _deps.BasicUserPrincipalProvider))
			{
				await _deps.SeedRolesAndUsersAsync(dbContext);
				dbContext.Artifacts.Add(_artifact);
				await dbContext.SaveChangesAsync();

				const string totpSharedSecret = "so-secret";
				const int totpDurationSeconds = 60;
				var service = new ArtifactLeasingService(dbContext, _deps.BlobStorage, _deps.DateTimeProvider, totpSharedSecret,
					totpDurationSeconds);
				var totpCode = service.AssociateTotp(_artifact.Id);
				var stream = new MemoryStream(new byte[] { 0x00 }, false);
				Assert.ThrowsAsync<ApplicationException>(
					Task.Run(async () =>
					{
						var artifact = await service.ArtifactFromTokenAsync(totpCode);
						await service.StreamArtifactAsync(artifact, stream);
					}), Strings.StreamNotWritable);
			}
		}

		[TestMethod]
		public async Task StreamArtifactAsync_ShouldStreamArtifact()
		{
			var azureBlobStorageMock = new Mock<IBlobStorage>();
			azureBlobStorageMock
				.Setup(s =>
					s.WriteBlockBlobToStreamAsync(
						It.IsAny<Stream>(),
						It.IsAny<string>(),
						It.IsAny<CancellationToken>()))
				.Callback<Stream, string, CancellationToken>((stream, pathToBlob, cancellationToken) =>
				{
					var bytes = Encoding.UTF8.GetBytes("Hello World");
					stream.Write(bytes, 0, bytes.Length);
				})
				.Returns(Task.FromResult(0));

			using (var dbContext = new TestBaseDbContext(DbConnectionFactory.CreateTransient(), _deps.DateTimeProvider, _deps.BasicUserPrincipalProvider))
			{
				await _deps.SeedRolesAndUsersAsync(dbContext);
				dbContext.Artifacts.Add(_artifact);
				await dbContext.SaveChangesAsync();

				const string totpSharedSecret = "so-secret";
				const int totpDurationSeconds = 60;
				var service = new ArtifactLeasingService(dbContext, azureBlobStorageMock.Object, _deps.DateTimeProvider, totpSharedSecret,
					totpDurationSeconds);
				var totpCode = service.AssociateTotp(_artifact.Id);
				var artifact = await service.ArtifactFromTokenAsync(totpCode, CancellationToken.None);
				var stream = new MemoryStream(new byte[Encoding.UTF8.GetBytes("Hello World").Length]);
				await service.StreamArtifactAsync(artifact, stream, CancellationToken.None);
				stream.Position = 0;
				var reader = new StreamReader(stream);
				Assert.AreEqual("Hello World", reader.ReadToEnd());
			}
		}
	}
}