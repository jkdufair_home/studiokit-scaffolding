﻿using Microsoft.AspNet.Identity;
using Moq;
using StudioKit.Cloud.Storage.Blob;
using StudioKit.Data.Entity.Identity;
using StudioKit.Scaffolding.BusinessLogic.Services;
using StudioKit.Scaffolding.Common.Interfaces;
using StudioKit.Scaffolding.DataAccess;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Tests.TestData.DataAccess;
using StudioKit.Scaffolding.Tests.TestData.Models;
using StudioKit.Security.Interfaces;
using StudioKit.UserInfoService;
using System;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.Tests
{
	public class BaseTestDependencies
	{
		public string SuperAdminId = "super-admin-id";
		public string AdminId = "admin-id";
		public string BasicUserId = "basic-user-id";

		public TestBaseUser SuperAdmin;
		public TestBaseUser Admin;
		public TestBaseUser BasicUser;

		public IPrincipal SuperAdminPrincipal { get; set; }
		public IPrincipal AdminPrincipal { get; set; }
		public IPrincipal BasicUserPrincipal { get; set; }

		public IPrincipalProvider SuperAdminPrincipalProvider { get; set; }
		public IPrincipalProvider AdminPrincipalProvider { get; set; }
		public IPrincipalProvider BasicUserPrincipalProvider { get; set; }

		public DateTime DateTime { get; set; }
		public IDateTimeProvider DateTimeProvider { get; set; }

		public IShardKeyProvider ShardKeyProvider { get; set; }

		public IUserInfoService UserInfoService { get; set; }

		public IBlobStorage BlobStorage { get; }

		public static DateTime StartDate = new DateTime(2017, 12, 31);

		public BaseTestDependencies(DateTime dateTime)
		{
			(SuperAdminPrincipal, SuperAdminPrincipalProvider) = GeneratePrincipalAndProvider(SuperAdminId);
			(AdminPrincipal, AdminPrincipalProvider) = GeneratePrincipalAndProvider(AdminId);
			(BasicUserPrincipal, BasicUserPrincipalProvider) = GeneratePrincipalAndProvider(BasicUserId);

			DateTime = dateTime;
			var mockDateTimeProvider = new Mock<IDateTimeProvider>();
			mockDateTimeProvider.SetupGet(d => d.UtcNow).Returns(DateTime);
			DateTimeProvider = mockDateTimeProvider.Object;

			var mockShardKeyProvider = new Mock<IShardKeyProvider>();
			mockShardKeyProvider.Setup(x => x.GetShardKey()).Returns("test-shard");
			ShardKeyProvider = mockShardKeyProvider.Object;
			UserInfoService = new Mock<IUserInfoService>().Object;

			var blobStorageMock = new Mock<IBlobStorage>();
			blobStorageMock.Setup(s => s.UriForBlockBlob(It.IsAny<string>()))
				.Returns<string>(path => new Uri($"https://127.0.0.1/{path}"));
			BlobStorage = blobStorageMock.Object;
		}

		private static (IPrincipal, IPrincipalProvider) GeneratePrincipalAndProvider(string userId)
		{
			var principalMock = new Mock<IPrincipal>();
			var identity = new GenericIdentity(userId);
			identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, userId));
			principalMock.SetupGet(m => m.Identity).Returns(identity);
			var principal = principalMock.Object;

			var principalProviderMock = new Mock<IPrincipalProvider>();
			principalProviderMock.Setup(p => p.GetPrincipal()).Returns(principal);
			var principalProvider = principalProviderMock.Object;

			return (principal, principalProvider);
		}

		public static TestBaseUser GenerateUser(string userId)
		{
			return new TestBaseUser
			{
				Id = userId,
				UserName = userId,
				Email = $"{userId}@localhost.com",
				FirstName = $"{userId} LastName",
				LastName = $"{userId} LastName"
			};
		}

		public static UserManager<TestBaseUser> GetUserManager(TestBaseDbContext dbContext)
		{
			var userStore = new UserStore<TestBaseUser>(dbContext);
			return new UserManager<TestBaseUser>(userStore);
		}

		public static RoleManager GetRoleManager(TestBaseDbContext dbContext)
		{
			return new RoleManager(new RoleStore(dbContext));
		}

		public async Task SeedRolesAndUsersAsync(TestBaseDbContext dbContext, CancellationToken cancellationToken = default(CancellationToken))
		{
			BaseSeed.Seed(dbContext);

			dbContext.Configurations.Add(new BaseConfiguration());
			SuperAdmin = GenerateUser(SuperAdminId);
			Admin = GenerateUser(AdminId);
			BasicUser = GenerateUser(BasicUserId);

			dbContext.Users.Add(SuperAdmin);
			dbContext.Users.Add(Admin);
			dbContext.Users.Add(BasicUser);
			await dbContext.SaveChangesAsync(cancellationToken);

			var userManager = GetUserManager(dbContext);

			await userManager.AddToRoleAsync(SuperAdminId, BaseRole.SuperAdmin);
			await userManager.AddToRoleAsync(SuperAdminId, BaseRole.Admin);
			await userManager.AddToRoleAsync(AdminId, BaseRole.Admin);
			await dbContext.SaveChangesAsync(cancellationToken);
		}
	}
}