﻿using Microsoft.AspNet.Identity.EntityFramework;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Models.Interfaces;
using StudioKit.Sharding.Entity.Identity.Models;
using System;
using System.Collections.Generic;

namespace StudioKit.Scaffolding.Tests.TestData.Models
{
	public class TestBaseUser : IdentityUser<string, UserLogin, UserRole, UserClaim>, IBaseUser
	{
		public DateTime DateStored { get; set; }
		public DateTime DateLastUpdated { get; set; }
		public string LastUpdatedById { get; set; }
		public string Uid { get; set; }
		public string CareerAccountAlias { get; set; }
		public string EmployeeNumber { get; set; }
		public string Puid { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string FullName { get; }
		public ICollection<ExternalGroup> ExternalGroups { get; set; }
		public ICollection<ExternalGroupUser> ExternalGroupUsers { get; set; }
		public ICollection<EntityUserRole> EntityUserRoles { get; set; }
		public IEnumerable<GroupUserRole> GroupUserRoles { get; }
		public ICollection<LicenseUser> LicenseUsers { get; set; }
		public IEnumerable<BaseGroup> Groups { get; }
		public IEnumerable<BaseGroup> InstructorGroups { get; }
		public ICollection<Artifact> Artifacts { get; set; }
	}
}