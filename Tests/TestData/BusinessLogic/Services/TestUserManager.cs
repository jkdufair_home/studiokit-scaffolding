﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Services;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Tests.BusinessLogic.Services;
using StudioKit.Scaffolding.Tests.TestData.DataAccess;
using StudioKit.Scaffolding.Tests.TestData.Models;

namespace StudioKit.Scaffolding.Tests.TestData.BusinessLogic.Services
{
	public class TestUserManager : UserManager<TestBaseDbContext, TestBaseUser, TestBaseGroup, BaseConfiguration>
	{
		public TestUserManager(IUserStore<TestBaseUser> store,
			IdentityFactoryOptions<UserManager<TestBaseDbContext, TestBaseUser, TestBaseGroup, BaseConfiguration>> options,
			TestBaseDbContext dbContext,
			IInstructorSandboxService<TestBaseUser> instructorSandboxService,
			IUserSetupService<TestBaseUser> userSetupService)
			: base(store, options, dbContext, instructorSandboxService, userSetupService)
		{
		}
	}
}