﻿using StudioKit.Scaffolding.Common.Interfaces;
using StudioKit.Scaffolding.DataAccess;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Tests.TestData.Models;
using StudioKit.Security.Interfaces;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.Tests.TestData.DataAccess
{
	public class TestBaseDbContext : BaseDbContext<TestBaseUser, TestBaseGroup, BaseConfiguration>
	{
		public TestBaseDbContext(DbConnection dbConnection, IDateTimeProvider dateTimeProvider, IPrincipalProvider principalProvider) :
			base(dbConnection, dateTimeProvider, principalProvider)
		{
		}

		public override async Task<List<TestBaseUser>> BulkLoadUsersAsync(List<TestBaseUser> validUsers, CancellationToken cancellationToken = default(CancellationToken))
		{
			var validUserNames = validUsers.Select(u => u.UserName).ToList();
			var validEmails = validUsers.Select(u => u.Email).ToList();

			var existingUsers = await Users
				.Where(u => validUserNames.Contains(u.UserName) || validEmails.Contains(u.Email))
				.Distinct()
				.ToListAsync(cancellationToken);

			return existingUsers;
		}
	}
}