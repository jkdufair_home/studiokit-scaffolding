using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace StudioKit.Scaffolding.Tests
{
	public class DbProviderSetup
	{
		[AssemblyInitialize]
		public void RegisterEffortProvider()
		{
			Effort.Provider.EffortProviderConfiguration.RegisterProvider();
		}
	}
}