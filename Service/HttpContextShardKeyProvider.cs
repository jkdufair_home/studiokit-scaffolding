﻿using StudioKit.Scaffolding.Common.Interfaces;
using StudioKit.Sharding.Owin.Extensions;
using System.Web;

namespace StudioKit.Scaffolding.Service
{
	public class HttpContextShardKeyProvider : IShardKeyProvider
	{
		public string GetShardKey()
		{
			return HttpContext.Current?.Request.GetOwinContext().GetShardKeyFromHost();
		}
	}
}