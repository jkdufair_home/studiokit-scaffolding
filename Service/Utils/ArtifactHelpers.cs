﻿using FluentValidation;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Properties;
using System;
using System.Collections.Specialized;
using System.IO;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace StudioKit.Scaffolding.Service.Utils
{
	public static class ArtifactHelpers
	{
		private const string TypeDiscriminatorName = "typename";

		private static HttpFileCollection GetRequestFileCollection() => HttpContext.Current.Request.Files;

		private static async Task<NameValueCollection> GetRequestFormDataAsync(HttpRequestMessage httpRequestMessage, CancellationToken cancellationToken = default(CancellationToken))
		{
			var provider = new MultipartFormDataStreamProvider(Path.GetTempPath());
			await httpRequestMessage.Content.ReadAsMultipartAsync(provider, cancellationToken);
			return provider.FormData;
		}

		public static async Task<IArtifactBusinessModel> ExtractArtifactRequestModelAsync(HttpRequestMessage httpRequestMessage, CancellationToken cancellationToken = default(CancellationToken))
		{
			var fileCollection = GetRequestFileCollection();
			var formData = await GetRequestFormDataAsync(httpRequestMessage, cancellationToken);
			var typename = formData.Get(TypeDiscriminatorName);
			if (string.IsNullOrWhiteSpace(typename))
			{
				throw new ValidationException(string.Format(Strings.ParameterRequired, TypeDiscriminatorName));
			}

			switch (typename)
			{
				case nameof(TextArtifact):
					return new TextArtifactBusinessModel
					{
						Text = formData.Get(nameof(TextArtifactBusinessModel.Text))?.Trim(),
						WordCount = Convert.ToInt32(formData.Get(nameof(TextArtifactBusinessModel.WordCount)))
					};

				case nameof(FileArtifact):
					if (fileCollection == null || fileCollection.Count < 1)
					{
						throw new ValidationException(Strings.FileRequired);
					}

					var postedFile = fileCollection[0];
					return new FileArtifactBusinessModel
					{
						//IE returns a file name with the full path of the local computer
						FileName = Path.GetFileName(postedFile.FileName),
						FileStream = postedFile.InputStream,
						ContentType = MimeMapping.GetMimeMapping(postedFile.FileName)
					};

				case nameof(UrlArtifact):
					return new UrlArtifactBusinessModel
					{
						Url = formData.Get(nameof(UrlArtifactBusinessModel.Url))?.Trim(),
					};

				default:
					throw new ValidationException(string.Format(Strings.ParameterInvalid, TypeDiscriminatorName));
			}
		}
	}
}