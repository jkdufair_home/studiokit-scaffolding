﻿using StudioKit.ErrorHandling.WebApi;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace StudioKit.Scaffolding.Service.Filters
{
	public class HandleModelStateErrorsFilterAttribute : ActionFilterAttribute
	{
		public override Task OnActionExecutingAsync(HttpActionContext actionContext, CancellationToken cancellationToken)
		{
			var modelState = actionContext.ModelState;
			if (modelState.IsValid)
				return base.OnActionExecutingAsync(actionContext, cancellationToken);

			actionContext.Response = actionContext.Request.CreateResponse(
				new ApiErrorMessage(HttpStatusCode.BadRequest, HttpStatusCode.BadRequest.ToString(),
					string.Join(",\r\n", modelState.Values.SelectMany(v => v.Errors.Select(e => e.Exception.Message)))));
			return Task.FromResult(0);
		}
	}
}