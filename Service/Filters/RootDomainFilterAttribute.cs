﻿using StudioKit.ErrorHandling.WebApi;
using StudioKit.Sharding;
using StudioKit.Sharding.Owin.Extensions;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace StudioKit.Scaffolding.Service.Filters
{
	public class RootDomainFilterAttribute : AuthorizationFilterAttribute
	{
		public override void OnAuthorization(HttpActionContext actionContext)
		{
			var owinContext = actionContext.Request.GetOwinContext();
			var shardKey = owinContext.GetShardKeyFromHost();
			if (shardKey == ShardDomainConstants.RootShardKey &&
				!actionContext.ActionDescriptor.GetCustomAttributes<AllowRootDomainAttribute>().Any())
			{
				actionContext.Response =
					actionContext.Request.CreateResponse(new ApiErrorMessage(HttpStatusCode.NotFound,
						"Not Found",
						"The requested content was not found."));
				return;
			}
			base.OnAuthorization(actionContext);
		}
	}
}