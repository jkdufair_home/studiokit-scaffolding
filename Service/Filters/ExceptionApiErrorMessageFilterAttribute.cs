﻿using FluentValidation;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.ErrorHandling.WebApi;
using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Filters;

namespace StudioKit.Scaffolding.Service.Filters
{
	public class ExceptionApiErrorMessageFilterAttribute : ExceptionFilterAttribute
	{
		public override Task OnExceptionAsync(HttpActionExecutedContext actionExecutedContext, CancellationToken cancellationToken)
		{
			var statusCode = HttpStatusCode.InternalServerError;
			Exception e = null;

			switch (actionExecutedContext.Exception)
			{
				case EntityNotFoundException entityNotFoundException:
					statusCode = HttpStatusCode.NotFound;
					e = entityNotFoundException;
					break;

				case ForbiddenException forbiddenException:
					statusCode = HttpStatusCode.Forbidden;
					e = forbiddenException;
					break;

				case UnauthorizedException unauthorizedException:
					statusCode = HttpStatusCode.Unauthorized;
					e = unauthorizedException;
					break;

				case ValidationException validationException:
					statusCode = HttpStatusCode.BadRequest;
					e = validationException;
					break;
			}

			if (e == null)
				return base.OnExceptionAsync(actionExecutedContext, cancellationToken);

			actionExecutedContext.Response = actionExecutedContext.Request.CreateResponse(
				new ApiErrorMessage(statusCode, statusCode.ToString(), e.Message));
			return Task.FromResult(0);
		}
	}
}