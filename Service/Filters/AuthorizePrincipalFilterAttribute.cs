﻿using Autofac;
using Autofac.Integration.Owin;
using Microsoft.AspNet.Identity;
using StudioKit.ErrorHandling.WebApi;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Sharding;
using StudioKit.Sharding.Owin.Extensions;
using StudioKit.Web.Http.Extensions;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace StudioKit.Scaffolding.Service.Filters
{
	public class AuthorizePrincipalFilterAttribute : AuthorizationFilterAttribute
	{
		public override void OnAuthorization(HttpActionContext actionContext)
		{
			var owinContext = actionContext.Request.GetOwinContext();

			// Do not allow access on invalid shards
			var shardKey = owinContext.GetShardKeyFromHost();
			if (shardKey == ShardDomainConstants.InvalidShardKey)
			{
				actionContext.Response =
					actionContext.Request.CreateResponse(new ApiErrorMessage(HttpStatusCode.Forbidden,
						"Session Invalid",
						"Your request has been denied. You do not have access."));
				return;
			}

			// Do not continue if request is unauthorized
			if (!actionContext.IsPrincipalAuthenticated())
			{
				base.OnAuthorization(actionContext);
				return;
			}

			// Verify Principal exists
			var principalId = actionContext.RequestContext.Principal.Identity.GetUserId();
			var scope = owinContext.GetAutofacLifetimeScope();
			var service = scope.Resolve<IPrincipalService>();
			var principalExists = service.PrincipalExists(principalId);

			if (!principalExists)
			{
				actionContext.Response =
					actionContext.Request.CreateResponse(new ApiErrorMessage(HttpStatusCode.Forbidden,
						"Session Invalid",
						"Your request has been denied. You do not have access."));
				return;
			}

			base.OnAuthorization(actionContext);
		}
	}
}