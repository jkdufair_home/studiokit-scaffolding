﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;

namespace StudioKit.Scaffolding.Service.Models.Converters
{
	// Deserializing Web Api input into object based on inheritance
	// https://forums.asp.net/t/1917675.aspx?Deserializing+Web+Api+input+into+object+based+on+inheritance+JsonConverter

	public abstract class JsonCreationConverter<T> : JsonConverter
	{
		public override bool CanWrite => false;

		/// <summary>
		/// The name of the property in the JSON object which specifies the subtype
		/// </summary>
		protected const string TypeDiscriminatorName = "typename";

		/// <summary>
		/// Create an instance of objectType, based on the properties in the JSON object
		/// </summary>
		/// <param name="objectType">type of object expected</param>
		/// <param name="jObject">contents of JSON object that will be deserialized</param>
		protected abstract T Create(Type objectType, JObject jObject);

		public override bool CanConvert(Type objectType) =>
			typeof(T).IsAssignableFrom(objectType);

		public override object ReadJson(JsonReader reader, Type objectType,
			object existingValue, JsonSerializer serializer)
		{
			if (reader.TokenType == JsonToken.Null)
				return null;
			// Load JObject from stream
			JObject jObject = JObject.Load(reader);

			// Create target object based on JObject
			T target = Create(objectType, jObject);

			// Populate the object properties
			serializer.Populate(jObject.CreateReader(), target);

			return target;
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer) =>
			throw new NotImplementedException();
	}
}