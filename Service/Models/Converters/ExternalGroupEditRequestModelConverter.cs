﻿using Newtonsoft.Json.Linq;
using StudioKit.ExternalProvider.Lti.Models;
using StudioKit.ExternalProvider.UniTime.Models;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Properties;
using StudioKit.Scaffolding.Service.Models.RequestModels;
using System;

namespace StudioKit.Scaffolding.Service.Models.Converters
{
	public class ExternalGroupEditRequestModelConverter : JsonCreationConverter<IExternalGroupEditRequestModel>
	{
		protected override IExternalGroupEditRequestModel Create(Type objectType, JObject jObject)
		{
			var typename = jObject?.Value<string>(TypeDiscriminatorName)?.ToLower();
			if (typename == null)
			{
				throw new ArgumentNullException(TypeDiscriminatorName);
			}

			if (typename.Equals(nameof(ExternalGroup),
				StringComparison.OrdinalIgnoreCase))
			{
				return new ExternalGroupEditRequestModel();
			}

			if (typename.Equals(nameof(LtiLaunch),
				StringComparison.OrdinalIgnoreCase))
			{
				return new LtiLaunchExternalGroupEditRequestModel();
			}

			if (typename.Equals(nameof(UniTimeGroup),
				StringComparison.OrdinalIgnoreCase))
			{
				return new UniTimeGroupExternalGroupEditRequestModel();
			}

			throw new ArgumentException(string.Format(Strings.ParameterInvalid, TypeDiscriminatorName));
		}
	}
}