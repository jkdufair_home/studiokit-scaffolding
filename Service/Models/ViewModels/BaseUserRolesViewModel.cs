﻿using Microsoft.AspNet.Identity.EntityFramework;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Models;
using System.Collections.Generic;

namespace StudioKit.Scaffolding.Service.Models.ViewModels
{
	public class BaseUserRolesViewModel<TUser> : IViewModel<UserRoleBusinessModel<TUser>>
		where TUser : IdentityUser<string, UserLogin, UserRole, UserClaim>, IUser
	{
		public IViewModel<UserRoleBusinessModel<TUser>> MapFrom(UserRoleBusinessModel<TUser> model)
		{
			Id = model.User.Id;
			FirstName = model.User.FirstName;
			LastName = model.User.LastName;
			Email = model.User.Email;
			Uid = model.User.Uid;
			Roles = model.Roles;
			return this;
		}

		public string Id { get; set; }

		public string FirstName { get; set; }

		public string LastName { get; set; }

		public string Email { get; set; }

		public string Uid { get; set; }

		public IEnumerable<string> Roles { get; set; }
	}
}