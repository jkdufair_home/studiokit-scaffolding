﻿using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.Models.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace StudioKit.Scaffolding.Service.Models.ViewModels
{
	public class BaseCreateEntityUserRolesViewModel<TUser> : IViewModel<CreateEntityUserRolesBusinessModel<TUser>>
		where TUser : IBaseUser
	{
		public IViewModel<CreateEntityUserRolesBusinessModel<TUser>> MapFrom(CreateEntityUserRolesBusinessModel<TUser> model)
		{
			AddedUsers = model.AddedUsers.Select(au =>
				new BaseUserViewModel<TUser, BaseUserBusinessModel<TUser>>()
					.MapFrom(new BaseUserBusinessModel<TUser> { User = au.User }));
			ExistingUsers = model.ExistingUsers.Select(eu =>
				new BaseUserViewModel<TUser, BaseUserBusinessModel<TUser>>()
					.MapFrom(new BaseUserBusinessModel<TUser> { User = eu.User }));
			InvalidIdentifiers = model.InvalidIdentifiers;
			InvalidDomainIdentifiers = model.InvalidDomainIdentifiers;
			return this;
		}

		/// <summary>
		/// Users that were added to the Entity Role, sent in the CreateEntityUserRoles request
		/// </summary>
		public IEnumerable<IViewModel<BaseUserBusinessModel<TUser>>> AddedUsers { get; set; }

		/// <summary>
		/// Users that were already in the Entity Role, sent in the CreateEntityUserRoles request
		/// </summary>
		public IEnumerable<IViewModel<BaseUserBusinessModel<TUser>>> ExistingUsers { get; set; }

		public IEnumerable<string> InvalidIdentifiers { get; set; }

		public string AllowedDomains { get; set; }

		public IEnumerable<string> InvalidDomainIdentifiers { get; set; }
	}
}