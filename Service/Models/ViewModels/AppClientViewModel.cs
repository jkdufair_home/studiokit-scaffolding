﻿using StudioKit.Security;
using System;

namespace StudioKit.Scaffolding.Service.Models.ViewModels
{
	public class AppClientViewModel
	{
		public AppClientViewModel(AppClient appClient)
		{
			if (appClient == null) throw new ArgumentNullException(nameof(appClient));

			ClientId = appClient.ClientId;
			CurrentVersion = appClient.CurrentVersion;
			MinVersion = appClient.MinVersion;
		}

		public string ClientId { get; set; }

		public string CurrentVersion { get; set; }

		public string MinVersion { get; set; }
	}
}