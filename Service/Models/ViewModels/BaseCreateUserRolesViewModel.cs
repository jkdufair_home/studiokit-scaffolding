﻿using Microsoft.AspNet.Identity.EntityFramework;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.Models.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace StudioKit.Scaffolding.Service.Models.ViewModels
{
	public class BaseCreateUserRolesViewModel<TUser> : IViewModel<CreateUserRolesBusinessModel<TUser>>
		where TUser : IdentityUser<string, UserLogin, UserRole, UserClaim>, IBaseUser, new()
	{
		public IViewModel<CreateUserRolesBusinessModel<TUser>> MapFrom(CreateUserRolesBusinessModel<TUser> model)
		{
			AddedUsers = model.AddedUsers.Select(au => new BaseUserViewModel<TUser, BaseUserBusinessModel<TUser>>().MapFrom(
				new BaseUserBusinessModel<TUser> { User = au }));
			ExistingUsers = model.ExistingUsers.Select(eu => new BaseUserViewModel<TUser, BaseUserBusinessModel<TUser>>().MapFrom(
				new BaseUserBusinessModel<TUser> { User = eu }));
			InvalidIdentifiers = model.InvalidIdentifiers;
			AllowedDomains = model.AllowedDomains;
			InvalidDomainIdentifiers = model.InvalidDomainIdentifiers;
			return this;
		}

		public IEnumerable<IViewModel<BaseUserBusinessModel<TUser>>> AddedUsers { get; set; }

		/// <summary>
		/// Users that were already in the Role, sent in the CreateUserRoles request
		/// </summary>
		public IEnumerable<IViewModel<BaseUserBusinessModel<TUser>>> ExistingUsers { get; set; }

		public IEnumerable<string> InvalidIdentifiers { get; set; }

		public string AllowedDomains { get; set; }

		public IEnumerable<string> InvalidDomainIdentifiers { get; set; }
	}
}