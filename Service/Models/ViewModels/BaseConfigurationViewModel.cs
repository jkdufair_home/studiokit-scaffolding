﻿using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.Models;
using System;

namespace StudioKit.Scaffolding.Service.Models.ViewModels
{
	public class BaseConfigurationViewModel<TConfiguration> : IViewModel<TConfiguration>
		where TConfiguration : BaseConfiguration, new()
	{
		public virtual IViewModel<TConfiguration> MapFrom(TConfiguration configuration)
		{
			if (configuration == null) throw new ArgumentNullException(nameof(configuration));

			Id = configuration.Id;
			Name = configuration.Name;
			UserSupportEmail = configuration.UserSupportEmail;
			AllowedDomains = configuration.AllowedDomains;
			Image = configuration.Image;
			DefaultTimeZoneId = configuration.DefaultTimeZoneId;
			CaliperEnabled = configuration.CaliperEnabled;
			CaliperEventStoreHostname = configuration.CaliperEventStoreHostname;
			CaliperPersonNamespace = configuration.CaliperPersonNamespace;
			RedisShouldCollectStatistics = configuration.RedisShouldCollectStatistics;
			IsInstructorSandboxEnabled = configuration.IsInstructorSandboxEnabled;
			DemoExpirationDayLimit = configuration.DemoExpirationDayLimit;
			return this;
		}

		public int Id { get; set; }

		public string Name { get; set; }

		public string UserSupportEmail { get; set; }

		public string AllowedDomains { get; set; }

		public byte[] Image { get; set; }

		public string DefaultTimeZoneId { get; set; }

		public bool CaliperEnabled { get; set; }

		public string CaliperEventStoreHostname { get; set; }

		public string CaliperPersonNamespace { get; set; }

		public bool RedisShouldCollectStatistics { get; set; }

		public bool IsInstructorSandboxEnabled { get; set; }

		public int? DemoExpirationDayLimit { get; set; }
	}
}