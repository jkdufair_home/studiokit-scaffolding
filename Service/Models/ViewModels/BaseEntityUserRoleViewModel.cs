﻿using Microsoft.AspNet.Identity.EntityFramework;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Models;
using System.Collections.Generic;

namespace StudioKit.Scaffolding.Service.Models.ViewModels
{
	public class BaseEntityUserRoleViewModel<TUser> : IViewModel<EntityUserRoleBusinessModel<TUser>>
		where TUser : IdentityUser<string, UserLogin, UserRole, UserClaim>, IUser
	{
		public virtual IViewModel<EntityUserRoleBusinessModel<TUser>> MapFrom(EntityUserRoleBusinessModel<TUser> model)
		{
			Id = model.User.Id;
			FirstName = model.User.FirstName;
			LastName = model.User.LastName;
			Email = model.User.Email;
			Uid = model.User.Uid;
			EntityId = model.EntityId;
			Roles = model.Roles;
			return this;
		}

		public string Id { get; set; }

		public string FirstName { get; set; }

		public string LastName { get; set; }

		public string Email { get; set; }

		public string Uid { get; set; }

		public int EntityId { get; set; }

		public List<string> Roles { get; set; }
	}
}