using Microsoft.AspNet.Identity.EntityFramework;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace StudioKit.Scaffolding.Service.Models.ViewModels
{
	public class BaseGroupViewModel<TGroup, TGroupBusinessModel, TUser> : IViewModel<TGroupBusinessModel>
		where TUser : IdentityUser<string, UserLogin, UserRole, UserClaim>, IUser

		where TGroup : BaseGroup
		where TGroupBusinessModel : BaseGroupBusinessModel<TGroup, TUser>
	{
		public virtual IViewModel<TGroupBusinessModel> MapFrom(TGroupBusinessModel businessModel)
		{
			if (businessModel == null) throw new ArgumentNullException(nameof(businessModel));
			if (businessModel.Group == null) throw new ArgumentNullException(nameof(businessModel.Group));

			Id = businessModel.Group.Id;
			ExternalTermId = businessModel.Group.ExternalTermId;
			Name = businessModel.Group.Name;
			StartDate = businessModel.Group.StartDate;
			EndDate = businessModel.Group.EndDate;
			IsDeleted = businessModel.Group.IsDeleted;
			IsRosterSyncEnabled = businessModel.IsRosterSyncEnabled;
			ExternalGroups = businessModel.ExternalGroups.Select(b => new ExternalGroupViewModel(b));

			Roles = businessModel.Roles;
			Activities = businessModel.Activities;
			Owners = businessModel.Owners.Select(o => new BaseEntityUserViewModel<TUser>().MapFrom(o));
			Graders = businessModel.Graders.Select(o => new BaseEntityUserViewModel<TUser>().MapFrom(o));
			return this;
		}

		public int Id { get; set; }

		public string Name { get; set; }

		public int? ExternalTermId { get; set; }

		public DateTime? StartDate { get; set; }

		public DateTime? EndDate { get; set; }
		public bool IsDeleted { get; private set; }
		public bool IsRosterSyncEnabled { get; set; }

		public IEnumerable<string> Roles { get; set; } = new List<string>();

		public IEnumerable<string> Activities { get; set; } = new List<string>();

		public IEnumerable<IViewModel<EntityUserBusinessModel<TUser>>> Owners { get; set; } = new List<BaseEntityUserViewModel<TUser>>();

		public IEnumerable<IViewModel<EntityUserBusinessModel<TUser>>> Graders { get; set; } = new List<BaseEntityUserViewModel<TUser>>();

		public IEnumerable<ExternalGroupViewModel> ExternalGroups { get; set; } = new List<ExternalGroupViewModel>();
	}
}