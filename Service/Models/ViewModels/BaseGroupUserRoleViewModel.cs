﻿using Microsoft.AspNet.Identity.EntityFramework;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.Models.Interfaces;

namespace StudioKit.Scaffolding.Service.Models.ViewModels
{
	public class BaseGroupUserRoleViewModel<TUser> : BaseEntityUserRoleViewModel<TUser>
		where TUser : IdentityUser<string, UserLogin, UserRole, UserClaim>, IBaseUser
	{
		public override IViewModel<EntityUserRoleBusinessModel<TUser>> MapFrom(EntityUserRoleBusinessModel<TUser> model)
		{
			base.MapFrom(model);
			IsExternal = ((GroupUserRoleBusinessModel<TUser>)model).IsExternal;
			return this;
		}

		public bool IsExternal { get; set; }
	}
}