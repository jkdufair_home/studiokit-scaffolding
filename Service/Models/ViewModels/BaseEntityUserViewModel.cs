﻿using Microsoft.AspNet.Identity.EntityFramework;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Models;

namespace StudioKit.Scaffolding.Service.Models.ViewModels
{
	public class BaseEntityUserViewModel<TUser> : IViewModel<EntityUserBusinessModel<TUser>>
		where TUser : IdentityUser<string, UserLogin, UserRole, UserClaim>, IUser
	{
		public IViewModel<EntityUserBusinessModel<TUser>> MapFrom(EntityUserBusinessModel<TUser> model)
		{
			Id = model.User.Id;
			FirstName = model.User.FirstName;
			LastName = model.User.LastName;
			Email = model.User.Email;
			Uid = model.User.Uid;
			EntityId = model.EntityId;
			return this;
		}

		public string Id { get; set; }

		public string FirstName { get; set; }

		public string LastName { get; set; }

		public string Email { get; set; }

		public string Uid { get; set; }

		public int EntityId { get; set; }
	}
}