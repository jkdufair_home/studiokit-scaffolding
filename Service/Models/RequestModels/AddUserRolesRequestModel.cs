﻿using System.Collections.Generic;

namespace StudioKit.Scaffolding.Service.Models.RequestModels
{
	public class AddUserRolesRequestModel
	{
		public string RoleName { get; set; }

		public List<string> Identifiers { get; set; }
	}
}