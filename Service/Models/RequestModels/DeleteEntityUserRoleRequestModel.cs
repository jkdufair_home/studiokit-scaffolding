﻿namespace StudioKit.Scaffolding.Service.Models.RequestModels
{
	public class DeleteEntityUserRoleRequestModel
	{
		public int EntityId { get; set; }

		public string Typename { get; set; }

		public string RoleName { get; set; }
	}
}