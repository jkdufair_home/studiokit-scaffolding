﻿using System.Runtime.Serialization;

namespace StudioKit.Scaffolding.Service.Models.RequestModels
{
	[DataContract]
	public class RoleRequestModel
	{
		[DataMember(Name = "emails")]
		public string Emails { get; set; }

		[DataMember(Name = "roleName")]
		public string RoleName { get; set; }
	}
}