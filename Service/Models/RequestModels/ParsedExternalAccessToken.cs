﻿using Microsoft.AspNet.Identity;
using StudioKit.Scaffolding.Common;
using System.Collections.Generic;
using System.Security.Claims;

namespace StudioKit.Scaffolding.Service.Models.RequestModels
{
	public class ParsedExternalAccessToken
	{
		public string LoginProvider { get; set; }

		public string AccessToken { get; set; }

		public string ExpiresIn { get; set; }

		public string ProviderKey { get; set; }

		public string AppId { get; set; }

		public bool IsValid { get; set; }

		public string Email { get; set; }

		public string FirstName { get; set; }

		public string LastName { get; set; }

		public List<Claim> Claims
		{
			get
			{
				var claims = new List<Claim>
				{
					new Claim(ScaffoldingClaimTypes.AccessToken, AccessToken, ClaimValueTypes.String, LoginProvider),
					new Claim(ScaffoldingClaimTypes.AppId, AppId, ClaimValueTypes.String, LoginProvider),
					new Claim(ClaimTypes.NameIdentifier, ProviderKey, ClaimValueTypes.String, LoginProvider),
					new Claim(ClaimTypes.Email, Email, ClaimValueTypes.String, LoginProvider),
					new Claim(ClaimTypes.GivenName, FirstName, ClaimValueTypes.String, LoginProvider),
					new Claim(ClaimTypes.Surname, LastName, ClaimValueTypes.String, LoginProvider),
				};
				if (ExpiresIn != null)
					claims.Add(new Claim(ScaffoldingClaimTypes.ExpiresIn, ExpiresIn, ClaimValueTypes.String, LoginProvider));
				return claims;
			}
		}

		public UserLoginInfo Login => new UserLoginInfo(LoginProvider, ProviderKey);
	}
}