﻿using System.Runtime.Serialization;

namespace StudioKit.Scaffolding.Service.Models.RequestModels
{
	[DataContract]
	public class SingleRoleRequestModel
	{
		[DataMember(Name = "email")]
		public string Email { get; set; }

		[DataMember(Name = "roleName")]
		public string RoleName { get; set; }
	}
}