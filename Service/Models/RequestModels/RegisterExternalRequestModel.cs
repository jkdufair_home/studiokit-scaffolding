using System.ComponentModel.DataAnnotations;

namespace StudioKit.Scaffolding.Service.Models.RequestModels
{
	public class RegisterExternalRequestModel
	{
		[Required]
		[Display(Name = "Access token")]
		public string AccessToken { get; set; }

		[Required]
		public string Provider { get; set; }
	}
}