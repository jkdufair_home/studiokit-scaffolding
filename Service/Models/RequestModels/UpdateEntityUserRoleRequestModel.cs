﻿namespace StudioKit.Scaffolding.Service.Models.RequestModels
{
	public class UpdateEntityUserRoleRequestModel
	{
		public int EntityId { get; set; }

		public string Typename { get; set; }

		public string UserId { get; set; }

		public string NewRoleName { get; set; }

		public string CurrentRoleName { get; set; }
	}
}