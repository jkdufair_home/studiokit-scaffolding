﻿using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.Service.Models.RequestModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace StudioKit.Scaffolding.Service.Controllers
{
	public class BaseEntityUserRolesController<TEntityUserRoleBusinessModel, TCreateEntityUserRoleBusinessModel> : ApiController
	{
		private readonly Func<Dictionary<string, IEntityUserRoleService<TEntityUserRoleBusinessModel, TCreateEntityUserRoleBusinessModel>>> _serviceMappingProviderFunc;
		private readonly Func<string, Func<TEntityUserRoleBusinessModel, IViewModel<TEntityUserRoleBusinessModel>>> _viewModelProviderFunc;
		private readonly Func<string, Func<TCreateEntityUserRoleBusinessModel, IViewModel<TCreateEntityUserRoleBusinessModel>>> _createViewModelProviderFunc;

		public BaseEntityUserRolesController(
			Func<Dictionary<string, IEntityUserRoleService<TEntityUserRoleBusinessModel, TCreateEntityUserRoleBusinessModel>>> serviceMappingProviderFunc,
			Func<string, Func<TEntityUserRoleBusinessModel, IViewModel<TEntityUserRoleBusinessModel>>> viewModelProviderFunc,
			Func<string, Func<TCreateEntityUserRoleBusinessModel, IViewModel<TCreateEntityUserRoleBusinessModel>>> createViewModelProviderFunc)
		{
			_serviceMappingProviderFunc = serviceMappingProviderFunc ?? throw new ArgumentNullException(nameof(serviceMappingProviderFunc));
			_viewModelProviderFunc = viewModelProviderFunc ?? throw new ArgumentNullException(nameof(viewModelProviderFunc));
			_createViewModelProviderFunc = createViewModelProviderFunc ?? throw new ArgumentNullException(nameof(createViewModelProviderFunc));
		}

		[HttpGet]
		[Route("")]
		public async Task<IHttpActionResult> GetEntityUserRolesAsync(int entityId, string typename, CancellationToken cancellationToken = default(CancellationToken))
		{
			var service = GetService(typename);
			var entityUserRoles = await service.GetEntityUserRolesAsync(entityId, User, cancellationToken);
			return Ok(entityUserRoles.Select(_viewModelProviderFunc(typename)));
		}

		[HttpPost]
		[Route("")]
		public async Task<IHttpActionResult> CreateEntityUserRoleAsync([FromBody] AddEntityUserRolesRequestModel requestModel, CancellationToken cancellationToken = default(CancellationToken))
		{
			var service = GetService(requestModel.Typename);
			var createdEntityUserRoles = await service.CreateEntityUserRolesAsync(requestModel.EntityId, requestModel.Identifiers,
				requestModel.RoleName, User, cancellationToken);
			return Ok(_createViewModelProviderFunc(requestModel.Typename)(createdEntityUserRoles));
		}

		[HttpDelete]
		[Route("{userId}")]
		public async Task<IHttpActionResult> DeleteEntityUserRoleAsync(string userId, [FromBody] DeleteEntityUserRoleRequestModel requestModel, CancellationToken cancellationToken = default(CancellationToken))
		{
			var service = GetService(requestModel.Typename);
			await service.DeleteEntityUserRoleAsync(requestModel.EntityId, userId, requestModel.RoleName, User, cancellationToken);
			return StatusCode(HttpStatusCode.NoContent);
		}

		[HttpPost]
		[Route("swapUserRoles")]
		public async Task<IHttpActionResult> SwapUserRolesAsync(UpdateEntityUserRoleRequestModel requestModel, CancellationToken cancellationToken = default(CancellationToken))
		{
			var service = GetService(requestModel.Typename);
			await service.SwapEntityUserRolesAsync(requestModel.EntityId, requestModel.UserId, requestModel.NewRoleName,
				requestModel.CurrentRoleName, User, cancellationToken);
			return Ok();
		}

		#region Private Methods

		private IEntityUserRoleService<TEntityUserRoleBusinessModel, TCreateEntityUserRoleBusinessModel> GetService(string typename)
		{
			var mapping = _serviceMappingProviderFunc();
			return mapping[typename];
		}

		#endregion Private Methods
	}
}