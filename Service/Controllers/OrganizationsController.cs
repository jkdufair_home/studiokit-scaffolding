﻿using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.Service.Filters;
using StudioKit.Sharding;
using StudioKit.Sharding.Models;
using StudioKit.Sharding.Utils;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace StudioKit.Scaffolding.Service.Controllers
{
	public class OrganizationsController : ApiController
	{
		private readonly IOrganizationService _organizationService;

		public OrganizationsController(IOrganizationService organizationService)
		{
			_organizationService = organizationService;
		}

		[AllowAnonymous]
		[AllowRootDomain]
		[HttpGet]
		[Route("api/organizations")]
		public async Task<IHttpActionResult> GetAsync(CancellationToken cancellationToken = default(CancellationToken))
		{
			var result = new List<Organization>();
			await ShardUtils.PerformAsyncFuncOnAllShards(ShardManager.Instance.DefaultShardMap, async shardKey =>
			{
				result.Add(await _organizationService.GetOrganizationAsync(shardKey, cancellationToken));
			});
			return Ok(result);
		}
	}
}