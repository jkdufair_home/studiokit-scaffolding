using StudioKit.Scaffolding.BusinessLogic.Services;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace StudioKit.Scaffolding.Service.Controllers
{
	[RoutePrefix("api/artifacts")]
	public class ArtifactsController : ApiController
	{
		private readonly ArtifactLeasingService _artifactLeasingService;

		public ArtifactsController(ArtifactLeasingService artifactLeasingService)
		{
			_artifactLeasingService = artifactLeasingService;
		}

		[AllowAnonymous]
		[HttpGet]
		[Route("{artifactToken}", Name = nameof(GetAssignmentArtifactFromTokenAsync))]
		public async Task<HttpResponseMessage> GetAssignmentArtifactFromTokenAsync(string artifactToken, CancellationToken cancellationToken = default(CancellationToken))
		{
			var artifact = await _artifactLeasingService.ArtifactFromTokenAsync(artifactToken, cancellationToken);
			var uri = new Uri(artifact.Url);
			var fileName = uri.Segments[uri.Segments.Length - 1];
			var response = new HttpResponseMessage(HttpStatusCode.OK)
			{
				Content = new PushStreamContent(async (stream, _, __) =>
				{
					await _artifactLeasingService.StreamArtifactAsync(artifact, stream, cancellationToken);
					stream.Close();
				})
			};
			response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
			response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment") { FileName = fileName };
			return response;
		}
	}
}