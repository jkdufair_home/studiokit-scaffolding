﻿using StudioKit.ErrorHandling.WebApi;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace StudioKit.Scaffolding.Service.Controllers
{
	public class RootController : ApiController
	{
		[AllowAnonymous]
		[Route("")]
		[HttpGet]
		public IHttpActionResult Index()
		{
			return Ok();
		}

		[AllowAnonymous]
		[Route("error")]
		[HttpGet]
		public HttpResponseMessage Error()
		{
			return Request.CreateResponse(HttpStatusCode.InternalServerError,
				new ApiErrorMessage(HttpStatusCode.InternalServerError, "Error", "An error has occurred."));
		}

		[AllowAnonymous]
		[Route("not-found")]
		[HttpGet]
		public HttpResponseMessage GetNotFound()
		{
			return Request.CreateResponse(HttpStatusCode.NotFound,
				new ApiErrorMessage(HttpStatusCode.NotFound, "Not Found", "The requested resource was not found."));
		}
	}
}