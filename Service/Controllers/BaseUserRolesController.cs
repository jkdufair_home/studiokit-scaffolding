﻿using Microsoft.AspNet.Identity.EntityFramework;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.Models.Interfaces;
using StudioKit.Scaffolding.Service.Models.RequestModels;
using StudioKit.Scaffolding.Service.Models.ViewModels;
using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace StudioKit.Scaffolding.Service.Controllers
{
	public class BaseUserRolesController<TUser, TUserRoleBusinessModel, TViewModel, TCreateViewModel> : ApiController
		where TUser : IdentityUser<string, UserLogin, UserRole, UserClaim>, IBaseUser, new()
		where TUserRoleBusinessModel : UserRoleBusinessModel<TUser>
		where TViewModel : BaseUserRolesViewModel<TUser>, new()
		where TCreateViewModel : BaseCreateUserRolesViewModel<TUser>, new()
	{
		private readonly IUserRoleService<TUser, TUserRoleBusinessModel> _userRoleService;

		public BaseUserRolesController(IUserRoleService<TUser, TUserRoleBusinessModel> userRoleService)
		{
			_userRoleService = userRoleService ?? throw new ArgumentNullException(nameof(userRoleService));
		}

		[HttpGet]
		[Route("")]
		public async Task<IHttpActionResult> GetUserRolesAsync(string roleName, CancellationToken cancellationToken = default(CancellationToken))
		{
			var userRoles = await _userRoleService.GetUserRolesAsync(roleName, User, cancellationToken);
			return Ok(userRoles.Select(ur => new TViewModel().MapFrom(ur)));
		}

		[HttpPost]
		[Route("")]
		public async Task<IHttpActionResult> CreateEntityUserRoleAsync([FromBody] AddUserRolesRequestModel requestModel, CancellationToken cancellationToken = default(CancellationToken))
		{
			var createdEntityUserRoles = await _userRoleService.CreateUserRolesAsync(requestModel.RoleName, requestModel.Identifiers, User, cancellationToken);
			return Ok(new TCreateViewModel().MapFrom(createdEntityUserRoles));
		}

		[HttpDelete]
		[Route("{userId}")]
		public async Task<IHttpActionResult> DeleteEntityUserRoleAsync(string userId, string roleName, CancellationToken cancellationToken = default(CancellationToken))
		{
			await _userRoleService.DeleteUserRoleAsync(userId, roleName, User, cancellationToken);
			return StatusCode(HttpStatusCode.NoContent);
		}
	}
}