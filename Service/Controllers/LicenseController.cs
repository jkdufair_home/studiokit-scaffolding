﻿using StudioKit.Scaffolding.DataAccess.Extensions;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace StudioKit.Scaffolding.Service.Controllers
{
	[RoutePrefix("api/license")]
	public class LicenseController : ApiController
	{
		[AllowAnonymous]
		[HttpGet]
		public async Task<IHttpActionResult> GetAsync(CancellationToken cancellationToken = default(CancellationToken))
		{
			var owinContext = Request.GetOwinContext();
			var license = await owinContext.GetLicenseAsync(cancellationToken);
			if (license == null)
				return Ok();
			return Ok(license);
		}
	}
}