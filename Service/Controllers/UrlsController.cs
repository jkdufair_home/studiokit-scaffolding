﻿using StudioKit.TransientFaultHandling.Http;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace StudioKit.Scaffolding.Service.Controllers
{
	[RoutePrefix("api/url")]
	public class UrlsController : ApiController
	{
		//GET: api/url/headers?url={ url }
		[HttpGet]
		[Route("headers")]
		public async Task<IHttpActionResult> GetHeadersAsync(string url, CancellationToken cancellationToken = default(CancellationToken))
		{
			if (string.IsNullOrWhiteSpace(url))
			{
				return BadRequest("Url link is required");
			}

			HttpResponseMessage response = null;
			var client = new RetryingHttpClient();
			try
			{
				response = await client.SendAsync(new HttpRequestMessage(HttpMethod.Head, new Uri(url)), cancellationToken);
			}
			catch (Exception)
			{
				return Ok(response);
			}
			return Ok(response.Headers.ToDictionary(h => h.Key, h => h.Value));
		}
	}
}