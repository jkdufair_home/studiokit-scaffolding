﻿using StudioKit.Scaffolding.DataAccess.Interfaces;
using System;
using System.Data.Entity;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace StudioKit.Scaffolding.Service.Controllers
{
	[AllowAnonymous]
	[RoutePrefix("api/identityProviders")]
	public class IdentityProvidersController : ApiController
	{
		private readonly IBaseDbContext _dbContext;

		public IdentityProvidersController(IBaseDbContext dbContext)
		{
			_dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
		}

		// GET: api/identityProviders
		[HttpGet]
		public async Task<IHttpActionResult> GetAsync(CancellationToken cancellationToken = default(CancellationToken))
		{
			var externalProviders = await _dbContext.IdentityProviders.ToListAsync(cancellationToken);
			return Ok(externalProviders);
		}
	}
}