﻿using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.Service.Models.ViewModels;
using StudioKit.Sharding;
using StudioKit.Sharding.Owin.Extensions;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace StudioKit.Scaffolding.Service.Controllers
{
	[RoutePrefix("api/uniTime")]
	public class UniTimeController : ApiController
	{
		private readonly IUniTimeService _uniTimeService;

		public UniTimeController(IUniTimeService uniTimeService)
		{
			_uniTimeService = uniTimeService ?? throw new ArgumentNullException(nameof(uniTimeService));
		}

		[HttpGet]
		[Route("instructorSchedule")]
		public async Task<IHttpActionResult> GetInstructorScheduleAsync(string userId, int externalTermId, CancellationToken cancellationToken = default(CancellationToken))
		{
			var shardKey = Request.GetOwinContext().GetShardKeyFromHost();
			if (!shardKey.Equals(ShardDomainConstants.PurdueShardKey))
				throw new NotSupportedException();
			var instructorSchedule = await _uniTimeService.GetInstructorScheduleAsync(userId, externalTermId, User, cancellationToken);
			instructorSchedule.ExternalGroups = instructorSchedule.ExternalGroups.Select(eg => new UniTimeGroupViewModel(eg));
			return Ok(instructorSchedule);
		}
	}
}