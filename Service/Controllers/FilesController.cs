﻿using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;

namespace StudioKit.Scaffolding.Service.Controllers
{
	[RoutePrefix("api/files")]
	public class FilesController : ApiController
	{
		[HttpGet]
		[Route("processResult")]
		public HttpResponseMessage ProcessResult(string data, string fileName)
		{
			if (string.IsNullOrEmpty(data))
				throw new HttpRequestException("data missing");

			var content = new StringContent(data);
			var response = new HttpResponseMessage(HttpStatusCode.OK)
			{
				Content = content,
			};

			var contentType = MimeMapping.GetMimeMapping("csv");
			response.Content.Headers.ContentType = new MediaTypeHeaderValue(contentType);
			response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
			{
				FileName = fileName
			};

			return response;
		}
	}
}