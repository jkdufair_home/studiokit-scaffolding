﻿using StudioKit.Scaffolding.Service.Models.ViewModels;
using StudioKit.Security;
using StudioKit.Security.Entity.Identity.Interfaces;
using StudioKit.Security.Entity.Identity.Models;
using System;
using System.Data.Entity;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace StudioKit.Scaffolding.Service.Controllers
{
	[RoutePrefix("api/appClients")]
	public class AppClientsController : ApiController
	{
		private readonly IIdentityClientDbContext<AppClient, IdentityServiceClient, ServiceClientRole, RefreshToken> _dbContext;

		public AppClientsController(IIdentityClientDbContext<AppClient, IdentityServiceClient, ServiceClientRole, RefreshToken> dbContext)
		{
			_dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
		}

		// GET: api/appClients/:id
		[AllowAnonymous]
		[Route("{clientId}")]
		public async Task<IHttpActionResult> GetAsync(string clientId, CancellationToken cancellationToken = default(CancellationToken))
		{
			var client = await _dbContext.AppClients.SingleOrDefaultAsync(c => c.ClientId.Equals(clientId), cancellationToken);
			return Ok(new AppClientViewModel(client));
		}
	}
}