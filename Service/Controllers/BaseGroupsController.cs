using Microsoft.AspNet.Identity.EntityFramework;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Service.Models.RequestModels;
using StudioKit.Scaffolding.Service.Models.ViewModels;
using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace StudioKit.Scaffolding.Service.Controllers
{
	public class
		BaseGroupsController<TGroup, TGroupBusinessModel, TGroupEditBusinessModel, TGroupEditRequestModel, TGroupViewModel, TUser> : ApiController
		where TGroup : BaseGroup, new()
		where TGroupBusinessModel : BaseGroupBusinessModel<TGroup, TUser>, new()
		where TGroupEditBusinessModel : BaseGroupEditBusinessModel<TGroup>, new()
		where TGroupEditRequestModel : BaseGroupEditRequestModel
		where TGroupViewModel : BaseGroupViewModel<TGroup, TGroupBusinessModel, TUser>, new()
		where TUser : IdentityUser<string, UserLogin, UserRole, UserClaim>, IUser

	{
		private readonly IBaseGroupService<TGroup, TGroupBusinessModel, TGroupEditBusinessModel, TUser> _baseGroupService;

		public BaseGroupsController(IBaseGroupService<TGroup, TGroupBusinessModel, TGroupEditBusinessModel, TUser> baseGroupService)
		{
			_baseGroupService = baseGroupService;
		}

		#region Group

		[HttpGet]
		[Route("")]
		public async Task<IHttpActionResult> GetAsync(string keywords = null, DateTime? date = null, bool queryAll = false, string userId = null, bool includeIfSoftDeleted = false, CancellationToken cancellationToken = default(CancellationToken))
		{
			var groups = await _baseGroupService.GetAsync(User, keywords, date, queryAll, userId, includeIfSoftDeleted, cancellationToken);
			return Ok(groups.Select(CreateViewModel));
		}

		[HttpGet]
		[Route("{groupId}")]
		public async Task<IHttpActionResult> GetAsync(int groupId, CancellationToken cancellationToken = default(CancellationToken))
		{
			var group = await _baseGroupService.GetAsync(groupId, User, cancellationToken);
			return Ok(CreateViewModel(group));
		}

		[HttpPost]
		[Route("")]
		public async Task<IHttpActionResult> CreateOrCopyAsync([FromBody] TGroupEditRequestModel groupEditRequestModel, [FromUri]int? sourceId = null, CancellationToken cancellationToken = default(CancellationToken))
		{
			var groupEditBusinessModel = new TGroupEditBusinessModel
			{
				Name = groupEditRequestModel.Name,
				ExternalTermId = groupEditRequestModel.ExternalTermId,
				StartDate = groupEditRequestModel.StartDate,
				EndDate = groupEditRequestModel.EndDate,
				ExternalGroups = groupEditRequestModel.ExternalGroups
			};
			var groupBusinessModel = sourceId == null
				? await _baseGroupService.CreateAsync(groupEditBusinessModel, User, cancellationToken)
				: await _baseGroupService.CopyAsync(groupEditBusinessModel, sourceId.Value, User, cancellationToken);

			return Ok(CreateViewModel(groupBusinessModel));
		}

		[HttpPut]
		[Route("{groupId}")]
		public async Task<IHttpActionResult> UpdateAsync(int groupId, [FromBody] TGroupEditRequestModel groupEditRequestModel, CancellationToken cancellationToken = default(CancellationToken))
		{
			var groupEditBusinessModel = new TGroupEditBusinessModel
			{
				Name = groupEditRequestModel.Name,
				ExternalTermId = groupEditRequestModel.ExternalTermId,
				StartDate = groupEditRequestModel.StartDate,
				EndDate = groupEditRequestModel.EndDate,
				ExternalGroups = groupEditRequestModel.ExternalGroups,
				IsDeleted = groupEditRequestModel.IsDeleted
			};
			var groupBusinessModel = await _baseGroupService.UpdateAsync(groupId, groupEditBusinessModel, User, cancellationToken);
			return Ok(CreateViewModel(groupBusinessModel));
		}

		[HttpDelete]
		[Route("{groupId}")]
		public async Task<IHttpActionResult> DeleteAsync(int groupId, CancellationToken cancellationToken = default(CancellationToken))
		{
			await _baseGroupService.DeleteAsync(groupId, User, cancellationToken);
			return StatusCode(HttpStatusCode.NoContent);
		}

		#endregion Group

		#region Roster Sync

		[HttpGet]
		[Route("{groupId}/externalGroups")]
		public async Task<IHttpActionResult> GetExternalGroupsAsync(int groupId, CancellationToken cancellationToken = default(CancellationToken))
		{
			var externalGroups = await _baseGroupService.GetExternalGroupsAsync(groupId, User, cancellationToken);
			return Ok(externalGroups.Select(eg => new ExternalGroupViewModel(eg)));
		}

		[HttpPost]
		[Route("{groupId}/syncRoster")]
		public async Task<IHttpActionResult> SyncRosterAsync(int groupId, CancellationToken cancellationToken = default(CancellationToken))
		{
			await _baseGroupService.SyncRosterAsync(groupId, User, cancellationToken);
			return Ok();
		}

		[HttpGet]
		[Route("syncAll")]
		public async Task<IHttpActionResult> SyncAllAsync(CancellationToken cancellationToken = default(CancellationToken))
		{
			await _baseGroupService.SyncAllRostersAsync(User, cancellationToken);
			return Ok();
		}

		#endregion Roster Sync

		#region Helpers

		private static IViewModel<TGroupBusinessModel> CreateViewModel(TGroupBusinessModel businessModel)
		{
			var viewModel = new TGroupViewModel().MapFrom(businessModel);
			return viewModel;
		}

		#endregion Helpers
	}
}