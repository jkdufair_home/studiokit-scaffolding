﻿using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace StudioKit.Scaffolding.Service.Controllers
{
	public class JsonWebKeyController : ApiController
	{
		private readonly IJsonWebKeyService _jsonWebKeyService;

		public JsonWebKeyController(IJsonWebKeyService jsonWebKeyService)
		{
			_jsonWebKeyService = jsonWebKeyService ?? throw new ArgumentNullException(nameof(jsonWebKeyService));
		}

		[AllowAnonymous]
		[Route("api/jwks")]
		public async Task<IHttpActionResult> GetAsync(CancellationToken cancellationToken = default(CancellationToken))
		{
			var keySet = await _jsonWebKeyService.GetJsonWebKeySetAsync(cancellationToken);
			return Ok(keySet);
		}
	}
}