﻿using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.Service.Models.ViewModels;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace StudioKit.Scaffolding.Service.Controllers
{
	[RoutePrefix("api/ltiLaunches")]
	public class LtiLaunchesController : ApiController
	{
		private readonly ILtiLaunchService _ltiLaunchService;

		public LtiLaunchesController(ILtiLaunchService ltiLaunchService)
		{
			_ltiLaunchService = ltiLaunchService;
		}

		[HttpGet]
		[Route("{ltiLaunchId}")]
		public async Task<IHttpActionResult> GetAsync(int ltiLaunchId, CancellationToken cancellationToken = default(CancellationToken))
		{
			var ltiLaunch = await _ltiLaunchService.GetLtiLaunchAsync(ltiLaunchId, User, cancellationToken);
			return Ok(new LtiLaunchViewModel(ltiLaunch));
		}
	}
}