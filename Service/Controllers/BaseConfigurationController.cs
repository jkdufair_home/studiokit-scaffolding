﻿using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Service.Models.ViewModels;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace StudioKit.Scaffolding.Service.Controllers
{
	public class BaseConfigurationController<TConfiguration, TConfigurationViewModel> : ApiController
		where TConfiguration : BaseConfiguration, new()
		where TConfigurationViewModel : BaseConfigurationViewModel<TConfiguration>, new()
	{
		private readonly IBaseConfigurationService<TConfiguration> _configurationService;

		public BaseConfigurationController(IBaseConfigurationService<TConfiguration> configurationService)
		{
			_configurationService = configurationService ?? throw new ArgumentNullException(nameof(configurationService));
		}

		[AllowAnonymous]
		[HttpGet]
		public async Task<IHttpActionResult> GetAsync(CancellationToken cancellationToken = default(CancellationToken))
		{
			var configuration = await _configurationService.GetConfigurationAsync(cancellationToken);
			var viewModel = new TConfigurationViewModel().MapFrom(configuration);
			return Ok(viewModel);
		}
	}
}