﻿using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.Models.Interfaces;
using StudioKit.Scaffolding.Service.Models.ViewModels;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace StudioKit.Scaffolding.Service.Controllers
{
	public class BaseUsersController<TUser, TUserBusinessModel, TUserEditBusinessModel, TUserViewModel> : ApiController
		where TUser : IBaseUser, new()
		where TUserBusinessModel : BaseUserBusinessModel<TUser>, new()
		where TUserEditBusinessModel : BaseUserEditBusinessModel<TUser>, new()
		where TUserViewModel : BaseUserViewModel<TUser, TUserBusinessModel>, new()
	{
		private readonly IBaseUserService<TUser, TUserBusinessModel, TUserEditBusinessModel> _baseUserService;

		public BaseUsersController(IBaseUserService<TUser, TUserBusinessModel, TUserEditBusinessModel> baseUserService)
		{
			_baseUserService = baseUserService ?? throw new ArgumentNullException(nameof(baseUserService));
		}

		[HttpGet]
		[Route("")]
		public async Task<IHttpActionResult> GetAsync(string keywords, CancellationToken cancellationToken = default(CancellationToken))
		{
			var users = await _baseUserService.GetAsync(User, keywords, cancellationToken);
			return Ok(users.Select(u =>
			{
				var viewModel = new TUserViewModel().MapFrom(u);
				return viewModel;
			}));
		}

		[HttpGet]
		[Route("{userId}")]
		public virtual async Task<IHttpActionResult> GetUserByIdAsync(string userId, CancellationToken cancellationToken = default(CancellationToken))
		{
			var user = await _baseUserService.GetAsync(userId, User, cancellationToken);
			var viewModel = new TUserViewModel().MapFrom(user);
			return Ok(viewModel);
		}
	}
}