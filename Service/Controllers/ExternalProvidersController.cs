﻿using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.Service.Models.ViewModels;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace StudioKit.Scaffolding.Service.Controllers
{
	[RoutePrefix("api/externalProviders")]
	public class ExternalProvidersController : ApiController
	{
		private readonly IExternalProviderService _externalProviderService;

		public ExternalProvidersController(IExternalProviderService externalProviderService)
		{
			_externalProviderService = externalProviderService ?? throw new ArgumentNullException(nameof(externalProviderService));
		}

		[HttpGet]
		public async Task<IHttpActionResult> GetAsync(CancellationToken cancellationToken = default(CancellationToken))
		{
			var externalProviders = await _externalProviderService.GetAllAsync(User, cancellationToken);
			return Ok(externalProviders.Select(ep => new ExternalProviderViewModel(ep)));
		}
	}
}