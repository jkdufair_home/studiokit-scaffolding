﻿using StudioKit.Scaffolding.DataAccess.Interfaces;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace StudioKit.Scaffolding.Service.Controllers
{
	[RoutePrefix("api/externalTerms")]
	public class ExternalTermsController : ApiController
	{
		private readonly IBaseDbContext _dbContext;

		public ExternalTermsController(IBaseDbContext dbContext)
		{
			_dbContext = dbContext;
		}

		// GET: api/externalTerms
		[AllowAnonymous]
		public async Task<IHttpActionResult> GetAsync(CancellationToken cancellationToken = default(CancellationToken))
		{
			var result = await _dbContext.ExternalTerms.Select(x => new { x.Id, x.ExternalProviderId, x.Name, x.ExternalId, x.StartDate, x.EndDate }).ToListAsync(cancellationToken);
			return Ok(result);
		}
	}
}