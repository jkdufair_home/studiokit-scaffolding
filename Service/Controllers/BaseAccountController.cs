﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using StudioKit.Configuration;
using StudioKit.Data.Entity.Identity;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.Diagnostics;
using StudioKit.Encryption;
using StudioKit.ErrorHandling.Interfaces;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.BusinessLogic.Services;
using StudioKit.Scaffolding.Common;
using StudioKit.Scaffolding.DataAccess;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Models.Interfaces;
using StudioKit.Scaffolding.Service.Models.RequestModels;
using StudioKit.Scaffolding.Service.Models.ViewModels;
using StudioKit.Scaffolding.Service.Results;
using StudioKit.Sharding;
using StudioKit.Sharding.Owin.Extensions;
using StudioKit.Shibboleth;
using StudioKit.Web.Http;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Security.Authentication;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace StudioKit.Scaffolding.Service.Controllers
{
	public class BaseAccountController<TContext, TUser, TGroup, TConfiguration, TUserBusinessModel, TUserEditBusinessModel, TUserViewModel> : ApiController
		where TContext : BaseDbContext<TUser, TGroup, TConfiguration>
		where TUser : IdentityUser<string, UserLogin, UserRole, UserClaim>, IBaseUser, new()
		where TGroup : BaseGroup, new()
		where TConfiguration : BaseConfiguration, new()
		where TUserBusinessModel : BaseUserBusinessModel<TUser>, new()
		where TUserEditBusinessModel : BaseUserEditBusinessModel<TUser>, new()
		where TUserViewModel : BaseUserViewModel<TUser, TUserBusinessModel>, new()
	{
		private readonly TContext _dbContext;
		private readonly IUserManager<TUser> _userManager;
		private readonly IBaseUserService<TUser, TUserBusinessModel, TUserEditBusinessModel> _userService;
		private readonly PurdueCasService _purdueCasService;
		private readonly ILtiService _ltiService;
		private readonly ILogger _logger;
		private readonly IErrorHandler _errorHandler;

		public BaseAccountController(
			TContext dbContext,
			IUserManager<TUser> userManager,
			IBaseUserService<TUser, TUserBusinessModel, TUserEditBusinessModel> userService,
			PurdueCasService purdueCasService,
			ILtiService ltiService,
			ILogger logger,
			IErrorHandler errorHandler)
		{
			_dbContext = dbContext;
			_userManager = userManager;
			_userService = userService;
			_purdueCasService = purdueCasService;
			_ltiService = ltiService;
			_logger = logger;
			_errorHandler = errorHandler;
		}

		// GET api/account/userInfo
		[HttpGet]
		public virtual async Task<IHttpActionResult> GetUserInfoAsync(CancellationToken cancellationToken)
		{
			var userId = User.Identity.GetUserId();
			var user = await _userService.GetAsync(userId, User, cancellationToken);
			var viewModel = new TUserViewModel();
			viewModel.MapFrom(user);
			return Ok(viewModel);
		}

		#region CAS

		[AllowAnonymous]
		[Route("casV1")]
		[HttpPost]
		public async Task<IHttpActionResult> CasV1Async(LoginRequestModel model, CancellationToken cancellationToken)
		{
			if (!ModelState.IsValid)
				return BadRequest(ModelState);
			var externalLoginInfo = await _purdueCasService.ValidateV1CredentialsAsync(model, cancellationToken);
			var user = GetUser(externalLoginInfo);
			return await FinishSignInAsync(user, externalLoginInfo.ExternalIdentity.Claims.ToList(), externalLoginInfo.Login, cancellationToken: cancellationToken);
		}

		[AllowAnonymous]
		[Route("validateCasTicket")]
		[HttpGet]
		public async Task<IHttpActionResult> ValidateCasTicketAsync(string ticket, CancellationToken cancellationToken, string service = null)
		{
			var externalLoginInfo = await _purdueCasService.ValidateTicketAsync(
				ticket,
				service ?? Request.RequestUri.GetLeftPart(UriPartial.Authority) + "/", cancellationToken);
			var user = GetUser(externalLoginInfo);
			return await FinishSignInAsync(user, externalLoginInfo.ExternalIdentity.Claims.ToList(), externalLoginInfo.Login, cancellationToken: cancellationToken);
		}

		#endregion CAS

		#region Local

		/// <summary>
		/// Note: GetHashCode is not guaranteed to be identical across .NET versions and platforms
		/// this is sufficient for generating Puids that correspond to usernames for testing purposes
		/// </summary>
		/// <param name="username"></param>
		/// <returns></returns>
		private static string HashedUsernameAsPuid(string username)
		{
			return username == null ? "0000000000" : ((uint)username.GetHashCode()).ToString("0000000000");
		}

		// POST api/account/local
		[AllowAnonymous]
		[Route("local")]
		[HttpPost]
		public async Task<IHttpActionResult> LocalAsync(LocalLoginRequestModel model, CancellationToken cancellationToken)
		{
			var config = EncryptedConfigurationManager.GetSetting(BaseAppSetting.RunConfiguration);
			if (config != RunConfiguration.Debug && config != RunConfiguration.LoadTest)
				return NotFound();

			if (!ModelState.IsValid)
				return BadRequest(ModelState);

			var email = $"{model.Username}@localhost";
			var user = new TUser
			{
				UserName = model.Username,
				Uid = model.Username,
				Email = email
			};

			var claims = new List<Claim>();
			if (model.IsInstructor)
				claims.Add(new Claim(ClaimTypes.Role, BaseRole.Creator));

			return await FinishSignInAsync(user, claims, null, cancellationToken: cancellationToken);
		}

		#endregion Local

		#region External

		[HttpGet]
		[AllowAnonymous]
		[Route("externalLogin")]
		public IHttpActionResult ExternalLogin(string provider, string returnUrl, string idp = null, string shardKey = null, bool debug = false)
		{
			var redirectBaseUrl = "";
			if (provider == LoginProviderConstants.Shibboleth)
			{
				var publicOrigin = EncryptedConfigurationManager.GetSetting(ShibbolethAppSetting.ShibbolethSpPublicOrigin);
				if (!string.IsNullOrWhiteSpace(publicOrigin))
					redirectBaseUrl = publicOrigin;
			}

			var encodedReturnUrl = HttpUtility.UrlEncode(returnUrl);
			shardKey = shardKey ?? Request.GetOwinContext().GetShardKeyFromHost();
			return new ChallengeResult(
				provider,
				Request,
				$"{redirectBaseUrl}/api/Account/ExternalLoginCallback?returnUrl={encodedReturnUrl}&shardKey={shardKey}{(debug ? "&debug=true" : "")}",
				idp);
		}

		[HttpGet]
		[AllowAnonymous]
		[Route("externalLoginCallback")]
		public async Task<IHttpActionResult> ExternalLoginCallbackAsync(string returnUrl, string shardKey, CancellationToken cancellationToken, string error = null, bool debug = false)
		{
			if (!string.IsNullOrWhiteSpace(error))
				throw new AuthenticationException(error);

			var externalLoginInfo = await Authentication.GetExternalLoginInfoAsync();
			var claims = externalLoginInfo?.ExternalIdentity?.Claims.ToList();

			if (debug)
			{
				Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);
				return Ok(new
				{
					info = new
					{
						externalLoginInfo?.Email,
						externalLoginInfo?.DefaultUserName,
						externalLoginInfo?.Login
					},
					claims = claims?.Select(c => new
					{
						c.Type,
						c.Value,
						c.Issuer,
						c.OriginalIssuer,
						c.Properties,
						c.ValueType
					})
				});
			}

			if (externalLoginInfo == null)
				return BadRequest("External identity login info was not found.");
			if (string.IsNullOrWhiteSpace(externalLoginInfo.Email))
				return BadRequest($"External identity login info does not contain an claim of type {ClaimTypes.Email}");
			if (claims == null)
				return BadRequest("External identity login info does not contain claims.");

			externalLoginInfo.Email = externalLoginInfo.Email.ToLower().Trim();

			// Domain Validation
			if (await _userManager.FindByNameAsync(externalLoginInfo.Email) == null)
			{
				var configuration = await _dbContext.GetConfigurationAsync(cancellationToken);
				var allowed = configuration.ValidateAllowedDomains(externalLoginInfo.Email);
				if (!allowed)
					return new ForbiddenResult(Request, "You do not have access. Your email address was not accepted.");
			}

			if (new[] { ShardDomainConstants.DemoShardKey }.Contains(shardKey))
				claims.Add(new Claim(ClaimTypes.Role, BaseRole.Creator));

			var user = GetUser(externalLoginInfo);

			return await FinishSignInAsync(user, claims, externalLoginInfo.Login, shardKey, returnUrl, cancellationToken);
		}

		// POST api/account/registerExternal
		[AllowAnonymous]
		[Route("registerExternal")]
		public async Task<IHttpActionResult> RegisterExternalAsync(RegisterExternalRequestModel model, CancellationToken cancellationToken)
		{
			if (!ModelState.IsValid)
				return BadRequest(ModelState);
			var verifiedAccessToken = await VerifyExternalAccessTokenAsync(model.Provider, model.AccessToken, cancellationToken);
			if (verifiedAccessToken == null)
				return InternalServerError(new Exception("This provider is not supported."));
			if (!verifiedAccessToken.IsValid)
				return InternalServerError(new Exception("Access token has expired."));

			if (await _userManager.FindByNameAsync(verifiedAccessToken.Email) == null)
			{
				var configuration = await _dbContext.GetConfigurationAsync(cancellationToken);
				var allowed = configuration.ValidateAllowedDomains(verifiedAccessToken.Email);
				if (!allowed)
					return new ForbiddenResult(Request, "You do not have access. Your email address was not accepted.");
			}

			var user = new TUser
			{
				Email = verifiedAccessToken.Email,
				FirstName = verifiedAccessToken.FirstName,
				LastName = verifiedAccessToken.LastName,
				UserName = verifiedAccessToken.Email
			};

			return await FinishSignInAsync(user, verifiedAccessToken.Claims, verifiedAccessToken.Login, cancellationToken: cancellationToken);
		}

		#endregion External

		#region Lti

		[AllowAnonymous]
		[HttpPost]
		[Route("~/api/lti/1p3/launch")]
		public async Task<IHttpActionResult> Lti1P3LaunchAsync(FormDataCollection formData, CancellationToken cancellationToken)
		{
			var responseUrl = Request.GetOwinContext().GetWebUrlFromRequest();
			string ltiReturnUrl = null;
			var queryParams = new NameValueCollection();

			try
			{
				var token = formData.Get("id_token");
				ltiReturnUrl = _ltiService.GetLaunchReturnUrl(token);

				var externalLoginInfo = await _ltiService.GetLoginInfoAsync(token, cancellationToken);
				var user = GetUser(externalLoginInfo);
				var claims = externalLoginInfo.ExternalIdentity.Claims.ToList();
				var code = await _userManager.FinishSignInAsync(user,
					claims,
					externalLoginInfo.Login,
					Request.GetOwinContext().GetShardKeyFromHost(), cancellationToken);
				var launchResponse = await _ltiService.GetLaunchResponseAsync(token, user.Id, claims, cancellationToken);

				queryParams["code"] = code;
				if (launchResponse.LtiLaunchId.HasValue)
					queryParams["ltiLaunchId"] = launchResponse.LtiLaunchId.Value.ToString();
				else if (launchResponse.GroupId.HasValue)
					queryParams["groupId"] = launchResponse.GroupId.Value.ToString();
			}
			catch (Exception e)
			{
				var config = EncryptedConfigurationManager.GetSetting(BaseAppSetting.RunConfiguration);

				// if provided, use the lti return_url
				if (ltiReturnUrl != null)
					responseUrl = ltiReturnUrl;

				// obfuscate the error details when not debugging
				queryParams["_ltierrormsg"] = config != RunConfiguration.Debug
					? "An error was encountering while processing your request."
					: e.Message;

				_logger.Error(e.Message);
				_errorHandler.CaptureException(e);
			}

			var response = Request.CreateResponse(HttpStatusCode.Redirect);
			var queryString = string.Join("&", queryParams.AllKeys.Select(k => k + "=" + HttpUtility.UrlEncode(queryParams[k])));
			response.Headers.Location = new Uri($"{responseUrl}{(queryParams.Count > 0 ? $"?{queryString}" : "")}");
			return ResponseMessage(response);
		}

		#endregion Lti

		#region Helpers

		private IAuthenticationManager Authentication => Request.GetOwinContext().Authentication;

		private static TUser GetUser(ExternalLoginInfo externalLoginInfo)
		{
			// Note: Use email as both username and email
			return new TUser
			{
				Email = externalLoginInfo.Email,
				UserName = externalLoginInfo.Email
			};
		}

		private async Task<IHttpActionResult> FinishSignInAsync(TUser user, List<Claim> claims, UserLoginInfo userLoginInfo, string shardKey = null, string returnUrl = null, CancellationToken cancellationToken = default(CancellationToken))
		{
			var code = await _userManager.FinishSignInAsync(user,
				claims,
				userLoginInfo,
				shardKey ?? Request.GetOwinContext().GetShardKeyFromHost(),
				cancellationToken);

			// Clear External SignIn Cookie (if any)
			Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);

			// Return a JSON object if no returnUrl is set
			if (string.IsNullOrWhiteSpace(returnUrl))
				return Ok(new { Code = code });

			// Add "code" as a query parameter for returnUrl redirect
			var response = Request.CreateResponse(HttpStatusCode.Redirect);
			response.Headers.Location = new Uri($"{returnUrl}?code={HttpUtility.UrlEncode(code)}");
			return ResponseMessage(response);
		}

		private static async Task<ParsedExternalAccessToken> VerifyExternalAccessTokenAsync(string provider, string accessToken, CancellationToken cancellationToken = default(CancellationToken))
		{
			string verifyTokenEndPoint;
			string profileEndPoint;

			switch (provider)
			{
				case LoginProviderConstants.Facebook:
					{
						var appId = EncryptedConfigurationManager.GetSetting(ScaffoldingAppSetting.FacebookAppId);
						var appSecret = EncryptedConfigurationManager.GetSetting(ScaffoldingAppSetting.FacebookAppSecret);
						verifyTokenEndPoint =
							$"https://graph.facebook.com/debug_token?input_token={accessToken}&access_token={appId}|{appSecret}";
						break;
					}
				case LoginProviderConstants.Google:
					verifyTokenEndPoint = $"https://www.googleapis.com/oauth2/v1/tokeninfo?access_token={accessToken}";
					break;

				default:
					return null;
			}

			var client = new HttpClient();
			var uri = new Uri(verifyTokenEndPoint);
			var response = await client.GetAsync(uri, cancellationToken);

			if (!response.IsSuccessStatusCode)
				throw new Exception(await response.Content.ReadAsStringAsync());

			var json = await response.Content.ReadAsStringAsync();
			var jsonObject = JsonConvert.DeserializeObject<JObject>(json);

			var parsedToken = new ParsedExternalAccessToken
			{
				LoginProvider = provider,
				AccessToken = accessToken
			};

			switch (provider)
			{
				case LoginProviderConstants.Facebook:
					var data = jsonObject.Value<JObject>("data");
					var error = data.Value<JObject>("error");

					if (error != null)
						throw new Exception(error.Value<string>("message"));

					parsedToken.AppId = data.Value<string>("app_id");
					parsedToken.ProviderKey = data.Value<string>("user_id");
					parsedToken.IsValid = data.Value<bool>("is_valid");

					if (parsedToken.AppId != EncryptedConfigurationManager.GetSetting(ScaffoldingAppSetting.FacebookAppId))
						return null;

					profileEndPoint = $"https://graph.facebook.com/me?access_token={accessToken}";
					uri = new Uri(profileEndPoint);
					response = await client.GetAsync(uri, cancellationToken);
					json = await response.Content.ReadAsStringAsync();
					jsonObject = JsonConvert.DeserializeObject<JObject>(json);

					parsedToken.Email = jsonObject.Value<string>("email");
					parsedToken.FirstName = jsonObject.Value<string>("first_name");
					parsedToken.LastName = jsonObject.Value<string>("last_name");

					break;

				case LoginProviderConstants.Google:
					parsedToken.AppId = jsonObject.Value<string>("audience");
					parsedToken.ProviderKey = jsonObject.Value<string>("user_id");
					parsedToken.IsValid = jsonObject.Value<int>("expires_in") > 0;

					// Accept Web, iOS and Android Tokens
					if (parsedToken.AppId != EncryptedConfigurationManager.GetSetting(ScaffoldingAppSetting.GoogleClientId) &&
							parsedToken.AppId != EncryptedConfigurationManager.GetSetting(ScaffoldingAppSetting.GoogleClientIdiOS) &&
							parsedToken.AppId != EncryptedConfigurationManager.GetSetting(ScaffoldingAppSetting.GoogleClientIdAndroidDebug) &&
							parsedToken.AppId != EncryptedConfigurationManager.GetSetting(ScaffoldingAppSetting.GoogleClientIdAndroid))
						return null;

					parsedToken.Email = jsonObject.Value<string>("email");

					profileEndPoint = $"https://www.googleapis.com/plus/v1/people/me?fields=name&access_token={accessToken}";
					uri = new Uri(profileEndPoint);
					response = await client.GetAsync(uri, cancellationToken);
					json = await response.Content.ReadAsStringAsync();
					jsonObject = JsonConvert.DeserializeObject<JObject>(json);

					var name = jsonObject.Value<JObject>("name");
					parsedToken.FirstName = name.Value<string>("givenName");
					parsedToken.LastName = name.Value<string>("familyName");

					break;
			}

			// Let's make sure we don't get any funky input
			parsedToken.Email = parsedToken.Email.ToLower().Trim();

			return parsedToken;
		}

		#endregion Helpers
	}
}