﻿using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace StudioKit.Scaffolding.Service.Controllers
{
	[RoutePrefix("api/cache")]
	public class CacheController : ApiController
	{
		private readonly ICacheService _cacheService;

		public CacheController(ICacheService cacheService)
		{
			_cacheService = cacheService ?? throw new ArgumentNullException(nameof(cacheService));
		}

		[HttpGet]
		[Route("statistics", Name = nameof(GetCacheStatisticsAsync))]
		public async Task<IHttpActionResult> GetCacheStatisticsAsync(CancellationToken cancellationToken = default(CancellationToken))
		{
			return Ok(await _cacheService.GetCacheStatisticsAsync(User, cancellationToken));
		}

		[HttpPost]
		[Route("purge", Name = nameof(PurgeAsync))]
		public async Task<IHttpActionResult> PurgeAsync(CancellationToken cancellationToken = default(CancellationToken))
		{
			await _cacheService.PurgeCacheAsync(User, cancellationToken);
			return Ok();
		}
	}
}