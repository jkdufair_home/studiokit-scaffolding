﻿using Microsoft.ApplicationInsights.Extensibility;
using StudioKit.Configuration;
using StudioKit.Diagnostics;
using StudioKit.Encryption;
using StudioKit.ErrorHandling;
using System;
using System.Web;

namespace StudioKit.Scaffolding.Service
{
	public class BaseHttpApplication : HttpApplication
	{
		protected virtual void Application_Start()
		{
			var instrumentationKey = EncryptedConfigurationManager.GetSetting(BaseAppSetting.AppInsightsInstrumentationKey);
			if (instrumentationKey == null)
			{
				TelemetryConfiguration.Active.DisableTelemetry = true;
			}
			else
			{
				TelemetryConfiguration.Active.InstrumentationKey = instrumentationKey;
			}

			var logger = new Logger("WebRoleLogger");
			logger.Info("Application started");
		}

		protected virtual void Application_Error(object sender, EventArgs e)
		{
			ErrorHandler.Instance?.CaptureException(Server.GetLastError(), Context.User?.Identity.ToExceptionUser());
		}
	}
}