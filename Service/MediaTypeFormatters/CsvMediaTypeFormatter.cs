﻿using CsvHelper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;

namespace StudioKit.Scaffolding.Service.MediaTypeFormatters
{
	public class CsvMediaTypeFormatter : BufferedMediaTypeFormatter
	{
		private const string MediaTypeHeader = "text/csv";
		private readonly CsvHelper.Configuration.Configuration _configuration;

		public CsvMediaTypeFormatter(CsvHelper.Configuration.Configuration configuration)
		{
			_configuration = configuration;
			SupportedMediaTypes.Add(new MediaTypeHeaderValue(MediaTypeHeader));
		}

		public CsvMediaTypeFormatter(MediaTypeMapping mediaTypeMapping, CsvHelper.Configuration.Configuration configuration) : this(configuration)
		{
			MediaTypeMappings.Add(mediaTypeMapping);
		}

		public override bool CanReadType(Type type) => false;

		public override bool CanWriteType(Type type)
		{
			return type != null && IsTypeOfIEnumerable(type);
		}

		public override void WriteToStream(Type type, object value, Stream stream, HttpContent content)
		{
			if (value == null)
				return;

			using (var streamWriter = new StreamWriter(stream))
			using (var csvWriter = new CsvWriter(streamWriter, _configuration))
			{
				csvWriter.WriteRecords(value as IEnumerable<object>);
			}
		}

		#region Private methods

		private static bool IsTypeOfIEnumerable(Type type)
		{
			return type.GetInterfaces().Any(interfaceType => interfaceType == typeof(IEnumerable));
		}

		#endregion Private methods
	}
}