using Autofac;
using Autofac.Core;
using Autofac.Integration.WebApi;
using JsonPatch;
using JsonPatch.Formatting;
using JsonPatch.Paths.Resolvers;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Security.Facebook;
using Microsoft.Owin.Security.Google;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Owin;
using StudioKit.Caliper;
using StudioKit.Caliper.Controllers.API;
using StudioKit.Caliper.Interfaces;
using StudioKit.Cloud.Storage.Blob;
using StudioKit.Cloud.Storage.Queue;
using StudioKit.Configuration;
using StudioKit.Data.Entity.Identity;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.Diagnostics;
using StudioKit.Encryption;
using StudioKit.ErrorHandling;
using StudioKit.ErrorHandling.Interfaces;
using StudioKit.ErrorHandling.WebApi;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Interfaces;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Services;
using StudioKit.ExternalProvider.UniTime.BusinessLogic.Services;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Interfaces.Authorization;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.BusinessLogic.Services;
using StudioKit.Scaffolding.BusinessLogic.Services.Authorization;
using StudioKit.Scaffolding.BusinessLogic.Services.OAuth;
using StudioKit.Scaffolding.BusinessLogic.Validators;
using StudioKit.Scaffolding.Common;
using StudioKit.Scaffolding.Common.Interfaces;
using StudioKit.Scaffolding.DataAccess;
using StudioKit.Scaffolding.DataAccess.Queues.QueueManagers;
using StudioKit.Scaffolding.DataAccess.Queues.QueueMessages;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Models.Interfaces;
using StudioKit.Scaffolding.Service.Filters;
using StudioKit.Scaffolding.Service.MediaTypeFormatters;
using StudioKit.Scaffolding.Service.Models.ViewModels;
using StudioKit.Security.Interfaces;
using StudioKit.Sharding.Entity.Identity.Models;
using StudioKit.Shibboleth;
using StudioKit.TransientFaultHandling.Http;
using StudioKit.TransientFaultHandling.Http.Interfaces;
using StudioKit.UserInfoService;
using StudioKit.UserInfoService.UserIdentityService;
using StudioKit.Web.Http.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Reflection;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Routing;

namespace StudioKit.Scaffolding.Service
{
	public abstract class BaseStartup<TContext, TUser, TGroup, TConfiguration>
		where TContext : BaseDbContext<TUser, TGroup, TConfiguration>
		where TUser : IdentityUser<string, UserLogin, UserRole, UserClaim>, IBaseUser, new()
		where TGroup : BaseGroup, new()
		where TConfiguration : BaseConfiguration, new()
	{
		public virtual void Configuration(IAppBuilder app)
		{
			var config = CreateWebApiConfig();
			var builder = CreateContainerBuilder();
			var container = builder.Build();

			// update WebApi Config
			config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
			// Add CSV formatter, use container to resolve injected dependencies
			config.Formatters.Add(new CsvMediaTypeFormatter(container.Resolve<CsvHelper.Configuration.Configuration>()));

			// Autofac middleware must come first
			app.UseAutofacMiddleware(container);
			ConfigureResponseCaching(app);
			ConfigureShard(app);
			ConfigureAppClientService(app);
			ConfigureCache(container);
			// Auth middleware must come before WebApi
			ConfigureAuth(app);
			app.UseAutofacWebApi(config);
			app.UseWebApi(config);
		}

		protected abstract void ConfigureCache(IContainer container);

		protected virtual HttpConfiguration CreateWebApiConfig()
		{
			var isDebug = EncryptedConfigurationManager.GetSetting(BaseAppSetting.RunConfiguration) == RunConfiguration.Debug;
			var config = new HttpConfiguration();

			// Customize Api Error Responses
			config.MessageHandlers.Add(new ErrorMessageHandler(config, isDebug));

			// Display JSON when apis are loaded in a browser, not XML
			config.Formatters.JsonFormatter.MediaTypeMappings
				.Add(new RequestHeaderMapping("Accept",
					"text/html",
					StringComparison.InvariantCultureIgnoreCase,
					true,
					"application/json"));

			// Enable camelCase JSON Formatting and UTC Dates
			var jsonFormatter = config.Formatters.JsonFormatter;
			jsonFormatter.SerializerSettings =
				new JsonSerializerSettings
				{
					ContractResolver = new CamelCasePropertyNamesContractResolver(),
					DateTimeZoneHandling = DateTimeZoneHandling.Utc
				};

			// Add Json Patch Formatting
			config.Formatters.Add(new JsonPatchFormatter(new JsonPatchSettings
			{
				PathResolver = new FlexiblePathResolver()
			}));

			// Handle EF self referencing loop
			config.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling =
				ReferenceLoopHandling.Ignore;

			// Web API configuration and services
			// Configure Web API to use only bearer token authentication.
			config.SuppressDefaultHostAuthentication();
			config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

			if (!isDebug)
				config.Filters.Add(new RequireHttpsFilterAttribute());

			// Add Root Domain Default Filter
			//config.Filters.Add(new RootDomainFilterAttribute());

			// Add Custom Exception Handling
			config.Filters.Add(new ExceptionApiErrorMessageFilterAttribute());

			// Add ModelState Error Handling
			config.Filters.Add(new HandleModelStateErrorsFilterAttribute());

			// Add Exception Handling
			config.Filters.Add(new ShardWebApiExceptionFilterAttribute());

			// Authorize all requests by default
			config.Filters.Add(new AuthorizeAttribute());

			// Authorize that a Principal/User exists using the identity from the OAuth token
			config.Filters.Add(new AuthorizePrincipalFilterAttribute());

			config.Filters.Add(new GzipFilterAttribute());

			// Web API routes
			config.MapHttpAttributeRoutes(new InheritActionsDirectRouteProvider());

			config.Routes.MapHttpRoute("DefaultApi", "api/{controller}/{id}", new { id = RouteParameter.Optional });

			return config;
		}

		private class InheritActionsDirectRouteProvider : DefaultDirectRouteProvider
		{
			protected override IReadOnlyList<IDirectRouteFactory> GetActionRouteFactories(HttpActionDescriptor actionDescriptor)
			{
				// inherit route attributes decorated on base class controller's actions
				return actionDescriptor.GetCustomAttributes<IDirectRouteFactory>(inherit: true);
			}
		}

		protected virtual ContainerBuilder CreateContainerBuilder()
		{
			var builder = new ContainerBuilder();

			// ApiControllers
			builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
			builder.RegisterType<CaliperEventStoreController>()
				.AsSelf();

			// Singletons
			builder.Register(c => new Logger("WebRoleLogger"))
				.As<ILogger>()
				.SingleInstance();
			ErrorHandler.Instance = new SentryErrorHandler(
				EncryptedConfigurationManager.GetSetting(BaseAppSetting.SentryDsn),
				new DefaultJsonPacketFactory());
			builder.RegisterInstance(ErrorHandler.Instance)
				.As<IErrorHandler>()
				.ExternallyOwned();

			builder.RegisterType<DateTimeProvider>()
				.As<IDateTimeProvider>()
				.SingleInstance();

			builder.RegisterType<HttpContextPrincipalProvider>()
				.As<IPrincipalProvider>()
				.SingleInstance();

			builder.RegisterType<HttpContextShardKeyProvider>()
				.As<IShardKeyProvider>()
				.SingleInstance();

			// CsvHelper Configuration
			builder.Register(c => new CsvHelper.Configuration.Configuration())
				.As<CsvHelper.Configuration.Configuration>()
				.SingleInstance();

			// Authorization Services

			builder.RegisterType<UserRoleAuthorizationService<TContext>>()
				.As<IUserRoleAuthorizationService>()
				.AsSelf()
				.InstancePerRequest();

			builder.RegisterType<CacheAuthorizationService<TContext>>()
				.As<ICacheAuthorizationService>()
				.InstancePerRequest();

			builder.RegisterType<GroupAuthorizationService<TContext>>()
				.As<IGroupAuthorizationService>()
				.Named<IEntityUserRoleAuthorizationService>("groupAuthorizationService")
				.InstancePerRequest();

			builder.RegisterType<ExternalGroupAuthorizationService<TContext>>()
				.As<IExternalGroupAuthorizationService>()
				.InstancePerRequest();

			builder.RegisterType<ExternalProviderAuthorizationService<TContext>>()
				.As<IExternalProviderAuthorizationService>()
				.InstancePerRequest();

			builder.RegisterType<LtiLaunchAuthorizationService<TContext>>()
				.As<ILtiLaunchAuthorizationService>()
				.InstancePerRequest();

			// BusinessLogic Services

			builder.RegisterType<CacheService>()
				.As<ICacheService>()
				.InstancePerRequest();

			builder.RegisterType<UniTimeService<TContext, TUser, TGroup, TConfiguration>>()
				.As<IUniTimeService>()
				.InstancePerRequest();

			builder.RegisterType<UniTimeRosterProviderService<TUser, ExternalGroup, ExternalGroupUser>>()
				.AsSelf()
				.InstancePerRequest();

			builder.RegisterType<RoleStore>()
				.AsSelf()
				.InstancePerRequest();

			builder.RegisterType<RoleManager>()
				.AsSelf()
				.InstancePerRequest();

			builder.RegisterType<BusinessLogic.Services.UserStore<TUser>>()
				.AsSelf()
				.As<IUserStore<TUser>>()
				.InstancePerRequest();

			builder.Register(c => new IdentityFactoryOptions<UserManager<TContext, TUser, TGroup, TConfiguration>>
			{
				DataProtectionProvider = new Microsoft.Owin.Security.DataProtection.DpapiDataProtectionProvider("ApplicationName")
			});

			builder.RegisterType<ServiceClientManager>()
				.AsSelf();

			builder.RegisterType<OrganizationService<TContext, TConfiguration, License>>()
				.As<IOrganizationService>();

			builder.RegisterType<PurdueCasService>()
				.AsSelf();

			builder.RegisterType<ExternalProviderService<TContext, TUser, TGroup, TConfiguration>>()
				.As<IExternalProviderService>()
				.InstancePerRequest();

			builder
				.RegisterType<BaseGroupService<TContext, TUser, TGroup, TConfiguration, BaseGroupBusinessModel<TGroup, TUser>,
					BaseGroupEditBusinessModel<TGroup>>>()
				.As<IBaseGroupService<TGroup, BaseGroupBusinessModel<TGroup, TUser>, BaseGroupEditBusinessModel<TGroup>, TUser>>()
				.InstancePerRequest();

			builder.RegisterType<BaseGroupUserRoleService<TContext, TUser, TGroup, TConfiguration, GroupUserRoleBusinessModel<TUser>, CreateEntityUserRolesBusinessModel<TUser>>>()
				.WithParameter(ResolvedParameter.ForNamed<IEntityUserRoleAuthorizationService>("groupAuthorizationService"))
				.AsSelf()
				.InstancePerRequest();

			// BaseEntityUserRolesController dependencies

			// let BaseEntityUserRolesController use the correct EntityUserRoleService for the given typename
			builder.Register<Func<Dictionary<string, IEntityUserRoleService<GroupUserRoleBusinessModel<TUser>, CreateEntityUserRolesBusinessModel<TUser>>>>>(ctx =>
				{
					var context = ctx.Resolve<IComponentContext>();
					return () => new Dictionary<string, IEntityUserRoleService<GroupUserRoleBusinessModel<TUser>, CreateEntityUserRolesBusinessModel<TUser>>>
					{
						{typeof(TGroup).Name, context.Resolve<BaseGroupUserRoleService<TContext, TUser, TGroup, TConfiguration, GroupUserRoleBusinessModel<TUser>, CreateEntityUserRolesBusinessModel<TUser>>>()}
					};
				})
				.InstancePerRequest();

			// let BaseEntityUserRolesController return different viewModels based on typename
			builder.Register<Func<string, Func<EntityUserRoleBusinessModel<TUser>, IViewModel<EntityUserRoleBusinessModel<TUser>>>>>(
					ctx =>
					{
						return typename =>
						{
							if (typename == typeof(TGroup).Name)
								return businessModel =>
									new BaseGroupUserRoleViewModel<TUser>()
										.MapFrom(businessModel);
							return businessModel =>
								new BaseEntityUserRoleViewModel<TUser>()
									.MapFrom(businessModel);
						};
					})
				.InstancePerRequest();

			// let BaseEntityUserRolesController return different create viewModels based on typename
			builder.Register<Func<string, Func<CreateEntityUserRolesBusinessModel<TUser>, IViewModel<CreateEntityUserRolesBusinessModel<TUser>>>>>(
					ctx =>
					{
						// No need for other models at the moment, but the framework is in place if needed
						return typename => businessModel => new BaseCreateEntityUserRolesViewModel<TUser>().MapFrom(businessModel);
					})
				.InstancePerRequest();

			// BusinessLogic.Validators

			builder.RegisterType<BaseGroupBusinessModelValidator<TGroup>>()
				.AsSelf()
				.InstancePerRequest();

			builder.RegisterType<ArtifactBusinessModelValidator>()
				.AsSelf()
				.InstancePerRequest();

			// Storage + Artifacts

			builder.RegisterType<AzureBlobStorage>()
				.As<IBlobStorage>()
				.WithParameter("containerName", "artifact")
				.InstancePerRequest();

			builder.RegisterType<ArtifactLeasingService>()
				.AsSelf()
				.WithParameters(new List<Parameter>
				{
					new NamedParameter("totpSecretKey", EncryptedConfigurationManager.GetSetting(ScaffoldingAppSetting.ArtifactTotpSecretKey)),
					new NamedParameter("totpDurationSeconds", EncryptedConfigurationManager.GetSetting(ScaffoldingAppSetting.ArtifactTotpDurationSeconds))
				})
				.InstancePerRequest();

			// Caliper
			builder.RegisterType<CaliperEventStoreService>()
				.AsSelf()
				.InstancePerRequest();

			builder.RegisterType<BaseConfigurationService<TContext, TUser, TGroup, TConfiguration>>()
				.As<ICaliperConfigurationProvider>()
				.As<ILtiConfigurationProvider>()
				.As<IBaseConfigurationService<TConfiguration>>()
				.InstancePerRequest();

			// UserInfo
			builder.RegisterType<UserInfoService.UserInfoService>()
				.AsSelf()
				.As<IUserInfoService>();
			builder.RegisterType<UserIdentityServiceClient>()
				.AsSelf();

			// QueueManagers
			builder.RegisterType<SyncGroupRosterQueueManager>()
				.As<IQueueManager<SyncGroupRosterMessage>>()
				.AsSelf();
			builder.RegisterType<SyncAllRostersQueueManager>()
				.As<IQueueManager<SyncAllRostersMessage>>()
				.AsSelf();

			// Lti Services

			builder.RegisterType<JwtTokenService<GroupUserRole, GroupUserRoleLog, ExternalGroup, ExternalGroupUser>>()
				.As<IJwtTokenService>()
				.InstancePerRequest();

			builder.RegisterType<LtiService<GroupUserRole, GroupUserRoleLog, ExternalGroup, ExternalGroupUser>>()
				.As<ILtiService>()
				.InstancePerRequest();

			builder.RegisterType<LtiKeySetProviderService>()
				.As<ILtiKeySetProviderService>()
				.InstancePerRequest();

			builder.RegisterType<LtiLaunchService<TContext>>()
				.As<ILtiLaunchService>()
				.InstancePerRequest();

			builder.RegisterType<JsonWebKeyService>()
				.As<IJsonWebKeyService>()
				.InstancePerRequest();

			builder.Register<Func<IHttpClient>>(c => () => new RetryingHttpClient());

			builder.RegisterType<LtiApiService>()
				.As<ILtiApiService>()
				.InstancePerRequest();

			return builder;
		}

		protected virtual void ConfigureResponseCaching(IAppBuilder app)
		{
			app.Use(async (context, next) =>
			{
				var response = context.Response;
				response.OnSendingHeaders(state =>
				{
					var resp = (OwinResponse)state;
					resp.Headers["Cache-Control"] = "no-store, must-revalidate";
				}, response);
				await next();
			});
		}

		protected abstract void ConfigureShard(IAppBuilder app);

		protected abstract void ConfigureAppClientService(IAppBuilder app);

		#region Auth

		protected virtual void ConfigureAuth(IAppBuilder app)
		{
			var isDebug = EncryptedConfigurationManager.GetSetting(BaseAppSetting.RunConfiguration) == RunConfiguration.Debug;
			var tier = EncryptedConfigurationManager.GetSetting(BaseAppSetting.Tier);

			// Allow cross origin access for JS on DEV
			if (tier != Tier.Prod)
				app.UseCors(CorsOptions.AllowAll);

			// Enable the application to use a cookie to temporarily store information about a user logging in with a third party login provider
			app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

			// Configure the application for OAuth based flow
			var oauthServerOptions = new OAuthAuthorizationServerOptions
			{
				TokenEndpointPath = new PathString("/Token"),
				Provider = new ApplicationOAuthProvider<TContext>(),
				// chosen to provide balance between server load and risk of bearer token compromise
				AccessTokenExpireTimeSpan = TimeSpan.FromHours(2),
				AllowInsecureHttp = isDebug,
				RefreshTokenProvider = new RefreshTokenProvider<TContext>(),
				AuthorizationCodeProvider = new AuthorizationCodeProvider()
			};
			app.UseOAuthAuthorizationServer(oauthServerOptions);

			// Enable the application to use bearer tokens to authenticate users
			// Alternate to `app.UseOAuthBearerTokens(...)` to allow for custom Provider
			var oauthBearerOptions = new OAuthBearerAuthenticationOptions
			{
				AccessTokenFormat = oauthServerOptions.AccessTokenFormat,
				AccessTokenProvider = oauthServerOptions.AccessTokenProvider,
				AuthenticationMode = oauthServerOptions.AuthenticationMode,
				AuthenticationType = oauthServerOptions.AuthenticationType,
				Description = oauthServerOptions.Description,
				Provider = new ApplicationOAuthBearerProvider(),
				SystemClock = oauthServerOptions.SystemClock
			};
			app.UseOAuthBearerAuthentication(oauthBearerOptions);

			// TODO: LTI

			// Shibboleth
			if (ShibbolethConfiguration.IsEnabled)
			{
				var shibbolethClaimsMapper = new ShibbolethClaimsMapper
				{
					OnAuthenticate = claims =>
					{
						// Add Purdue Instructors to Instructor role
						var claimsList = claims.ToList();
						var emailClaim = claimsList.FirstOrDefault(c => c.Type == ClaimTypes.Email);
						if (emailClaim?.Value != null && !emailClaim.Value.Contains("purdue.edu"))
							return claimsList;
						var employeeType = claimsList.Where(c => c.Type == ShibbolethSaml2Attributes.EmployeeType).Select(c => c.Value).ToArray();
						if (PurdueI2A2Characteristics.IsPurdueInstructor(employeeType))
							claimsList.Add(new Claim(ClaimTypes.Role, BaseRole.Creator));
						return claimsList;
					}
				};
				var shibbolethOptions = ShibbolethConfiguration.GetAuthServicesOptions(shibbolethClaimsMapper);
				app.UseSaml2Authentication(shibbolethOptions);
			}

			// Facebook
			if (!string.IsNullOrEmpty(EncryptedConfigurationManager.GetSetting(ScaffoldingAppSetting.FacebookAppId)))
			{
				var facebookOptions = new FacebookAuthenticationOptions
				{
					AppId = EncryptedConfigurationManager.GetSetting(ScaffoldingAppSetting.FacebookAppId),
					AppSecret = EncryptedConfigurationManager.GetSetting(ScaffoldingAppSetting.FacebookAppSecret),
					Provider = new FacebookAuthenticationProvider
					{
						OnAuthenticated = context =>
						{
							AddClaim(context.Identity, ScaffoldingClaimTypes.FacebookAccessToken, context.AccessToken, LoginProviderConstants.Facebook);
							AddClaim(context.Identity, ScaffoldingClaimTypes.FacebookExpiresIn, context.ExpiresIn?.ToString(), LoginProviderConstants.Facebook);
							AddClaim(context.Identity, ClaimTypes.GivenName, context.User.Value<string>("first_name"), LoginProviderConstants.Facebook);
							AddClaim(context.Identity, ClaimTypes.Surname, context.User.Value<string>("last_name"), LoginProviderConstants.Facebook);
							return Task.FromResult(0);
						}
					}
				};
				facebookOptions.Scope.Add("email");
				facebookOptions.Fields.Add("email");
				facebookOptions.Fields.Add("first_name");
				facebookOptions.Fields.Add("last_name");
				app.UseFacebookAuthentication(facebookOptions);
			}

			// Google
			if (!string.IsNullOrEmpty(EncryptedConfigurationManager.GetSetting(ScaffoldingAppSetting.GoogleClientId)))
			{
				app.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions
				{
					ClientId = EncryptedConfigurationManager.GetSetting(ScaffoldingAppSetting.GoogleClientId),
					ClientSecret = EncryptedConfigurationManager.GetSetting(ScaffoldingAppSetting.GoogleClientSecret),
					Provider = new GoogleOAuth2AuthenticationProvider
					{
						OnAuthenticated = context =>
						{
							AddClaim(context.Identity, ScaffoldingClaimTypes.GoogleAccessToken, context.AccessToken, LoginProviderConstants.Google);
							AddClaim(context.Identity, ScaffoldingClaimTypes.GoogleRefreshToken, context.RefreshToken, LoginProviderConstants.Google);
							AddClaim(context.Identity, ScaffoldingClaimTypes.GoogleExpiresIn, context.ExpiresIn?.ToString(), LoginProviderConstants.Google);
							AddClaim(context.Identity, ClaimTypes.GivenName, context.GivenName, LoginProviderConstants.Google);
							AddClaim(context.Identity, ClaimTypes.Surname, context.FamilyName, LoginProviderConstants.Google);
							return Task.FromResult(0);
						}
					}
				});
			}
		}

		private static void AddClaim(ClaimsIdentity identity, string claimType, string value, string provider)
		{
			// An exception will happen if the value is null, so let's prevent that
			if (string.IsNullOrWhiteSpace(value))
				return;

			identity.AddClaim(new Claim(claimType, value, ClaimValueTypes.String, provider));
		}

		#endregion Auth
	}
}