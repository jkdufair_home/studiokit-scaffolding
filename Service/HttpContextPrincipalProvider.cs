﻿using StudioKit.Security.Interfaces;
using System.Security.Principal;
using System.Web;

namespace StudioKit.Scaffolding.Service
{
	public class HttpContextPrincipalProvider : IPrincipalProvider
	{
		public IPrincipal GetPrincipal()
		{
			return HttpContext.Current?.User;
		}
	}
}