﻿using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace StudioKit.Scaffolding.Service.Results
{
	public class HttpActionResult<TContent> : IHttpActionResult
		where TContent : class
	{
		private readonly TContent _content;
		private readonly HttpStatusCode _statusCode;

		public HttpActionResult(HttpStatusCode statusCode, TContent content)
		{
			_statusCode = statusCode;
			_content = content;
		}

		public async Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken) =>
			await Task.FromResult(
				new HttpResponseMessage(_statusCode)
				{
					Content = _content == null ?
					null : new ObjectContent<TContent>(_content, new JsonMediaTypeFormatter())
				});
	}
}