﻿using System.Net;
using System.Web.Http;

namespace StudioKit.Scaffolding.Service.Results
{
	public static class ResultFactory
	{
		public static IHttpActionResult Create<TContent>(HttpStatusCode statusCode, TContent content)
			where TContent : class
			=> new HttpActionResult<TContent>(statusCode, content);
	}
}