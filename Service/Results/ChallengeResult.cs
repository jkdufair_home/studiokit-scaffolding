﻿using Microsoft.Owin.Security;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace StudioKit.Scaffolding.Service.Results
{
	public class ChallengeResult : IHttpActionResult
	{
		/// <summary>
		/// Used by KentorAuthServices provider to specify the target Shibboleth Idp
		/// </summary>
		private const string IdpKey = "idp";

		/// <summary>
		/// Used for XSRF protection when adding external logins
		/// </summary>
		private const string XsrfKey = "XsrfId";

		private readonly string _provider;
		private readonly HttpRequestMessage _messageRequest;
		private readonly string _redirectUri;
		private readonly string _userId;
		private readonly string _idp;

		public ChallengeResult(string provider, HttpRequestMessage request)
			: this(provider, request, null)
		{
		}

		public ChallengeResult(string provider, HttpRequestMessage request, string redirectUri)
			: this(provider, request, redirectUri, null, null)
		{
		}

		public ChallengeResult(string provider, HttpRequestMessage request, string redirectUri, string idp)
			: this(provider, request, redirectUri, idp, null)
		{
		}

		public ChallengeResult(string provider, HttpRequestMessage request, string redirectUri, string idp, string userId)
		{
			_provider = provider;
			_messageRequest = request;
			_redirectUri = redirectUri;
			_idp = idp;
			_userId = userId;
		}

		public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
		{
			var properties = new AuthenticationProperties();
			if (_redirectUri != null)
			{
				properties.RedirectUri = _redirectUri;
			}
			if (_idp != null)
			{
				properties.Dictionary[IdpKey] = _idp;
			}
			if (_userId != null)
			{
				properties.Dictionary[XsrfKey] = _userId;
			}

			_messageRequest.GetOwinContext().Authentication.Challenge(properties, _provider);

			var response = new HttpResponseMessage(HttpStatusCode.Unauthorized)
			{
				RequestMessage = _messageRequest
			};

			return Task.FromResult(response);
		}
	}
}