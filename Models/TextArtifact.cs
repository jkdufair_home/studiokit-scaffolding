﻿using System.Collections.Generic;

namespace StudioKit.Scaffolding.Models
{
	public class TextArtifact : Artifact
	{
		public int WordCount { get; set; }

		#region Cloning

		public override object Clone(string userId)
		{
			var artifact = (TextArtifact)base.Clone(userId);
			artifact.WordCount = WordCount;
			return artifact;
		}

		public sealed class ClonedTextArtifactEqualityComparer : IEqualityComparer<TextArtifact>
		{
			public bool Equals(TextArtifact x, TextArtifact y)
			{
				if (ReferenceEquals(x, y)) return true;
				if (ReferenceEquals(x, null)) return false;
				if (ReferenceEquals(y, null)) return false;
				if (x.GetType() != y.GetType()) return false;
				return x.WordCount == y.WordCount;
			}

			public int GetHashCode(TextArtifact obj)
			{
				return obj.WordCount;
			}
		}

		protected override bool IsCloneEqual(Artifact artifact)
		{
			return artifact is TextArtifact textArtifact &&
					new ClonedTextArtifactEqualityComparer().Equals(this, textArtifact);
		}

		#endregion Cloning
	}
}