using StudioKit.Data;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Data.Entity.Identity.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudioKit.Scaffolding.Models
{
	public abstract class EntityUserRole : ModelBase
	{
		[Index("IX_EntityUserRole_Entity", IsUnique = true, Order = 0)]
		[StringLength(128)]
		[Required]
		public string UserId { get; set; }

		[Index("IX_EntityUserRole_Entity", IsUnique = true, Order = 1)]
		[StringLength(128)]
		[Required]
		public string RoleId { get; set; }

		public abstract int EntityId { get; set; }

		[ForeignKey("UserId")]
		public virtual IUser User { get; set; }

		[ForeignKey("RoleId")]
		public virtual Role Role { get; set; }
	}
}