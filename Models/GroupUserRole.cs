using StudioKit.ExternalProvider.Models.Interfaces;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudioKit.Scaffolding.Models
{
	public class GroupUserRole : EntityUserRole, IGroupUserRole
	{
		[Index("IX_EntityUserRole_Entity", IsUnique = true, Order = 2)]
		public int GroupId { get; set; }

		[ForeignKey("GroupId")]
		public virtual BaseGroup Group { get; set; }

		public override int EntityId { get => GroupId; set => GroupId = value; }

		public bool IsExternal { get; set; }
	}
}