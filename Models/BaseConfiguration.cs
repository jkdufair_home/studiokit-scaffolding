﻿using StudioKit.ExternalProvider.Lti.Models.Interfaces;
using StudioKit.Sharding.Models;

namespace StudioKit.Scaffolding.Models
{
	public class BaseConfiguration : BaseShardConfiguration, ILtiConfiguration
	{
		public byte[] PrivateRsaKey { get; set; }
		public byte[] PublicRsaKey { get; set; }
	}
}