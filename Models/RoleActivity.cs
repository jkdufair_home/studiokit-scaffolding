﻿using StudioKit.Data;
using StudioKit.Data.Entity.Identity.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudioKit.Scaffolding.Models
{
	public class RoleActivity : ModelBase
	{
		[Required]
		[Index("IX_RoleId_ActivityId", 0, IsUnique = true)]
		public string RoleId { get; set; }

		[ForeignKey("RoleId")]
		public virtual Role Role { get; set; }

		[Required]
		[Index("IX_RoleId_ActivityId", 1, IsUnique = true)]
		public int ActivityId { get; set; }

		[ForeignKey("ActivityId")]
		public virtual Activity Activity { get; set; }
	}
}