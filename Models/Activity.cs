using StudioKit.Data;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudioKit.Scaffolding.Models
{
	public class Activity : ModelBase
	{
		[Required]
		[Index("IX_Name", IsUnique = true)]
		[StringLength(128)]
		public string Name { get; set; }

		public virtual ICollection<RoleActivity> RoleActivities { get; set; }
	}
}