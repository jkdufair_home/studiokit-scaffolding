﻿using StudioKit.Data;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.ExternalProvider.Models.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudioKit.Scaffolding.Models
{
	public class ExternalGroupUser : ModelBase, IExternalGroupUser<ExternalGroup>
	{
		[Required]
		[Index("IX_ExternalGroupId_UserId", 1)]
		public int ExternalGroupId { get; set; }

		[ForeignKey("ExternalGroupId")]
		public virtual ExternalGroup ExternalGroup { get; set; }

		[Required]
		[Index("IX_ExternalGroupId_UserId", 2)]
		public string UserId { get; set; }

		[ForeignKey("UserId")]
		public virtual IUser User { get; set; }

		[Required]
		public string ExternalUserId { get; set; }

		public string Roles { get; set; }
	}
}