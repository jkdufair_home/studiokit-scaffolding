﻿using StudioKit.Data;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudioKit.Scaffolding.Models
{
	public abstract class Artifact : ModelBase, IDelible, IOwnable
	{
		[Required]
		public string Url { get; set; }

		[Index("IX_Artifact_IsDeleted")]
		public bool IsDeleted { get; set; }

		[Required]
		public string CreatedById { get; set; }

		[ForeignKey(nameof(CreatedById))]
		public virtual IUser User { get; set; }

		#region Cloning

		/// <summary>
		/// Clone an artifact
		/// Note this does not clone the artifact's URL by default since this will
		/// require copying the associated blob for some subtypes
		/// </summary>
		/// <param name="userId"></param>
		/// <returns></returns>
		public virtual object Clone(string userId)
		{
			var artifact = (Artifact)Activator.CreateInstance(GetType());
			artifact.CreatedById = userId;
			return artifact;
		}

		private sealed class ClonedArtifactEqualityComparer : IEqualityComparer<Artifact>
		{
			public bool Equals(Artifact x, Artifact y)
			{
				if (ReferenceEquals(x, y)) return true;
				if (ReferenceEquals(x, null)) return false;
				if (ReferenceEquals(y, null)) return false;
				if (x.GetType() != y.GetType()) return false;
				return x.IsCloneEqual(y);
			}

			public int GetHashCode(Artifact obj)
			{
				return 0;
			}
		}

		protected virtual bool IsCloneEqual(Artifact artifact)
		{
			throw new NotImplementedException();
		}

		public static IEqualityComparer<Artifact> CloneComparer => new ClonedArtifactEqualityComparer();

		#endregion Cloning
	}
}