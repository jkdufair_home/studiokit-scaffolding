﻿using System.Collections.Generic;

namespace StudioKit.Scaffolding.Models.Interfaces
{
	public interface IArtifactsContainer<T> where T : IEntityArtifact
	{
		ICollection<T> EntityArtifacts { get; set; }

		IEnumerable<Artifact> Artifacts { get; }
	}
}