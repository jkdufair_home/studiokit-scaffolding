using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Sharding.Entity.Identity.Models;
using System.Collections.Generic;

namespace StudioKit.Scaffolding.Models.Interfaces
{
	public interface IBaseUser : IUser
	{
		ICollection<ExternalGroup> ExternalGroups { get; set; }

		ICollection<ExternalGroupUser> ExternalGroupUsers { get; set; }

		ICollection<EntityUserRole> EntityUserRoles { get; set; }

		IEnumerable<GroupUserRole> GroupUserRoles { get; }

		ICollection<LicenseUser> LicenseUsers { get; set; }

		IEnumerable<BaseGroup> Groups { get; }

		IEnumerable<BaseGroup> InstructorGroups { get; }

		ICollection<Artifact> Artifacts { get; set; }
	}
}