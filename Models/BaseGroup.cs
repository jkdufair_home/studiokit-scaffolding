﻿using StudioKit.Data;
using StudioKit.ExternalProvider.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudioKit.Scaffolding.Models
{
	[Table("Groups")]
	public abstract class BaseGroup : ModelBase, IGroup<GroupUserRole, ExternalGroup>
	{
		public string Name { get; set; }

		[Index("IX_Group_isDeleted")]
		public bool IsDeleted { get; set; }

		public int? ExternalTermId { get; set; }

		[ForeignKey(nameof(ExternalTermId))]
		public virtual ExternalTerm ExternalTerm { get; set; }

		public DateTime? StartDate { get; set; }

		public DateTime? EndDate { get; set; }

		public virtual ICollection<GroupUserRole> GroupUserRoles { get; set; }

		public virtual ICollection<GroupUserRoleLog> GroupUserRoleLogs { get; set; }

		public virtual ICollection<ExternalGroup> ExternalGroups { get; set; }

		public Type UserRoleAssociatedType => typeof(GroupUserRole);
	}
}